from django.db import models

from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.blocks import ImageChooserBlock
from streams import blocks
from wagtail.core.fields import StreamField
from wagtail.images.edit_handlers import ImageChooserPanel

# Create your models here.
class ProjectsListingPage(Page):
    """Listing page lists all the Blog Detail Pages."""

    template = "project/projects_listing_page.html"

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)

    

    content_panels = Page.content_panels + [
        
        FieldPanel("header_title"),
        FieldPanel("meta_content"),
    ]

    def get_context(self, request, *args, **kwargs):
        """Adding custom stuff to our context."""
        context = super().get_context(request, *args, **kwargs)
        context["posts"] = ProjectsDetailPage.objects.live().public()
       
        return context



class ProjectsDetailPage(Page):

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)
    
    right_content_heading = models.CharField(max_length=200, blank=True, null=True)
    right_content = StreamField(
        [
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
            ("cards", blocks.CardBlock()),
            ("cta", blocks.CTABlock()),
        ],
        null=True,
        blank=True,
    )
    timeline = models.CharField(max_length=200, blank=True, null=True)
    platform = StreamField(
        [
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
            ("cards", blocks.CardBlock()),
            ("cta", blocks.CTABlock()),
        ],
        null=True,
        blank=True,
    )
    deliverables = StreamField(
        [
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
            ("cards", blocks.CardBlock()),
            ("cta", blocks.CTABlock()),
        ],
        null=True,
        blank=True,
    )

    section1_image = models.ForeignKey(
        "wagtailimages.Image",
        blank=False,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )


    section1_content = StreamField(
        [
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
            ("cards", blocks.CardBlock()),
            ("cta", blocks.CTABlock()),
            ('image', ImageChooserBlock()),
        ],
        null=True,
        blank=True,
    )   

    section2_image = models.ForeignKey(
        "wagtailimages.Image",
        blank=False,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )

    section2_content = StreamField(
        [
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
            ("cards", blocks.CardBlock()),
            ("cta", blocks.CTABlock()),
            ('image', ImageChooserBlock()),
        ],
        null=False,
        blank=True,
    )  
    project_list_bg_image =models.ForeignKey(
        "wagtailimages.Image",
        blank=False,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    project_list_logo = models.ForeignKey(
        "wagtailimages.Image",
        blank=False,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    project_list_heading =  models.CharField(max_length=200, blank=True, null=True)
    project_list_title =  models.CharField(max_length=200, blank=True, null=True)
    project_list_subtitle = models.CharField(max_length=200, blank=True, null=True)
    bg_color = models.ForeignKey(
        "wagtailimages.Image",
        blank=False,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )


    nextpage_bg_image =models.ForeignKey(
        "wagtailimages.Image",
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    nextpage_logo = models.ForeignKey(
        "wagtailimages.Image",
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    nextpage_heading =  models.CharField(max_length=200, blank=True, null=True)
    nextpage_title =  models.CharField(max_length=200, blank=True, null=True)
    nextpage_subtitle = models.CharField(max_length=200, blank=True, null=True)
    nextpage_url = models.URLField(blank=True, null=True)
    

    




    content_panels = Page.content_panels + [
        FieldPanel('header_title'),
        FieldPanel('meta_content'),
        ImageChooserPanel('project_list_bg_image'),
        ImageChooserPanel('project_list_logo'),
        FieldPanel('project_list_heading'),
        FieldPanel('project_list_title'),
        FieldPanel('project_list_subtitle'),
        ImageChooserPanel('bg_color'),
        FieldPanel('timeline'),
        StreamFieldPanel('platform'),
        StreamFieldPanel('deliverables'),
        FieldPanel('right_content_heading'),
        StreamFieldPanel('right_content'),
        ImageChooserPanel('section1_image'),
        StreamFieldPanel('section1_content'),
        ImageChooserPanel('section2_image'),
        StreamFieldPanel('section2_content'),

        ImageChooserPanel('nextpage_bg_image'),
        ImageChooserPanel('nextpage_logo'),
        FieldPanel('nextpage_heading'),
        FieldPanel('nextpage_title'),
        FieldPanel('nextpage_url'),
        FieldPanel('nextpage_subtitle'),
        
    ]
