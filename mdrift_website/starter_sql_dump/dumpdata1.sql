--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: blog_blogdetailpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_blogdetailpage (
    page_ptr_id integer NOT NULL,
    header_title character varying(200),
    meta_content character varying(200),
    date date,
    read_time character varying(100),
    content text,
    blog_detail_image_id integer,
    blog_list_image_id integer
);


ALTER TABLE public.blog_blogdetailpage OWNER TO postgres;

--
-- Name: blog_bloglistingpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_bloglistingpage (
    page_ptr_id integer NOT NULL,
    header_title character varying(200),
    meta_content character varying(200)
);


ALTER TABLE public.blog_bloglistingpage OWNER TO postgres;

--
-- Name: contact_contactpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contact_contactpage (
    page_ptr_id integer NOT NULL,
    to_address character varying(255) NOT NULL,
    from_address character varying(255) NOT NULL,
    subject character varying(255) NOT NULL,
    intro text NOT NULL,
    thank_you_text text NOT NULL
);


ALTER TABLE public.contact_contactpage OWNER TO postgres;

--
-- Name: contact_formfield; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contact_formfield (
    id bigint NOT NULL,
    sort_order integer,
    clean_name character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    field_type character varying(16) NOT NULL,
    required boolean NOT NULL,
    choices text NOT NULL,
    default_value character varying(255) NOT NULL,
    help_text character varying(255) NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.contact_formfield OWNER TO postgres;

--
-- Name: contact_formfield_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.contact_formfield_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_formfield_id_seq OWNER TO postgres;

--
-- Name: contact_formfield_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.contact_formfield_id_seq OWNED BY public.contact_formfield.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: home_aboutpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.home_aboutpage (
    page_ptr_id integer NOT NULL,
    header_title character varying(200),
    meta_content character varying(200)
);


ALTER TABLE public.home_aboutpage OWNER TO postgres;

--
-- Name: home_homepage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.home_homepage (
    page_ptr_id integer NOT NULL,
    client_logos text NOT NULL,
    header_title character varying(200),
    meta_content character varying(200)
);


ALTER TABLE public.home_homepage OWNER TO postgres;

--
-- Name: home_technologiespage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.home_technologiespage (
    page_ptr_id integer NOT NULL,
    header_title character varying(200),
    meta_content character varying(200)
);


ALTER TABLE public.home_technologiespage OWNER TO postgres;

--
-- Name: project_projectsdetailpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_projectsdetailpage (
    page_ptr_id integer NOT NULL,
    header_title character varying(200),
    meta_content character varying(200),
    right_content_heading character varying(200),
    right_content text,
    timeline character varying(200),
    platform text,
    deliverables text,
    section1_content text,
    section2_content text NOT NULL,
    project_list_heading character varying(200),
    project_list_title character varying(200),
    project_list_subtitle character varying(200),
    nextpage_heading character varying(200),
    nextpage_title character varying(200),
    nextpage_subtitle character varying(200),
    nextpage_url character varying(200),
    bg_color_id integer,
    nextpage_bg_image_id integer,
    nextpage_logo_id integer,
    project_list_bg_image_id integer,
    project_list_logo_id integer,
    section1_image_id integer,
    section2_image_id integer
);


ALTER TABLE public.project_projectsdetailpage OWNER TO postgres;

--
-- Name: project_projectslistingpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_projectslistingpage (
    page_ptr_id integer NOT NULL,
    header_title character varying(200),
    meta_content character varying(200)
);


ALTER TABLE public.project_projectslistingpage OWNER TO postgres;

--
-- Name: taggit_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.taggit_tag (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    slug character varying(100) NOT NULL
);


ALTER TABLE public.taggit_tag OWNER TO postgres;

--
-- Name: taggit_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.taggit_tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taggit_tag_id_seq OWNER TO postgres;

--
-- Name: taggit_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.taggit_tag_id_seq OWNED BY public.taggit_tag.id;


--
-- Name: taggit_taggeditem; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.taggit_taggeditem (
    id integer NOT NULL,
    object_id integer NOT NULL,
    content_type_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.taggit_taggeditem OWNER TO postgres;

--
-- Name: taggit_taggeditem_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.taggit_taggeditem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taggit_taggeditem_id_seq OWNER TO postgres;

--
-- Name: taggit_taggeditem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.taggit_taggeditem_id_seq OWNED BY public.taggit_taggeditem.id;


--
-- Name: wagtailadmin_admin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailadmin_admin (
    id integer NOT NULL
);


ALTER TABLE public.wagtailadmin_admin OWNER TO postgres;

--
-- Name: wagtailadmin_admin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailadmin_admin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailadmin_admin_id_seq OWNER TO postgres;

--
-- Name: wagtailadmin_admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailadmin_admin_id_seq OWNED BY public.wagtailadmin_admin.id;


--
-- Name: wagtailcore_collection; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_collection (
    id integer NOT NULL,
    path character varying(255) NOT NULL COLLATE pg_catalog."C",
    depth integer NOT NULL,
    numchild integer NOT NULL,
    name character varying(255) NOT NULL,
    CONSTRAINT wagtailcore_collection_depth_check CHECK ((depth >= 0)),
    CONSTRAINT wagtailcore_collection_numchild_check CHECK ((numchild >= 0))
);


ALTER TABLE public.wagtailcore_collection OWNER TO postgres;

--
-- Name: wagtailcore_collection_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_collection_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_collection_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_collection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_collection_id_seq OWNED BY public.wagtailcore_collection.id;


--
-- Name: wagtailcore_collectionviewrestriction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_collectionviewrestriction (
    id integer NOT NULL,
    restriction_type character varying(20) NOT NULL,
    password character varying(255) NOT NULL,
    collection_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_collectionviewrestriction OWNER TO postgres;

--
-- Name: wagtailcore_collectionviewrestriction_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_collectionviewrestriction_groups (
    id integer NOT NULL,
    collectionviewrestriction_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_collectionviewrestriction_groups OWNER TO postgres;

--
-- Name: wagtailcore_collectionviewrestriction_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_collectionviewrestriction_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_collectionviewrestriction_groups_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_collectionviewrestriction_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_collectionviewrestriction_groups_id_seq OWNED BY public.wagtailcore_collectionviewrestriction_groups.id;


--
-- Name: wagtailcore_collectionviewrestriction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_collectionviewrestriction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_collectionviewrestriction_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_collectionviewrestriction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_collectionviewrestriction_id_seq OWNED BY public.wagtailcore_collectionviewrestriction.id;


--
-- Name: wagtailcore_comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_comment (
    id integer NOT NULL,
    text text NOT NULL,
    contentpath text NOT NULL,
    "position" text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    resolved_at timestamp with time zone,
    page_id integer NOT NULL,
    resolved_by_id integer,
    revision_created_id integer,
    user_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_comment OWNER TO postgres;

--
-- Name: wagtailcore_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_comment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_comment_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_comment_id_seq OWNED BY public.wagtailcore_comment.id;


--
-- Name: wagtailcore_commentreply; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_commentreply (
    id integer NOT NULL,
    text text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    comment_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_commentreply OWNER TO postgres;

--
-- Name: wagtailcore_commentreply_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_commentreply_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_commentreply_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_commentreply_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_commentreply_id_seq OWNED BY public.wagtailcore_commentreply.id;


--
-- Name: wagtailcore_groupapprovaltask; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_groupapprovaltask (
    task_ptr_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_groupapprovaltask OWNER TO postgres;

--
-- Name: wagtailcore_groupapprovaltask_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_groupapprovaltask_groups (
    id integer NOT NULL,
    groupapprovaltask_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_groupapprovaltask_groups OWNER TO postgres;

--
-- Name: wagtailcore_groupapprovaltask_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_groupapprovaltask_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_groupapprovaltask_groups_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_groupapprovaltask_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_groupapprovaltask_groups_id_seq OWNED BY public.wagtailcore_groupapprovaltask_groups.id;


--
-- Name: wagtailcore_groupcollectionpermission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_groupcollectionpermission (
    id integer NOT NULL,
    collection_id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_groupcollectionpermission OWNER TO postgres;

--
-- Name: wagtailcore_groupcollectionpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_groupcollectionpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_groupcollectionpermission_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_groupcollectionpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_groupcollectionpermission_id_seq OWNED BY public.wagtailcore_groupcollectionpermission.id;


--
-- Name: wagtailcore_grouppagepermission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_grouppagepermission (
    id integer NOT NULL,
    permission_type character varying(20) NOT NULL,
    group_id integer NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_grouppagepermission OWNER TO postgres;

--
-- Name: wagtailcore_grouppagepermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_grouppagepermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_grouppagepermission_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_grouppagepermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_grouppagepermission_id_seq OWNED BY public.wagtailcore_grouppagepermission.id;


--
-- Name: wagtailcore_locale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_locale (
    id integer NOT NULL,
    language_code character varying(100) NOT NULL
);


ALTER TABLE public.wagtailcore_locale OWNER TO postgres;

--
-- Name: wagtailcore_locale_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_locale_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_locale_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_locale_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_locale_id_seq OWNED BY public.wagtailcore_locale.id;


--
-- Name: wagtailcore_page; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_page (
    id integer NOT NULL,
    path character varying(255) NOT NULL COLLATE pg_catalog."C",
    depth integer NOT NULL,
    numchild integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    live boolean NOT NULL,
    has_unpublished_changes boolean NOT NULL,
    url_path text NOT NULL,
    seo_title character varying(255) NOT NULL,
    show_in_menus boolean NOT NULL,
    search_description text NOT NULL,
    go_live_at timestamp with time zone,
    expire_at timestamp with time zone,
    expired boolean NOT NULL,
    content_type_id integer NOT NULL,
    owner_id integer,
    locked boolean NOT NULL,
    latest_revision_created_at timestamp with time zone,
    first_published_at timestamp with time zone,
    live_revision_id integer,
    last_published_at timestamp with time zone,
    draft_title character varying(255) NOT NULL,
    locked_at timestamp with time zone,
    locked_by_id integer,
    translation_key uuid NOT NULL,
    locale_id integer NOT NULL,
    alias_of_id integer,
    CONSTRAINT wagtailcore_page_depth_check CHECK ((depth >= 0)),
    CONSTRAINT wagtailcore_page_numchild_check CHECK ((numchild >= 0))
);


ALTER TABLE public.wagtailcore_page OWNER TO postgres;

--
-- Name: wagtailcore_page_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_page_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_page_id_seq OWNED BY public.wagtailcore_page.id;


--
-- Name: wagtailcore_pagelogentry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pagelogentry (
    id integer NOT NULL,
    label text NOT NULL,
    action character varying(255) NOT NULL,
    data_json text NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    content_changed boolean NOT NULL,
    deleted boolean NOT NULL,
    content_type_id integer,
    page_id integer NOT NULL,
    revision_id integer,
    user_id integer
);


ALTER TABLE public.wagtailcore_pagelogentry OWNER TO postgres;

--
-- Name: wagtailcore_pagelogentry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pagelogentry_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pagelogentry_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pagelogentry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pagelogentry_id_seq OWNED BY public.wagtailcore_pagelogentry.id;


--
-- Name: wagtailcore_pagerevision; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pagerevision (
    id integer NOT NULL,
    submitted_for_moderation boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    content_json text NOT NULL,
    approved_go_live_at timestamp with time zone,
    page_id integer NOT NULL,
    user_id integer
);


ALTER TABLE public.wagtailcore_pagerevision OWNER TO postgres;

--
-- Name: wagtailcore_pagerevision_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pagerevision_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pagerevision_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pagerevision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pagerevision_id_seq OWNED BY public.wagtailcore_pagerevision.id;


--
-- Name: wagtailcore_pagesubscription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pagesubscription (
    id integer NOT NULL,
    comment_notifications boolean NOT NULL,
    page_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_pagesubscription OWNER TO postgres;

--
-- Name: wagtailcore_pagesubscription_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pagesubscription_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pagesubscription_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pagesubscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pagesubscription_id_seq OWNED BY public.wagtailcore_pagesubscription.id;


--
-- Name: wagtailcore_pageviewrestriction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pageviewrestriction (
    id integer NOT NULL,
    password character varying(255) NOT NULL,
    page_id integer NOT NULL,
    restriction_type character varying(20) NOT NULL
);


ALTER TABLE public.wagtailcore_pageviewrestriction OWNER TO postgres;

--
-- Name: wagtailcore_pageviewrestriction_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_pageviewrestriction_groups (
    id integer NOT NULL,
    pageviewrestriction_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_pageviewrestriction_groups OWNER TO postgres;

--
-- Name: wagtailcore_pageviewrestriction_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pageviewrestriction_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pageviewrestriction_groups_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pageviewrestriction_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pageviewrestriction_groups_id_seq OWNED BY public.wagtailcore_pageviewrestriction_groups.id;


--
-- Name: wagtailcore_pageviewrestriction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_pageviewrestriction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_pageviewrestriction_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_pageviewrestriction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_pageviewrestriction_id_seq OWNED BY public.wagtailcore_pageviewrestriction.id;


--
-- Name: wagtailcore_site; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_site (
    id integer NOT NULL,
    hostname character varying(255) NOT NULL,
    port integer NOT NULL,
    is_default_site boolean NOT NULL,
    root_page_id integer NOT NULL,
    site_name character varying(255) NOT NULL
);


ALTER TABLE public.wagtailcore_site OWNER TO postgres;

--
-- Name: wagtailcore_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_site_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_site_id_seq OWNED BY public.wagtailcore_site.id;


--
-- Name: wagtailcore_task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_task (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    active boolean NOT NULL,
    content_type_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_task OWNER TO postgres;

--
-- Name: wagtailcore_task_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_task_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_task_id_seq OWNED BY public.wagtailcore_task.id;


--
-- Name: wagtailcore_taskstate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_taskstate (
    id integer NOT NULL,
    status character varying(50) NOT NULL,
    started_at timestamp with time zone NOT NULL,
    finished_at timestamp with time zone,
    content_type_id integer NOT NULL,
    page_revision_id integer NOT NULL,
    task_id integer NOT NULL,
    workflow_state_id integer NOT NULL,
    finished_by_id integer,
    comment text NOT NULL
);


ALTER TABLE public.wagtailcore_taskstate OWNER TO postgres;

--
-- Name: wagtailcore_taskstate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_taskstate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_taskstate_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_taskstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_taskstate_id_seq OWNED BY public.wagtailcore_taskstate.id;


--
-- Name: wagtailcore_workflow; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_workflow (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    active boolean NOT NULL
);


ALTER TABLE public.wagtailcore_workflow OWNER TO postgres;

--
-- Name: wagtailcore_workflow_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_workflow_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_workflow_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_workflow_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_workflow_id_seq OWNED BY public.wagtailcore_workflow.id;


--
-- Name: wagtailcore_workflowpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_workflowpage (
    page_id integer NOT NULL,
    workflow_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_workflowpage OWNER TO postgres;

--
-- Name: wagtailcore_workflowstate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_workflowstate (
    id integer NOT NULL,
    status character varying(50) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    current_task_state_id integer,
    page_id integer NOT NULL,
    requested_by_id integer,
    workflow_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_workflowstate OWNER TO postgres;

--
-- Name: wagtailcore_workflowstate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_workflowstate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_workflowstate_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_workflowstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_workflowstate_id_seq OWNED BY public.wagtailcore_workflowstate.id;


--
-- Name: wagtailcore_workflowtask; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailcore_workflowtask (
    id integer NOT NULL,
    sort_order integer,
    task_id integer NOT NULL,
    workflow_id integer NOT NULL
);


ALTER TABLE public.wagtailcore_workflowtask OWNER TO postgres;

--
-- Name: wagtailcore_workflowtask_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailcore_workflowtask_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailcore_workflowtask_id_seq OWNER TO postgres;

--
-- Name: wagtailcore_workflowtask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailcore_workflowtask_id_seq OWNED BY public.wagtailcore_workflowtask.id;


--
-- Name: wagtaildocs_document; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtaildocs_document (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    file character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    uploaded_by_user_id integer,
    collection_id integer NOT NULL,
    file_size integer,
    file_hash character varying(40) NOT NULL,
    CONSTRAINT wagtaildocs_document_file_size_check CHECK ((file_size >= 0))
);


ALTER TABLE public.wagtaildocs_document OWNER TO postgres;

--
-- Name: wagtaildocs_document_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtaildocs_document_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtaildocs_document_id_seq OWNER TO postgres;

--
-- Name: wagtaildocs_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtaildocs_document_id_seq OWNED BY public.wagtaildocs_document.id;


--
-- Name: wagtaildocs_uploadeddocument; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtaildocs_uploadeddocument (
    id integer NOT NULL,
    file character varying(200) NOT NULL,
    uploaded_by_user_id integer
);


ALTER TABLE public.wagtaildocs_uploadeddocument OWNER TO postgres;

--
-- Name: wagtaildocs_uploadeddocument_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtaildocs_uploadeddocument_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtaildocs_uploadeddocument_id_seq OWNER TO postgres;

--
-- Name: wagtaildocs_uploadeddocument_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtaildocs_uploadeddocument_id_seq OWNED BY public.wagtaildocs_uploadeddocument.id;


--
-- Name: wagtailembeds_embed; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailembeds_embed (
    id integer NOT NULL,
    url text NOT NULL,
    max_width smallint,
    type character varying(10) NOT NULL,
    html text NOT NULL,
    title text NOT NULL,
    author_name text NOT NULL,
    provider_name text NOT NULL,
    thumbnail_url text NOT NULL,
    width integer,
    height integer,
    last_updated timestamp with time zone NOT NULL,
    hash character varying(32) NOT NULL,
    cache_until timestamp with time zone
);


ALTER TABLE public.wagtailembeds_embed OWNER TO postgres;

--
-- Name: wagtailembeds_embed_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailembeds_embed_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailembeds_embed_id_seq OWNER TO postgres;

--
-- Name: wagtailembeds_embed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailembeds_embed_id_seq OWNED BY public.wagtailembeds_embed.id;


--
-- Name: wagtailforms_formsubmission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailforms_formsubmission (
    id integer NOT NULL,
    form_data text NOT NULL,
    submit_time timestamp with time zone NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.wagtailforms_formsubmission OWNER TO postgres;

--
-- Name: wagtailforms_formsubmission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailforms_formsubmission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailforms_formsubmission_id_seq OWNER TO postgres;

--
-- Name: wagtailforms_formsubmission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailforms_formsubmission_id_seq OWNED BY public.wagtailforms_formsubmission.id;


--
-- Name: wagtailimages_image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailimages_image (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    file character varying(100) NOT NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    focal_point_x integer,
    focal_point_y integer,
    focal_point_width integer,
    focal_point_height integer,
    uploaded_by_user_id integer,
    file_size integer,
    collection_id integer NOT NULL,
    file_hash character varying(40) NOT NULL,
    CONSTRAINT wagtailimages_image_file_size_check CHECK ((file_size >= 0)),
    CONSTRAINT wagtailimages_image_focal_point_height_check CHECK ((focal_point_height >= 0)),
    CONSTRAINT wagtailimages_image_focal_point_width_check CHECK ((focal_point_width >= 0)),
    CONSTRAINT wagtailimages_image_focal_point_x_check CHECK ((focal_point_x >= 0)),
    CONSTRAINT wagtailimages_image_focal_point_y_check CHECK ((focal_point_y >= 0))
);


ALTER TABLE public.wagtailimages_image OWNER TO postgres;

--
-- Name: wagtailimages_image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailimages_image_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailimages_image_id_seq OWNER TO postgres;

--
-- Name: wagtailimages_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailimages_image_id_seq OWNED BY public.wagtailimages_image.id;


--
-- Name: wagtailimages_rendition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailimages_rendition (
    id integer NOT NULL,
    file character varying(100) NOT NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    focal_point_key character varying(16) NOT NULL,
    filter_spec character varying(255) NOT NULL,
    image_id integer NOT NULL
);


ALTER TABLE public.wagtailimages_rendition OWNER TO postgres;

--
-- Name: wagtailimages_rendition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailimages_rendition_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailimages_rendition_id_seq OWNER TO postgres;

--
-- Name: wagtailimages_rendition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailimages_rendition_id_seq OWNED BY public.wagtailimages_rendition.id;


--
-- Name: wagtailimages_uploadedimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailimages_uploadedimage (
    id integer NOT NULL,
    file character varying(200) NOT NULL,
    uploaded_by_user_id integer
);


ALTER TABLE public.wagtailimages_uploadedimage OWNER TO postgres;

--
-- Name: wagtailimages_uploadedimage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailimages_uploadedimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailimages_uploadedimage_id_seq OWNER TO postgres;

--
-- Name: wagtailimages_uploadedimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailimages_uploadedimage_id_seq OWNED BY public.wagtailimages_uploadedimage.id;


--
-- Name: wagtailredirects_redirect; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailredirects_redirect (
    id integer NOT NULL,
    old_path character varying(255) NOT NULL,
    is_permanent boolean NOT NULL,
    redirect_link character varying(255) NOT NULL,
    redirect_page_id integer,
    site_id integer
);


ALTER TABLE public.wagtailredirects_redirect OWNER TO postgres;

--
-- Name: wagtailredirects_redirect_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailredirects_redirect_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailredirects_redirect_id_seq OWNER TO postgres;

--
-- Name: wagtailredirects_redirect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailredirects_redirect_id_seq OWNED BY public.wagtailredirects_redirect.id;


--
-- Name: wagtailsearch_editorspick; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailsearch_editorspick (
    id integer NOT NULL,
    sort_order integer,
    description text NOT NULL,
    page_id integer NOT NULL,
    query_id integer NOT NULL
);


ALTER TABLE public.wagtailsearch_editorspick OWNER TO postgres;

--
-- Name: wagtailsearch_editorspick_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailsearch_editorspick_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailsearch_editorspick_id_seq OWNER TO postgres;

--
-- Name: wagtailsearch_editorspick_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailsearch_editorspick_id_seq OWNED BY public.wagtailsearch_editorspick.id;


--
-- Name: wagtailsearch_query; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailsearch_query (
    id integer NOT NULL,
    query_string character varying(255) NOT NULL
);


ALTER TABLE public.wagtailsearch_query OWNER TO postgres;

--
-- Name: wagtailsearch_query_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailsearch_query_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailsearch_query_id_seq OWNER TO postgres;

--
-- Name: wagtailsearch_query_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailsearch_query_id_seq OWNED BY public.wagtailsearch_query.id;


--
-- Name: wagtailsearch_querydailyhits; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailsearch_querydailyhits (
    id integer NOT NULL,
    date date NOT NULL,
    hits integer NOT NULL,
    query_id integer NOT NULL
);


ALTER TABLE public.wagtailsearch_querydailyhits OWNER TO postgres;

--
-- Name: wagtailsearch_querydailyhits_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailsearch_querydailyhits_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailsearch_querydailyhits_id_seq OWNER TO postgres;

--
-- Name: wagtailsearch_querydailyhits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailsearch_querydailyhits_id_seq OWNED BY public.wagtailsearch_querydailyhits.id;


--
-- Name: wagtailusers_userprofile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wagtailusers_userprofile (
    id integer NOT NULL,
    submitted_notifications boolean NOT NULL,
    approved_notifications boolean NOT NULL,
    rejected_notifications boolean NOT NULL,
    user_id integer NOT NULL,
    preferred_language character varying(10) NOT NULL,
    current_time_zone character varying(40) NOT NULL,
    avatar character varying(100) NOT NULL,
    updated_comments_notifications boolean NOT NULL
);


ALTER TABLE public.wagtailusers_userprofile OWNER TO postgres;

--
-- Name: wagtailusers_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wagtailusers_userprofile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wagtailusers_userprofile_id_seq OWNER TO postgres;

--
-- Name: wagtailusers_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wagtailusers_userprofile_id_seq OWNED BY public.wagtailusers_userprofile.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: contact_formfield id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_formfield ALTER COLUMN id SET DEFAULT nextval('public.contact_formfield_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: taggit_tag id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_tag ALTER COLUMN id SET DEFAULT nextval('public.taggit_tag_id_seq'::regclass);


--
-- Name: taggit_taggeditem id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem ALTER COLUMN id SET DEFAULT nextval('public.taggit_taggeditem_id_seq'::regclass);


--
-- Name: wagtailadmin_admin id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailadmin_admin ALTER COLUMN id SET DEFAULT nextval('public.wagtailadmin_admin_id_seq'::regclass);


--
-- Name: wagtailcore_collection id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collection ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_collection_id_seq'::regclass);


--
-- Name: wagtailcore_collectionviewrestriction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_collectionviewrestriction_id_seq'::regclass);


--
-- Name: wagtailcore_collectionviewrestriction_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_collectionviewrestriction_groups_id_seq'::regclass);


--
-- Name: wagtailcore_comment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_comment_id_seq'::regclass);


--
-- Name: wagtailcore_commentreply id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_commentreply ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_commentreply_id_seq'::regclass);


--
-- Name: wagtailcore_groupapprovaltask_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_groupapprovaltask_groups_id_seq'::regclass);


--
-- Name: wagtailcore_groupcollectionpermission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_groupcollectionpermission_id_seq'::regclass);


--
-- Name: wagtailcore_grouppagepermission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_grouppagepermission_id_seq'::regclass);


--
-- Name: wagtailcore_locale id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_locale ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_locale_id_seq'::regclass);


--
-- Name: wagtailcore_page id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_page_id_seq'::regclass);


--
-- Name: wagtailcore_pagelogentry id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagelogentry ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pagelogentry_id_seq'::regclass);


--
-- Name: wagtailcore_pagerevision id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagerevision ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pagerevision_id_seq'::regclass);


--
-- Name: wagtailcore_pagesubscription id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pagesubscription_id_seq'::regclass);


--
-- Name: wagtailcore_pageviewrestriction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pageviewrestriction_id_seq'::regclass);


--
-- Name: wagtailcore_pageviewrestriction_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_pageviewrestriction_groups_id_seq'::regclass);


--
-- Name: wagtailcore_site id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_site ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_site_id_seq'::regclass);


--
-- Name: wagtailcore_task id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_task ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_task_id_seq'::regclass);


--
-- Name: wagtailcore_taskstate id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_taskstate_id_seq'::regclass);


--
-- Name: wagtailcore_workflow id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflow ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_workflow_id_seq'::regclass);


--
-- Name: wagtailcore_workflowstate id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_workflowstate_id_seq'::regclass);


--
-- Name: wagtailcore_workflowtask id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask ALTER COLUMN id SET DEFAULT nextval('public.wagtailcore_workflowtask_id_seq'::regclass);


--
-- Name: wagtaildocs_document id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_document ALTER COLUMN id SET DEFAULT nextval('public.wagtaildocs_document_id_seq'::regclass);


--
-- Name: wagtaildocs_uploadeddocument id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_uploadeddocument ALTER COLUMN id SET DEFAULT nextval('public.wagtaildocs_uploadeddocument_id_seq'::regclass);


--
-- Name: wagtailembeds_embed id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailembeds_embed ALTER COLUMN id SET DEFAULT nextval('public.wagtailembeds_embed_id_seq'::regclass);


--
-- Name: wagtailforms_formsubmission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailforms_formsubmission ALTER COLUMN id SET DEFAULT nextval('public.wagtailforms_formsubmission_id_seq'::regclass);


--
-- Name: wagtailimages_image id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_image ALTER COLUMN id SET DEFAULT nextval('public.wagtailimages_image_id_seq'::regclass);


--
-- Name: wagtailimages_rendition id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_rendition ALTER COLUMN id SET DEFAULT nextval('public.wagtailimages_rendition_id_seq'::regclass);


--
-- Name: wagtailimages_uploadedimage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_uploadedimage ALTER COLUMN id SET DEFAULT nextval('public.wagtailimages_uploadedimage_id_seq'::regclass);


--
-- Name: wagtailredirects_redirect id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect ALTER COLUMN id SET DEFAULT nextval('public.wagtailredirects_redirect_id_seq'::regclass);


--
-- Name: wagtailsearch_editorspick id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_editorspick ALTER COLUMN id SET DEFAULT nextval('public.wagtailsearch_editorspick_id_seq'::regclass);


--
-- Name: wagtailsearch_query id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_query ALTER COLUMN id SET DEFAULT nextval('public.wagtailsearch_query_id_seq'::regclass);


--
-- Name: wagtailsearch_querydailyhits id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_querydailyhits ALTER COLUMN id SET DEFAULT nextval('public.wagtailsearch_querydailyhits_id_seq'::regclass);


--
-- Name: wagtailusers_userprofile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailusers_userprofile ALTER COLUMN id SET DEFAULT nextval('public.wagtailusers_userprofile_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
1	Moderators
2	Editors
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	1
2	2	1
3	1	2
4	1	3
5	1	4
6	2	2
7	2	3
8	2	4
9	1	5
10	2	5
11	1	8
12	1	6
13	1	7
14	2	8
15	2	6
16	2	7
17	1	9
18	2	9
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can access Wagtail admin	3	access_admin
2	Can add document	5	add_document
3	Can change document	5	change_document
4	Can delete document	5	delete_document
5	Can choose document	5	choose_document
6	Can add image	6	add_image
7	Can change image	6	change_image
8	Can delete image	6	delete_image
9	Can choose image	6	choose_image
10	Can add home page	2	add_homepage
11	Can change home page	2	change_homepage
12	Can delete home page	2	delete_homepage
13	Can view home page	2	view_homepage
14	Can add form submission	7	add_formsubmission
15	Can change form submission	7	change_formsubmission
16	Can delete form submission	7	delete_formsubmission
17	Can view form submission	7	view_formsubmission
18	Can add redirect	8	add_redirect
19	Can change redirect	8	change_redirect
20	Can delete redirect	8	delete_redirect
21	Can view redirect	8	view_redirect
22	Can add embed	9	add_embed
23	Can change embed	9	change_embed
24	Can delete embed	9	delete_embed
25	Can view embed	9	view_embed
26	Can add user profile	10	add_userprofile
27	Can change user profile	10	change_userprofile
28	Can delete user profile	10	delete_userprofile
29	Can view user profile	10	view_userprofile
30	Can view document	5	view_document
31	Can add uploaded document	11	add_uploadeddocument
32	Can change uploaded document	11	change_uploadeddocument
33	Can delete uploaded document	11	delete_uploadeddocument
34	Can view uploaded document	11	view_uploadeddocument
35	Can view image	6	view_image
36	Can add rendition	12	add_rendition
37	Can change rendition	12	change_rendition
38	Can delete rendition	12	delete_rendition
39	Can view rendition	12	view_rendition
40	Can add uploaded image	13	add_uploadedimage
41	Can change uploaded image	13	change_uploadedimage
42	Can delete uploaded image	13	delete_uploadedimage
43	Can view uploaded image	13	view_uploadedimage
44	Can add query	14	add_query
45	Can change query	14	change_query
46	Can delete query	14	delete_query
47	Can view query	14	view_query
48	Can add Query Daily Hits	15	add_querydailyhits
49	Can change Query Daily Hits	15	change_querydailyhits
50	Can delete Query Daily Hits	15	delete_querydailyhits
51	Can view Query Daily Hits	15	view_querydailyhits
52	Can add page	1	add_page
53	Can change page	1	change_page
54	Can delete page	1	delete_page
55	Can view page	1	view_page
56	Can add group page permission	16	add_grouppagepermission
57	Can change group page permission	16	change_grouppagepermission
58	Can delete group page permission	16	delete_grouppagepermission
59	Can view group page permission	16	view_grouppagepermission
60	Can add page revision	17	add_pagerevision
61	Can change page revision	17	change_pagerevision
62	Can delete page revision	17	delete_pagerevision
63	Can view page revision	17	view_pagerevision
64	Can add page view restriction	18	add_pageviewrestriction
65	Can change page view restriction	18	change_pageviewrestriction
66	Can delete page view restriction	18	delete_pageviewrestriction
67	Can view page view restriction	18	view_pageviewrestriction
68	Can add site	19	add_site
69	Can change site	19	change_site
70	Can delete site	19	delete_site
71	Can view site	19	view_site
72	Can add collection	20	add_collection
73	Can change collection	20	change_collection
74	Can delete collection	20	delete_collection
75	Can view collection	20	view_collection
76	Can add group collection permission	21	add_groupcollectionpermission
77	Can change group collection permission	21	change_groupcollectionpermission
78	Can delete group collection permission	21	delete_groupcollectionpermission
79	Can view group collection permission	21	view_groupcollectionpermission
80	Can add collection view restriction	22	add_collectionviewrestriction
81	Can change collection view restriction	22	change_collectionviewrestriction
82	Can delete collection view restriction	22	delete_collectionviewrestriction
83	Can view collection view restriction	22	view_collectionviewrestriction
84	Can add task	23	add_task
85	Can change task	23	change_task
86	Can delete task	23	delete_task
87	Can view task	23	view_task
88	Can add Task state	24	add_taskstate
89	Can change Task state	24	change_taskstate
90	Can delete Task state	24	delete_taskstate
91	Can view Task state	24	view_taskstate
92	Can add workflow	25	add_workflow
93	Can change workflow	25	change_workflow
94	Can delete workflow	25	delete_workflow
95	Can view workflow	25	view_workflow
96	Can add Group approval task	4	add_groupapprovaltask
97	Can change Group approval task	4	change_groupapprovaltask
98	Can delete Group approval task	4	delete_groupapprovaltask
99	Can view Group approval task	4	view_groupapprovaltask
100	Can add Workflow state	26	add_workflowstate
101	Can change Workflow state	26	change_workflowstate
102	Can delete Workflow state	26	delete_workflowstate
103	Can view Workflow state	26	view_workflowstate
104	Can add workflow page	27	add_workflowpage
105	Can change workflow page	27	change_workflowpage
106	Can delete workflow page	27	delete_workflowpage
107	Can view workflow page	27	view_workflowpage
108	Can add workflow task order	28	add_workflowtask
109	Can change workflow task order	28	change_workflowtask
110	Can delete workflow task order	28	delete_workflowtask
111	Can view workflow task order	28	view_workflowtask
112	Can add page log entry	29	add_pagelogentry
113	Can change page log entry	29	change_pagelogentry
114	Can delete page log entry	29	delete_pagelogentry
115	Can view page log entry	29	view_pagelogentry
116	Can add locale	30	add_locale
117	Can change locale	30	change_locale
118	Can delete locale	30	delete_locale
119	Can view locale	30	view_locale
120	Can add comment	31	add_comment
121	Can change comment	31	change_comment
122	Can delete comment	31	delete_comment
123	Can view comment	31	view_comment
124	Can add comment reply	32	add_commentreply
125	Can change comment reply	32	change_commentreply
126	Can delete comment reply	32	delete_commentreply
127	Can view comment reply	32	view_commentreply
128	Can add page subscription	33	add_pagesubscription
129	Can change page subscription	33	change_pagesubscription
130	Can delete page subscription	33	delete_pagesubscription
131	Can view page subscription	33	view_pagesubscription
132	Can add tag	34	add_tag
133	Can change tag	34	change_tag
134	Can delete tag	34	delete_tag
135	Can view tag	34	view_tag
136	Can add tagged item	35	add_taggeditem
137	Can change tagged item	35	change_taggeditem
138	Can delete tagged item	35	delete_taggeditem
139	Can view tagged item	35	view_taggeditem
140	Can add log entry	36	add_logentry
141	Can change log entry	36	change_logentry
142	Can delete log entry	36	delete_logentry
143	Can view log entry	36	view_logentry
144	Can add permission	37	add_permission
145	Can change permission	37	change_permission
146	Can delete permission	37	delete_permission
147	Can view permission	37	view_permission
148	Can add group	38	add_group
149	Can change group	38	change_group
150	Can delete group	38	delete_group
151	Can view group	38	view_group
152	Can add user	39	add_user
153	Can change user	39	change_user
154	Can delete user	39	delete_user
155	Can view user	39	view_user
156	Can add content type	40	add_contenttype
157	Can change content type	40	change_contenttype
158	Can delete content type	40	delete_contenttype
159	Can view content type	40	view_contenttype
160	Can add session	41	add_session
161	Can change session	41	change_session
162	Can delete session	41	delete_session
163	Can view session	41	view_session
164	Can add about page	42	add_aboutpage
165	Can change about page	42	change_aboutpage
166	Can delete about page	42	delete_aboutpage
167	Can view about page	42	view_aboutpage
168	Can add technologies page	43	add_technologiespage
169	Can change technologies page	43	change_technologiespage
170	Can delete technologies page	43	delete_technologiespage
171	Can view technologies page	43	view_technologiespage
172	Can add form field	44	add_formfield
173	Can change form field	44	change_formfield
174	Can delete form field	44	delete_formfield
175	Can view form field	44	view_formfield
176	Can add contact page	45	add_contactpage
177	Can change contact page	45	change_contactpage
178	Can delete contact page	45	delete_contactpage
179	Can view contact page	45	view_contactpage
180	Can add blog listing page	46	add_bloglistingpage
181	Can change blog listing page	46	change_bloglistingpage
182	Can delete blog listing page	46	delete_bloglistingpage
183	Can view blog listing page	46	view_bloglistingpage
184	Can add blog detail page	47	add_blogdetailpage
185	Can change blog detail page	47	change_blogdetailpage
186	Can delete blog detail page	47	delete_blogdetailpage
187	Can view blog detail page	47	view_blogdetailpage
188	Can add projects listing page	48	add_projectslistingpage
189	Can change projects listing page	48	change_projectslistingpage
190	Can delete projects listing page	48	delete_projectslistingpage
191	Can view projects listing page	48	view_projectslistingpage
192	Can add projects detail page	49	add_projectsdetailpage
193	Can change projects detail page	49	change_projectsdetailpage
194	Can delete projects detail page	49	delete_projectsdetailpage
195	Can view projects detail page	49	view_projectsdetailpage
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$260000$HOsxiwzE94XAL7LhGyl4bs$RhuJfTetIrNvl6NRJq1Azqywdbd+LHWH4ABi7y4NgTs=	2021-09-29 10:20:01.901405+05:30	t	mdrift				t	t	2021-09-29 10:19:32.566496+05:30
2	pbkdf2_sha256$260000$Hr4CWLpXL0a4j9FpCFS54Y$zSKZcy5eQChMOFjKOlJHWBHNyziV3YUFrwNeej1Thr4=	\N	t	archana	Archana	PC	archanapc0@gmail.com	f	t	2021-09-29 13:22:14.175678+05:30
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
1	2	1
2	2	2
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: blog_blogdetailpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_blogdetailpage (page_ptr_id, header_title, meta_content, date, read_time, content, blog_detail_image_id, blog_list_image_id) FROM stdin;
8	How to increase the value of Business data	\N	2021-09-29	7 min read	[{"type": "full_richtext", "value": "<p data-block-key=\\"tuebo\\"> </p><p data-block-key=\\"2o0lv\\"><b>How to increase the value of Business data</b></p><p data-block-key=\\"4ul3\\">Since technology reforms every industry, organizations continue to formulate considerable investments. However, a lot of such investments fail to bring their agreed returns. The outcomes by analyzing various international companies to find out whether improved technology spending could direct enhanced monetary performance show no direct connection between technology investments and revenue growth. Spending on technology does not automatically direct to improved financial performance. This by itself is not a surprise, but the results show a relationship between technology and revenue development if the assets are focused on targeted abilities, boosted with the true operating model and execution skills.</p><p data-block-key=\\"bpbrh\\">To begin thinking more tactically about your data, visualize you\\u2019re on an analytics drive that takes you to some thrilling places. Every stop along the way will assist you to connect better value from the data produced by your automation systems, incorporating project resource planning, CRM, e-commerce etc. The accurate value of client data lies in its capability to give context. This can merely happen when that data is linked across silos.</p><p data-block-key=\\"fkvct\\">There are several ways that big data can produce revenue, stimulate product and service improvement, cut costs, advance operations as well as business models. The most frequent area is product innovation, yet the chief gains approach from business model innovation along with data monetization. They both will have a basic impact across an organization.</p><p data-block-key=\\"1bmng\\">Here are the areas to consider for transforming the performance which boosts revenue and customer satisfaction:</p><ul><li data-block-key=\\"6bsl7\\">Strategy</li></ul><embed alt=\\"Strategy.png\\" embedtype=\\"image\\" format=\\"left\\" id=\\"11\\"/><p data-block-key=\\"3ot67\\"></p><p data-block-key=\\"8tek0\\">The term Strategic analytics implies choices that are made, capital which is invested, and plans for data along with analytics that is generated based on the requirements of a business. Analytical solutions in the present business environment are fundamental since they let users think intentionally about how an organization constructs its core competencies and makes value. This not only notifies the whole process, saves time, effort and money, but also guides to value creation.</p><ul><li data-block-key=\\"6pb21\\">Sales Forecasting</li></ul><embed alt=\\"sales-forecasting.png\\" embedtype=\\"image\\" format=\\"left\\" id=\\"12\\"/><p data-block-key=\\"ea1ud\\"></p><p data-block-key=\\"e47pr\\">Companies can advance the exactness of their sales forecasts with predictive analytics. Predictive analytics can assist to expose styles and models in a company\\u2019s past sales data and monetary performance. Employing those styles and models, analysts can guess profits based on the company\\u2019s recent sales pipeline.</p><p data-block-key=\\"639r1\\">Fresh companies with small or no historical monetary data can also forecast with predictive analytics. In these cases, the predictive models frequently rely on diverse types of data like industry sales statistics, economic indicators, market demographics, or the financial performance of parallel organizations.</p><ul><li data-block-key=\\"72vv7\\">Improved Advertising</li></ul><embed alt=\\"Improved-Advertsing.png\\" embedtype=\\"image\\" format=\\"left\\" id=\\"13\\"/><p data-block-key=\\"9q4j8\\"></p><p data-block-key=\\"7l094\\">All advertisement is A/B and even C split-tested. Every landing page, pop-ups, and even product pictures are reviewed for their efficiency with tweaks being made to guarantee the highest results. Yet the placing of products on the website is calculated to recognize the finest location to assist drive engagement and sales.</p><p data-block-key=\\"2ek6a\\">Advertising can be costly, and it\\u2019s significant to know how to get the greatest return on investment.</p><ul><li data-block-key=\\"301p2\\">Measure everything</li></ul><embed alt=\\"Measure-everything.png\\" embedtype=\\"image\\" format=\\"left\\" id=\\"14\\"/><p data-block-key=\\"boo6v\\"></p><p data-block-key=\\"off\\">Without measuring each part of your sales analytics efforts, you will not have the clue whether it\\u2019s functioning or not to help to make improved business choices. This denotes that everything ought to be computed adjacent to baselines and end goals.</p><p data-block-key=\\"18ut3\\">In calculating different parts of your sales procedure, you will be better situated to solve what works and what doesn\\u2019t. You can spot specific features that positively influence your bottom line and others that may be damaging it. You will get a better idea of how to best assign future resources.</p><ul><li data-block-key=\\"ekinr\\">Optimize Marketing Strategies</li></ul><embed alt=\\"optimize-marketing-strategies.png\\" embedtype=\\"image\\" format=\\"left\\" id=\\"15\\"/><p data-block-key=\\"ev8mg\\"></p><p data-block-key=\\"2qk6h\\">Organizations can boost their outcome by exploiting the return on their marketing investments. The rising number of companies is gathering data from their marketing attempts and applying predictive analytics to better understand their clientele and how to interact with them successfully.</p><p data-block-key=\\"9ehqv\\">Marketers these days can gather important data about customers through website analytics tools, social media activities, online forms and surveys etc. Companies can also record consumer purchase patterns themselves or buy comparable data from third-party resources. By applying predictive analytics, sellers can change this data into priceless insights, like who their most hopeful leads are, and who in their market is likely to buy a specific product or service.</p><p data-block-key=\\"bi1ma\\">With this information, organizations can aim for different parts in their target market with more efficient, modified messaging. Predictive analytics can likewise illustrate you which marketing campaigns and channels are successful at making sales. Eventually, this can aid you to assign more of your marketing funds to efforts that produce a higher return.</p><ul><li data-block-key=\\"1ks72\\">Technical ability to execute</li></ul><embed alt=\\"Technical-ability-to-execute.png\\" embedtype=\\"image\\" format=\\"left\\" id=\\"16\\"/><p data-block-key=\\"3m4bs\\"></p><p data-block-key=\\"1jthn\\">One of the key priorities is the skill to deliver difficult projects. As companies in every industry look for altering themselves through technology, the aptitude to execute large-scale IT plans will turn out to be critical. By winning organizations to recognize the skills required for executing their technology plan, they can compose a truthful evaluation of their present strengths and weaknesses.</p><p data-block-key=\\"f8t2s\\">In the end, numerous companies need a technology road map that holds their general strategy. This road map is not simply a sequencing diagram for how a company\\u2019s existing initiatives are likely to cooperate in the future, yet rather a description of future abilities, the technology style that is necessary to stand up these abilities.</p><ul><li data-block-key=\\"7co71\\">Retain Customers</li></ul><p data-block-key=\\"30ddd\\">Predictive analytics can assist you not only draw new business but also aid you retain the customers you attract and transform your first-time sales into a frequent income.</p><p data-block-key=\\"2emta\\">To apply predictive analytics for customer retention, organizations initially want to gather data on their consumers, including information about the products and services a client purchased, demographic and geographic data, and if they are first-time or frequent clients. If you know which clients are more likely to revisit, you can target them with personalized marketing campaigns, such as discount offers or definite product or service proposals. Giving personalized messages can assist foster faithfulness and remain customers away from your opponents.</p><p data-block-key=\\"dpqpb\\">Companies are becoming more advanced and data-driven than earlier. Predictive analytics has been demonstrated to have a major positive impact on an organization\\u2019s financial performance. Moreover, several companies that have already implemented predictive analytics report have helped them to increase an edge against their struggle. Progressed analytical approaches that can deal with the top priority opportunities are by far the fastest path to worth over the short term. This is factual in the parts of client management, operation and supply chain management, risk management etc. When you catch time to dig up into the details of your operations, you can find its incredible better opportunities as well as tools for excellent data analytics!</p>", "id": "7b4b2b4e-b11a-459c-a61f-0150478f6df3"}]	10	10
9	new post	\N	2021-09-28	7 min read	[{"type": "full_richtext", "value": "<p data-block-key=\\"1tdlm\\">new post</p>", "id": "803a8f3d-986b-4273-bd88-ae6b073f454c"}]	16	15
22	\N	\N	2021-09-01	7 min read	[{"type": "full_richtext", "value": "<p data-block-key=\\"2t0p4\\">new post 3</p>", "id": "55601ad1-f160-46bc-9620-83ca20cb74d2"}]	131	126
\.


--
-- Data for Name: blog_bloglistingpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_bloglistingpage (page_ptr_id, header_title, meta_content) FROM stdin;
7	Blogs	\N
\.


--
-- Data for Name: contact_contactpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.contact_contactpage (page_ptr_id, to_address, from_address, subject, intro, thank_you_text) FROM stdin;
6	ranjuranjitha.1997@gmail.com	archanapc08@gmail.com	contact form submission		<p data-block-key="tvshm">Thank you</p>
\.


--
-- Data for Name: contact_formfield; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.contact_formfield (id, sort_order, clean_name, label, field_type, required, choices, default_value, help_text, page_id) FROM stdin;
1	0	your_name	Your Name	singleline	t			Name	6
2	1	your_company	Your company	singleline	t			Company name	6
3	2	your_email_address	Your email address	singleline	t			E-mail	6
4	3	when_do_you_want_to_start	When do you want to start?	singleline	t			Select date	6
5	4	what_is_your_budget	What is your budget	singleline	t			Budget	6
6	5	describe_your_needs_the_more_we_know_the_better	Describe your needs, the more we know, the better	multiline	t			Describe your needs here	6
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	wagtailcore	page
2	home	homepage
3	wagtailadmin	admin
4	wagtailcore	groupapprovaltask
5	wagtaildocs	document
6	wagtailimages	image
7	wagtailforms	formsubmission
8	wagtailredirects	redirect
9	wagtailembeds	embed
10	wagtailusers	userprofile
11	wagtaildocs	uploadeddocument
12	wagtailimages	rendition
13	wagtailimages	uploadedimage
14	wagtailsearch	query
15	wagtailsearch	querydailyhits
16	wagtailcore	grouppagepermission
17	wagtailcore	pagerevision
18	wagtailcore	pageviewrestriction
19	wagtailcore	site
20	wagtailcore	collection
21	wagtailcore	groupcollectionpermission
22	wagtailcore	collectionviewrestriction
23	wagtailcore	task
24	wagtailcore	taskstate
25	wagtailcore	workflow
26	wagtailcore	workflowstate
27	wagtailcore	workflowpage
28	wagtailcore	workflowtask
29	wagtailcore	pagelogentry
30	wagtailcore	locale
31	wagtailcore	comment
32	wagtailcore	commentreply
33	wagtailcore	pagesubscription
34	taggit	tag
35	taggit	taggeditem
36	admin	logentry
37	auth	permission
38	auth	group
39	auth	user
40	contenttypes	contenttype
41	sessions	session
42	home	aboutpage
43	home	technologiespage
44	contact	formfield
45	contact	contactpage
46	blog	bloglistingpage
47	blog	blogdetailpage
48	project	projectslistingpage
49	project	projectsdetailpage
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-09-29 10:18:49.70998+05:30
2	auth	0001_initial	2021-09-29 10:18:50.08684+05:30
3	admin	0001_initial	2021-09-29 10:18:50.169936+05:30
4	admin	0002_logentry_remove_auto_add	2021-09-29 10:18:50.186762+05:30
5	admin	0003_logentry_add_action_flag_choices	2021-09-29 10:18:50.207649+05:30
6	contenttypes	0002_remove_content_type_name	2021-09-29 10:18:50.250019+05:30
7	auth	0002_alter_permission_name_max_length	2021-09-29 10:18:50.268259+05:30
8	auth	0003_alter_user_email_max_length	2021-09-29 10:18:50.286407+05:30
9	auth	0004_alter_user_username_opts	2021-09-29 10:18:50.30313+05:30
10	auth	0005_alter_user_last_login_null	2021-09-29 10:18:50.321756+05:30
11	auth	0006_require_contenttypes_0002	2021-09-29 10:18:50.326359+05:30
12	auth	0007_alter_validators_add_error_messages	2021-09-29 10:18:50.34389+05:30
13	auth	0008_alter_user_username_max_length	2021-09-29 10:18:50.391033+05:30
14	auth	0009_alter_user_last_name_max_length	2021-09-29 10:18:50.410696+05:30
15	auth	0010_alter_group_name_max_length	2021-09-29 10:18:50.433378+05:30
16	auth	0011_update_proxy_permissions	2021-09-29 10:18:50.455523+05:30
17	auth	0012_alter_user_first_name_max_length	2021-09-29 10:18:50.474317+05:30
18	wagtailcore	0001_initial	2021-09-29 10:18:51.11991+05:30
19	wagtailcore	0002_initial_data	2021-09-29 10:18:51.122468+05:30
20	wagtailcore	0003_add_uniqueness_constraint_on_group_page_permission	2021-09-29 10:18:51.124459+05:30
21	wagtailcore	0004_page_locked	2021-09-29 10:18:51.12646+05:30
22	wagtailcore	0005_add_page_lock_permission_to_moderators	2021-09-29 10:18:51.129734+05:30
23	wagtailcore	0006_add_lock_page_permission	2021-09-29 10:18:51.132359+05:30
24	wagtailcore	0007_page_latest_revision_created_at	2021-09-29 10:18:51.135769+05:30
25	wagtailcore	0008_populate_latest_revision_created_at	2021-09-29 10:18:51.138452+05:30
26	wagtailcore	0009_remove_auto_now_add_from_pagerevision_created_at	2021-09-29 10:18:51.139842+05:30
27	wagtailcore	0010_change_page_owner_to_null_on_delete	2021-09-29 10:18:51.142466+05:30
28	wagtailcore	0011_page_first_published_at	2021-09-29 10:18:51.145182+05:30
29	wagtailcore	0012_extend_page_slug_field	2021-09-29 10:18:51.147185+05:30
30	wagtailcore	0013_update_golive_expire_help_text	2021-09-29 10:18:51.149834+05:30
31	wagtailcore	0014_add_verbose_name	2021-09-29 10:18:51.152365+05:30
32	wagtailcore	0015_add_more_verbose_names	2021-09-29 10:18:51.156199+05:30
33	wagtailcore	0016_change_page_url_path_to_text_field	2021-09-29 10:18:51.158487+05:30
34	wagtailcore	0017_change_edit_page_permission_description	2021-09-29 10:18:51.18561+05:30
35	wagtailcore	0018_pagerevision_submitted_for_moderation_index	2021-09-29 10:18:51.222161+05:30
36	wagtailcore	0019_verbose_names_cleanup	2021-09-29 10:18:51.311824+05:30
37	wagtailcore	0020_add_index_on_page_first_published_at	2021-09-29 10:18:51.352701+05:30
38	wagtailcore	0021_capitalizeverbose	2021-09-29 10:18:51.91889+05:30
39	wagtailcore	0022_add_site_name	2021-09-29 10:18:51.957443+05:30
40	wagtailcore	0023_alter_page_revision_on_delete_behaviour	2021-09-29 10:18:51.984128+05:30
41	wagtailcore	0024_collection	2021-09-29 10:18:52.052219+05:30
42	wagtailcore	0025_collection_initial_data	2021-09-29 10:18:52.085107+05:30
43	wagtailcore	0026_group_collection_permission	2021-09-29 10:18:52.213759+05:30
44	wagtailcore	0027_fix_collection_path_collation	2021-09-29 10:18:52.276837+05:30
45	wagtailcore	0024_alter_page_content_type_on_delete_behaviour	2021-09-29 10:18:52.308593+05:30
46	wagtailcore	0028_merge	2021-09-29 10:18:52.315537+05:30
47	wagtailcore	0029_unicode_slugfield_dj19	2021-09-29 10:18:52.342311+05:30
48	wagtailcore	0030_index_on_pagerevision_created_at	2021-09-29 10:18:52.389495+05:30
49	wagtailcore	0031_add_page_view_restriction_types	2021-09-29 10:18:52.552022+05:30
50	wagtailcore	0032_add_bulk_delete_page_permission	2021-09-29 10:18:52.586274+05:30
51	wagtailcore	0033_remove_golive_expiry_help_text	2021-09-29 10:18:52.644644+05:30
52	wagtailcore	0034_page_live_revision	2021-09-29 10:18:52.701601+05:30
53	wagtailcore	0035_page_last_published_at	2021-09-29 10:18:52.740193+05:30
54	wagtailcore	0036_populate_page_last_published_at	2021-09-29 10:18:52.780986+05:30
55	wagtailcore	0037_set_page_owner_editable	2021-09-29 10:18:52.816744+05:30
56	wagtailcore	0038_make_first_published_at_editable	2021-09-29 10:18:52.845415+05:30
57	wagtailcore	0039_collectionviewrestriction	2021-09-29 10:18:52.996445+05:30
58	wagtailcore	0040_page_draft_title	2021-09-29 10:18:53.052742+05:30
59	home	0001_initial	2021-09-29 10:18:53.104462+05:30
60	home	0002_create_homepage	2021-09-29 10:18:53.169919+05:30
61	sessions	0001_initial	2021-09-29 10:18:53.234644+05:30
62	taggit	0001_initial	2021-09-29 10:18:53.483248+05:30
63	taggit	0002_auto_20150616_2121	2021-09-29 10:18:53.515508+05:30
64	taggit	0003_taggeditem_add_unique_index	2021-09-29 10:18:53.547401+05:30
65	wagtailadmin	0001_create_admin_access_permissions	2021-09-29 10:18:53.622055+05:30
66	wagtailadmin	0002_admin	2021-09-29 10:18:53.631317+05:30
67	wagtailadmin	0003_admin_managed	2021-09-29 10:18:53.66254+05:30
68	wagtailcore	0041_group_collection_permissions_verbose_name_plural	2021-09-29 10:18:53.702277+05:30
69	wagtailcore	0042_index_on_pagerevision_approved_go_live_at	2021-09-29 10:18:53.748292+05:30
70	wagtailcore	0043_lock_fields	2021-09-29 10:18:53.827356+05:30
71	wagtailcore	0044_add_unlock_grouppagepermission	2021-09-29 10:18:53.855973+05:30
72	wagtailcore	0045_assign_unlock_grouppagepermission	2021-09-29 10:18:53.914419+05:30
73	wagtailcore	0046_site_name_remove_null	2021-09-29 10:18:53.939714+05:30
74	wagtailcore	0047_add_workflow_models	2021-09-29 10:18:54.630162+05:30
75	wagtailcore	0048_add_default_workflows	2021-09-29 10:18:54.731219+05:30
76	wagtailcore	0049_taskstate_finished_by	2021-09-29 10:18:54.800307+05:30
77	wagtailcore	0050_workflow_rejected_to_needs_changes	2021-09-29 10:18:54.908918+05:30
78	wagtailcore	0051_taskstate_comment	2021-09-29 10:18:54.960502+05:30
79	wagtailcore	0052_pagelogentry	2021-09-29 10:18:55.121368+05:30
80	wagtailcore	0053_locale_model	2021-09-29 10:18:55.174246+05:30
81	wagtailcore	0054_initial_locale	2021-09-29 10:18:55.228839+05:30
82	wagtailcore	0055_page_locale_fields	2021-09-29 10:18:55.375171+05:30
83	wagtailcore	0056_page_locale_fields_populate	2021-09-29 10:18:55.538529+05:30
84	wagtailcore	0057_page_locale_fields_notnull	2021-09-29 10:18:55.647633+05:30
85	wagtailcore	0058_page_alias_of	2021-09-29 10:18:55.714422+05:30
86	wagtailcore	0059_apply_collection_ordering	2021-09-29 10:18:55.772892+05:30
87	wagtailcore	0060_fix_workflow_unique_constraint	2021-09-29 10:18:55.855538+05:30
88	wagtailcore	0061_change_promote_tab_helpt_text_and_verbose_names	2021-09-29 10:18:55.92948+05:30
89	wagtailcore	0062_comment_models_and_pagesubscription	2021-09-29 10:18:56.324255+05:30
90	wagtaildocs	0001_initial	2021-09-29 10:18:56.431762+05:30
91	wagtaildocs	0002_initial_data	2021-09-29 10:18:56.527598+05:30
92	wagtaildocs	0003_add_verbose_names	2021-09-29 10:18:56.698953+05:30
93	wagtaildocs	0004_capitalizeverbose	2021-09-29 10:18:56.906613+05:30
94	wagtaildocs	0005_document_collection	2021-09-29 10:18:57.091834+05:30
95	wagtaildocs	0006_copy_document_permissions_to_collections	2021-09-29 10:18:57.175694+05:30
96	wagtaildocs	0005_alter_uploaded_by_user_on_delete_action	2021-09-29 10:18:57.231449+05:30
97	wagtaildocs	0007_merge	2021-09-29 10:18:57.236324+05:30
98	wagtaildocs	0008_document_file_size	2021-09-29 10:18:57.286511+05:30
99	wagtaildocs	0009_document_verbose_name_plural	2021-09-29 10:18:57.347019+05:30
100	wagtaildocs	0010_document_file_hash	2021-09-29 10:18:57.421347+05:30
101	wagtaildocs	0011_add_choose_permissions	2021-09-29 10:18:57.730346+05:30
102	wagtaildocs	0012_uploadeddocument	2021-09-29 10:18:57.945977+05:30
103	wagtailembeds	0001_initial	2021-09-29 10:18:58.049011+05:30
104	wagtailembeds	0002_add_verbose_names	2021-09-29 10:18:58.068359+05:30
105	wagtailembeds	0003_capitalizeverbose	2021-09-29 10:18:58.080003+05:30
106	wagtailembeds	0004_embed_verbose_name_plural	2021-09-29 10:18:58.093851+05:30
107	wagtailembeds	0005_specify_thumbnail_url_max_length	2021-09-29 10:18:58.107989+05:30
108	wagtailembeds	0006_add_embed_hash	2021-09-29 10:18:58.121449+05:30
109	wagtailembeds	0007_populate_hash	2021-09-29 10:18:58.269143+05:30
110	wagtailembeds	0008_allow_long_urls	2021-09-29 10:18:58.384108+05:30
111	wagtailembeds	0009_embed_cache_until	2021-09-29 10:18:58.444436+05:30
112	wagtailforms	0001_initial	2021-09-29 10:18:58.649679+05:30
113	wagtailforms	0002_add_verbose_names	2021-09-29 10:18:58.795105+05:30
114	wagtailforms	0003_capitalizeverbose	2021-09-29 10:18:58.909814+05:30
115	wagtailforms	0004_add_verbose_name_plural	2021-09-29 10:18:59.004408+05:30
116	wagtailimages	0001_initial	2021-09-29 10:18:59.609621+05:30
117	wagtailimages	0002_initial_data	2021-09-29 10:18:59.61361+05:30
118	wagtailimages	0003_fix_focal_point_fields	2021-09-29 10:18:59.617024+05:30
119	wagtailimages	0004_make_focal_point_key_not_nullable	2021-09-29 10:18:59.620049+05:30
120	wagtailimages	0005_make_filter_spec_unique	2021-09-29 10:18:59.623106+05:30
121	wagtailimages	0006_add_verbose_names	2021-09-29 10:18:59.627095+05:30
122	wagtailimages	0007_image_file_size	2021-09-29 10:18:59.631114+05:30
123	wagtailimages	0008_image_created_at_index	2021-09-29 10:18:59.63353+05:30
124	wagtailimages	0009_capitalizeverbose	2021-09-29 10:18:59.637267+05:30
125	wagtailimages	0010_change_on_delete_behaviour	2021-09-29 10:18:59.641724+05:30
126	wagtailimages	0011_image_collection	2021-09-29 10:18:59.645248+05:30
127	wagtailimages	0012_copy_image_permissions_to_collections	2021-09-29 10:18:59.649242+05:30
128	wagtailimages	0013_make_rendition_upload_callable	2021-09-29 10:18:59.653673+05:30
129	wagtailimages	0014_add_filter_spec_field	2021-09-29 10:18:59.657801+05:30
130	wagtailimages	0015_fill_filter_spec_field	2021-09-29 10:18:59.660792+05:30
131	wagtailimages	0016_deprecate_rendition_filter_relation	2021-09-29 10:18:59.663785+05:30
132	wagtailimages	0017_reduce_focal_point_key_max_length	2021-09-29 10:18:59.667689+05:30
133	wagtailimages	0018_remove_rendition_filter	2021-09-29 10:18:59.671647+05:30
134	wagtailimages	0019_delete_filter	2021-09-29 10:18:59.675743+05:30
135	wagtailimages	0020_add-verbose-name	2021-09-29 10:18:59.678562+05:30
136	wagtailimages	0021_image_file_hash	2021-09-29 10:18:59.683089+05:30
137	wagtailimages	0022_uploadedimage	2021-09-29 10:19:00.069266+05:30
138	wagtailimages	0023_add_choose_permissions	2021-09-29 10:19:00.320389+05:30
139	wagtailredirects	0001_initial	2021-09-29 10:19:00.567599+05:30
140	wagtailredirects	0002_add_verbose_names	2021-09-29 10:19:00.772935+05:30
141	wagtailredirects	0003_make_site_field_editable	2021-09-29 10:19:00.881572+05:30
142	wagtailredirects	0004_set_unique_on_path_and_site	2021-09-29 10:19:01.020088+05:30
143	wagtailredirects	0005_capitalizeverbose	2021-09-29 10:19:01.366721+05:30
144	wagtailredirects	0006_redirect_increase_max_length	2021-09-29 10:19:01.431341+05:30
145	wagtailsearch	0001_initial	2021-09-29 10:19:01.746186+05:30
146	wagtailsearch	0002_add_verbose_names	2021-09-29 10:19:01.987463+05:30
147	wagtailsearch	0003_remove_editors_pick	2021-09-29 10:19:02.072008+05:30
148	wagtailsearch	0004_querydailyhits_verbose_name_plural	2021-09-29 10:19:02.080754+05:30
149	wagtailusers	0001_initial	2021-09-29 10:19:02.180647+05:30
150	wagtailusers	0002_add_verbose_name_on_userprofile	2021-09-29 10:19:02.303278+05:30
151	wagtailusers	0003_add_verbose_names	2021-09-29 10:19:02.352249+05:30
152	wagtailusers	0004_capitalizeverbose	2021-09-29 10:19:02.548431+05:30
153	wagtailusers	0005_make_related_name_wagtail_specific	2021-09-29 10:19:02.632055+05:30
154	wagtailusers	0006_userprofile_prefered_language	2021-09-29 10:19:02.678295+05:30
155	wagtailusers	0007_userprofile_current_time_zone	2021-09-29 10:19:02.724644+05:30
156	wagtailusers	0008_userprofile_avatar	2021-09-29 10:19:02.771771+05:30
157	wagtailusers	0009_userprofile_verbose_name_plural	2021-09-29 10:19:02.814581+05:30
158	wagtailusers	0010_userprofile_updated_comments_notifications	2021-09-29 10:19:02.855766+05:30
159	wagtailimages	0001_squashed_0021	2021-09-29 10:19:02.865787+05:30
160	wagtailcore	0001_squashed_0016_change_page_url_path_to_text_field	2021-09-29 10:19:02.867951+05:30
161	home	0003_auto_20210929_1023	2021-09-29 10:23:08.066137+05:30
162	contact	0001_initial	2021-09-29 10:34:28.835411+05:30
163	blog	0001_initial	2021-09-29 10:49:13.576402+05:30
164	project	0001_initial	2021-09-29 11:00:06.256377+05:30
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
qhyyzw8s7r8e5jzq8m2jw0uv3v96sc0c	.eJxVjEEOwiAQRe_C2hCGUggu3XsGMjNMpWogKe3KeHdt0oVu_3vvv1TCbS1p67KkOauzAnX63Qj5IXUH-Y711jS3ui4z6V3RB-362rI8L4f7d1Cwl28drFiMjsGwCzmbHAhQvBMiB16stc6PkSVOBGMwhgYPxg4TYCTDcVDvD-FMN4M:1mVRXd:1pg3hqRvKBTS16yUQBQ3yEUK2wIsuFHEiFb-ZyJoBrM	2021-10-13 10:20:01.903378+05:30
\.


--
-- Data for Name: home_aboutpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.home_aboutpage (page_ptr_id, header_title, meta_content) FROM stdin;
4	About Us	\N
\.


--
-- Data for Name: home_homepage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.home_homepage (page_ptr_id, client_logos, header_title, meta_content) FROM stdin;
3	[{"type": "image", "value": 1, "id": "35e51b42-87fd-4bea-b7e0-d7fee97cb17a"}, {"type": "image", "value": 2, "id": "524807e9-44b6-4bdc-94c3-58496732bc5d"}, {"type": "image", "value": 3, "id": "b4722ddf-098f-43ad-902e-dc8693d9717b"}, {"type": "image", "value": 4, "id": "fe72d63c-b558-466e-946c-52e893513bdd"}, {"type": "image", "value": 5, "id": "daceb825-976b-441e-a89b-7485b91e022f"}, {"type": "image", "value": 6, "id": "e3ab0cb5-f0a4-405d-bef8-b564dadc3d5e"}, {"type": "image", "value": 7, "id": "6a68aff0-5565-40f5-8706-126d4af5a019"}, {"type": "image", "value": 8, "id": "9fdcab0f-65ad-4173-8948-a23a74469234"}, {"type": "image", "value": 9, "id": "bf4489a9-f5f5-4d8f-ae40-7e67489adcf5"}]	Home	\N
\.


--
-- Data for Name: home_technologiespage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.home_technologiespage (page_ptr_id, header_title, meta_content) FROM stdin;
5	Technologies	\N
\.


--
-- Data for Name: project_projectsdetailpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.project_projectsdetailpage (page_ptr_id, header_title, meta_content, right_content_heading, right_content, timeline, platform, deliverables, section1_content, section2_content, project_list_heading, project_list_title, project_list_subtitle, nextpage_heading, nextpage_title, nextpage_subtitle, nextpage_url, bg_color_id, nextpage_bg_image_id, nextpage_logo_id, project_list_bg_image_id, project_list_logo_id, section1_image_id, section2_image_id) FROM stdin;
12	ISP	\N	User and Billing Management application for ISPs	[{"type": "full_richtext", "value": "<p data-block-key=\\"ov8d8\\"> Metis Subscriber Management System (SMS) and billing platform was developed for the largest broadband and cable TV distribution network of South India, Kerala Vision. Platform provided unique facilities to Internet Service Providers (ISPs) such as multi level partner system management, modular billing, marketing and promotional campaign management modules etc </p>", "id": "cc520515-459e-4d4e-9735-a14282e69067"}]	4 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"z4jy2\\"> Web Application, Mobile Apllication, Enterprise Integrations </p>", "id": "25299355-c4b7-4505-a8aa-0659f6ffa100"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"1me9v\\"> Business process consulting; User experience design; Application Development; Partner Training Programs, Deployment and Support. </p>", "id": "5134b6e2-e028-40c4-b653-e5f9cd4e64f3"}]	[{"type": "image", "value": 31, "id": "ee3df15e-5b5b-4c4e-9ecd-46fdc06cc417"}, {"type": "full_richtext", "value": "<p data-block-key=\\"zahrr\\"> </p><h3 data-block-key=\\"4mo3i\\"><b>Multi Tier Partner Management</b></h3><p data-block-key=\\"bruca\\">Application enables the central ISP to create, manage and serve its multi layer distribution network. Various users are present in the system according to various functions and hierarchy within the system</p>", "id": "8fec84e3-2e66-48c2-b629-890e7444ae90"}, {"type": "full_richtext", "value": "<p data-block-key=\\"zahrr\\"> </p><h3 data-block-key=\\"sbga\\"><b>Subscriber Management</b></h3><p data-block-key=\\"5o4r3\\">Application manages the entire lifecycle of an end-user acquired by the ISP. A subscriber\\u2019s recharges, plan changes, static IP allocation, auto login configurations, issue tracking and resolution etc are handled through various workflows within the application.</p>", "id": "19f05b27-2b05-467a-9599-3d025cbb45b2"}, {"type": "image", "value": 32, "id": "8d645b88-d4f5-4b80-b9f5-28e464898002"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"kpafw\\"> </p><h3 data-block-key=\\"e62ih\\"><b>Billing Management</b></h3><p data-block-key=\\"7kmca\\">Application also manages the billing, payment collection and revenue share functionalities for the ISP network. From billing and account management of individual subscribers to managing the overall ledger for the parent company, the application is designed to handle the entire billing system with multiple integrations into third party applications and services used by Kerala Vision.</p>", "id": "43935166-0de0-4c5b-a56a-57a8ee041070"}, {"type": "image", "value": 34, "id": "74e1593a-1a31-4924-8793-ae2fa99be117"}, {"type": "image", "value": 35, "id": "df77ec79-47d6-42af-83f4-28f1fdb5f857"}, {"type": "full_richtext", "value": "<p data-block-key=\\"kpafw\\"> </p><h3 data-block-key=\\"5j3je\\"><b>Self-Service portal</b></h3><p data-block-key=\\"6emch\\">Subscribers have their own self service portal and mobile application to not only monitor their account but also configure their plans, accounts and subscriptions of their choice.</p>", "id": "f73c58a0-1ef5-44fa-9214-d1d65d87f8a2"}, {"type": "full_richtext", "value": "<p data-block-key=\\"kpafw\\"> </p><h3 data-block-key=\\"clip4\\"><b>Extensive Role and Permission management</b></h3><p data-block-key=\\"63ms8\\">Highly configurable and secure role and permission management system is implemented in the application to enable the admin to create users with specific access into the system without any technical modification or interventions.</p>", "id": "2610c6a5-71a8-40ed-8955-7bc715a3fb19"}, {"type": "image", "value": 36, "id": "99569e46-c0e9-4bdb-9e9b-e8ceafa10929"}, {"type": "image", "value": 37, "id": "14962c68-a987-471c-adb5-ad37f734a28b"}, {"type": "full_richtext", "value": "<p data-block-key=\\"kpafw\\"> </p><h3 data-block-key=\\"6omsj\\"><b>Payment Gateway</b></h3><p data-block-key=\\"d2sql\\">Multiple payment gateway providers are integrated into the system to facilitate easy purchase and checkout for the customers while providing the necessary reporting and financial management tools for the management.</p>", "id": "006c390d-0524-43b5-80e4-1c45aec8ac6d"}, {"type": "full_richtext", "value": "<p data-block-key=\\"kpafw\\"> </p><h3 data-block-key=\\"fn8kn\\"><b>Automated Workflows</b></h3><p data-block-key=\\"d95o9\\">mDrift has automated most of the processes in the system, enabling the client to operate the network with minimal staff. Workflows such as revenue share, recharges, plan-changes, billing, issue resolution, account deactivation due to unauthorised activities etc are performed by the system automatically. Admin interventions are carefully designed to be crucial to monitoring and maintaining the system while being non-intrusive to provide the best experience to end-users.</p>", "id": "2f90a8c5-6e05-43fa-9519-b9925754aa0b"}, {"type": "image", "value": 38, "id": "031db736-88d3-4f1a-8aeb-72b5ad1d0e3f"}]	user management and billing application for internet service providers	web application / mobile application / enterprise integrations	Large Private Organisation	news website for visually challenged persons	web application / mobile application	Government Organisation	http://127.0.0.1:8000/projects/8sidor	29	39	40	27	28	30	33
11	Transafe	\N	Anti Money Laundering platform	[{"type": "full_richtext", "value": "<p data-block-key=\\"tz7wc\\">AML platform connects the Retails Banks and Non Banking Financial Institutes (NBFIs) with Financial Intelligence Centers (FICs) run under economic ministry of a country. Daily transaction reports are collected and analysed in real time to provide predictive AML analysis and transaction history investigation.</p>", "id": "3010b82d-4d86-4a6f-a5c4-1c2510db1a76"}]	6 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"9fqi6\\">Web application</p>", "id": "a30ce3e1-73a3-43ab-8f5a-cca8bd58d184"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"wlu3o\\">Business process consulting; User experience design; Application Development; Deployment and Support</p>", "id": "f8ed0dec-af12-4318-a26e-c08a5a78b788"}]	[{"type": "image", "value": 21, "id": "82186a6b-e652-44e9-8783-d5c958363401"}, {"type": "full_richtext", "value": "<p data-block-key=\\"eteko\\"></p><h3 data-block-key=\\"6pvoj\\"><b>Data Collection</b></h3><p data-block-key=\\"97h3n\\">Daily transactional data from banks and NBFIs are collected into the system via Financial Institute portal. User accounts and special workflows are implemented in the system for financial institutes to ensure data accuracy and legal compliance.</p>", "id": "9340adbc-5e03-4558-937f-e4816a75016a"}, {"type": "full_richtext", "value": "<p data-block-key=\\"eteko\\"></p><h3 data-block-key=\\"dq19k\\"><b>Compliance Validation</b></h3><p data-block-key=\\"8mkpl\\">Application validates all uploaded transactional data with extensive rules customisable by FIC admins. Validation rules are configurable for specific report types such as Cash Transaction Report (CTRs), Electronic Transaction Report (ETR), Suspicious Transaction Report (STR) etc.</p>", "id": "647da9ad-0acc-4c4c-a819-2b175a6a4d1c"}, {"type": "image", "value": 22, "id": "f8ddcb75-78d4-4fff-bc28-f40c317957f4"}]	[{"type": "image", "value": 24, "id": "b6b29310-2c79-4e49-b7a2-ab67e007b69c"}, {"type": "full_richtext", "value": "<p data-block-key=\\"vi43n\\"></p><h3 data-block-key=\\"ctgne\\"><b>Link Analysis</b></h3><p data-block-key=\\"dgvsm\\">Analysts at FICs can search for transactions within the system and link analysis the cash flow between multiple persons and accounts for investigative process. Link analysis is based on pattern recognition and smart transactional modeling of gross cash flow.</p>", "id": "3278f0b3-8b4d-4573-b281-884153a1631a"}, {"type": "full_richtext", "value": "<p data-block-key=\\"vi43n\\"></p><h3 data-block-key=\\"eqpgk\\"><b>Statistical Reporting</b></h3><p data-block-key=\\"9ns7n\\">Platform provides extensive reporting on activities in the system and business intelligence based on transactional data. Report types include employee logs, Financial Institute performance, Document statistics and preconfigured national transactional reporting.</p>", "id": "93ef941b-8cec-4db0-9d5b-f5ee5835a196"}, {"type": "image", "value": 25, "id": "2cc7652a-4e9e-4143-bf0e-7cabe4909b50"}, {"type": "image", "value": 26, "id": "5e6035c0-97d0-4740-a721-655f7337c966"}, {"type": "full_richtext", "value": "<p data-block-key=\\"vi43n\\"></p><h3 data-block-key=\\"1o5rq\\"><b>Behaviour Detection</b></h3><p data-block-key=\\"d8tmb\\">Rule based transactional modelling to identify customer behaviour and classification. A custom scoring system provides analysts with critical information on various user characters</p>", "id": "b014134b-0618-4b89-a1e8-c1e254d2ef7c"}]	Anti Money Laundering platform for Central Banks and FICS	Web Application / Analytics Platform	Government	user management and billing application for internet service providers	web application / mobile application / enterprise integration	Large private organisation	http://127.0.0.1:8000/projects/isp	19	27	28	17	18	20	23
13	8Sidor	\N	8 Sidor	[{"type": "full_richtext", "value": "<p data-block-key=\\"hx6uw\\"> 8sidor.se is a Swedish news portal that seeks to serve people with visual and auditory disabilities. mDrift implemented special tools and features into the website to ensure that everybody could navigate the website and consume news articles to stay updated on current events and other reported content. 8Sidor news agency was later then acquired by Swedish government due to its much revered service to their citizens with disabilities. </p>", "id": "b83a4605-6ba0-4187-bc53-e1190fcc2d31"}]	1.5 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"ls071\\"> WordPress CMS, Mobile Application, Custom tools and experience </p>", "id": "ae36313d-8541-4743-9ba4-78b095a68526"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"977l5\\"> Use cases development, User Experience Design, Application Development, Support and Maintenance </p>", "id": "121cdb39-624c-4fb6-b58f-530bc9db9c2b"}]	[{"type": "image", "value": 43, "id": "dee97a88-0018-4674-b578-2a106278cf1a"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5c9u5\\"> </p><h3 data-block-key=\\"cc9j\\"><b>Custom navigation design</b></h3><p data-block-key=\\"2nncr\\">8Sidor website had to be designed primarily for use by people with disabilities and hence navigation of the website was of primary concern from the get go. mDrift designed and implemented various tools such as JavaScript based on-screen readouts, automatically video subtitling, navigational callouts, automatic alt name generators for images in articles etc</p>", "id": "e934d9c5-86cd-46e2-9690-2b6928ed0b9d"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5c9u5\\"> </p><h3 data-block-key=\\"9rpjl\\"><b>Custom Search</b></h3><p data-block-key=\\"6mbuf\\">Due to expected lack of tech savviness of target user group and high categorization of news in the website, search logic was custom built from scratch to search for and present results using ranking on readability and importance.</p>", "id": "802b4420-300b-4ebd-9cb8-fb359a2589ad"}, {"type": "image", "value": 44, "id": "e13933b2-dd00-48e9-bc16-ce822e339ac5"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"hv0d4\\"> </p><h3 data-block-key=\\"19vrl\\"><b>Migrating &amp; merging legacy content</b></h3><p data-block-key=\\"71vho\\">Original 8Sidor website &amp; its political news section \\u2018Alla Valjare\\u2019 was built on separate platforms Umbraco &amp; WordPress respectively. mDrift migrated &amp; merged both websites and ran custom scripting to identify and remove duplicate content published on both website.</p>", "id": "26b86eb1-9619-4a4f-b73a-6e84e4ecb9d3"}, {"type": "image", "value": 46, "id": "3e43cdc0-6fe4-4bd0-a031-8161a156c4b0"}, {"type": "image", "value": 47, "id": "a15b06e0-6230-4c96-a374-95d265c2ab7a"}, {"type": "full_richtext", "value": "<p data-block-key=\\"hv0d4\\"> </p><h3 data-block-key=\\"nbhe\\"><b>Large Database</b></h3><p data-block-key=\\"8sr53\\">When both the clients websites were merged together, the database resulted in a combined size not suitable for WordPress Content Management System preferred by the client. We did extensive optimization on WordPress core as well as frontend JavaScript to make the website nimble and responsive.</p>", "id": "5ba7b6c0-c36c-4cf9-826c-1b42f7cb9e19"}]	news website for visually challenged persons	web application / mobile application	Government Organisation	music editing hardware and software platform	website / e-commerce store	Private Company	http://127.0.0.1:8000/projects/propellerhead	41	48	49	39	40	42	45
14	Propellerhead	\N	Propellerhead	[{"type": "full_richtext", "value": "<p data-block-key=\\"ibbqu\\"> Propellerheads, a swedish music company, offers software and hardware products for music editing, playback etc. mDrift worked very closely with Propellerheads technical team to design and develop a new look and feel to their online business while making sure their existing services are well integrated into the platform so as to minimise the impact on day to day company operations. </p>", "id": "e2719908-db9b-457c-a228-ea594eebba06"}]	4 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"gk3ln\\"> Web application, Mobile Application, Enterprise Integrations </p>", "id": "9f00c085-6118-4f53-9230-d7d91cc37611"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"32u0h\\"> Business process consulting; User experience design; Application Development; Partner Training Programs, Deployment and Support </p>", "id": "155834e2-fa7a-40ed-96eb-ab06740b6295"}]	[{"type": "image", "value": 52, "id": "4480b281-aec4-44f5-ae42-0503c59fcdab"}, {"type": "full_richtext", "value": "<p data-block-key=\\"rxdua\\"> </p><h3 data-block-key=\\"3a32g\\"><b>Client Customizability</b></h3><p data-block-key=\\"38bbf\\">Since the application was custom built on Python and client already had a technical team managing their product and environment, one of the targets from initial days was the code to be highly modular and configurable. This allowed client\\u2019s technical team to re-organise content and functionalities on the website without external help. Custom development on python was chosen as no existing CMS could cater to the client\\u2019s specific requirements and integrations.</p>", "id": "d74eddd9-9931-4e62-a051-98d576c36747"}, {"type": "full_richtext", "value": "<p data-block-key=\\"rxdua\\"> </p><h3 data-block-key=\\"9puut\\"><b>Hardware and Software based product sales</b></h3><p data-block-key=\\"85h4c\\">User experience on the website was designed to be consistent across purchase of software and hardware products. A visitor should feel comfortable about purchasing an online subscription and go right to ordering an equipment to home and providing the shipping details.</p>", "id": "7416d7a8-262d-43ce-86c1-83aa3c00fe6e"}, {"type": "image", "value": 53, "id": "849f0cb0-34ff-4ea3-91dc-9fca9867feb8"}]	[{"type": "image", "value": 56, "id": "c9e7db76-4b06-4910-b617-bf100cd3661d"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5vsy8\\"> </p><h3 data-block-key=\\"ftkh8\\"><b>Enterprise Integration</b></h3><p data-block-key=\\"8nbj1\\">Integrated into multiple existing services, systems and third party services subscribed to by client to make the new web experience as simple to transition onto for both customers and internal team.</p>", "id": "dfde8ca8-df2d-4dd1-bc0f-ea15f71528c6"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5vsy8\\"> </p><h3 data-block-key=\\"94i4c\\"><b>Custom inline music player</b></h3><p data-block-key=\\"aahm3\\">Specially designed custom music player was developed to integrate into client\\u2019s existing proprietary content distribution platform. This allowed the visitors on the website to play and hear music without external player load or wait times.</p>", "id": "62da4366-d11d-462f-aab2-abbef214e645"}, {"type": "image", "value": 57, "id": "e436d3ee-be70-417f-b194-f02acd7dd046"}, {"type": "image", "value": 58, "id": "3f395184-03cd-4502-9c3b-8ac3792f5c40"}, {"type": "full_richtext", "value": "<p data-block-key=\\"5vsy8\\"> </p><h3 data-block-key=\\"bjtos\\"><b>Custom Blog Module</b></h3><p data-block-key=\\"bdbh9\\">Custom blog module was developed for admin with capabilities such as automatic content validations, publisher management, user rewards module etc.</p>", "id": "f2def0e0-37dd-40ef-9683-73ed1161abc7"}]	music editing hardware and software platform	website/ e-commerce store	Private Company	geological subsurface visualization tool	windows desktop application,web application	Private Company	http://127.0.0.1:8000/projects/rocxia	50	59	60	48	49	51	55
15	Rocxia	\N	ROCXIA	[{"type": "full_richtext", "value": "<p data-block-key=\\"k64xn\\"> ROCXIA is a geological subsurface visualization tool used in analysis and discovery of oil and other rare elements. Development based on core C++ and visualizations powered by OpenGL stack let mDrift develop a high performance application that is also efficient in system resource utilization. Along with a web based project management application built on Python Django, administrators could remotely employee and manages personnel and operate a subsurface analysis team. </p>", "id": "8efc9ffb-63a1-4631-9dbc-05b0838e6a55"}]	11 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"gi8im\\"> Windows Desktop Application, Web Application </p>", "id": "7710d7b9-c0a3-4fc4-9655-c1b91f1c9b3c"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"e2gfi\\"> Scientific Research, Subject Matter Expert Panel, From scratch development, User Experience design </p>", "id": "944aa116-c4c5-4976-95df-de32a2ab6969"}]	[{"type": "image", "value": 63, "id": "6500b339-2d78-4257-a28b-e28d17f8f471"}, {"type": "full_richtext", "value": "<p data-block-key=\\"7t48a\\"> </p><h3 data-block-key=\\"300fn\\"><b>Core C++ Development</b></h3><p data-block-key=\\"a2f7b\\">Entire project was developed on C++ to get microscopic control of the project and its deliverables. One of the major challenges of application development was to handle the huge data sizes and maintain control on processes to ensure optimal runtime and efficiency</p>", "id": "3bfc1ffb-dd13-4b44-a534-3bddf68d8413"}, {"type": "full_richtext", "value": "<p data-block-key=\\"7t48a\\"> </p><h3 data-block-key=\\"9599p\\"><b>High Accuracy</b></h3><p data-block-key=\\"6ibot\\">Application was intended to be used by companies to accurately predict the location of oil within subsurface layers and develop well trajectories for drilling crews. The accuracy of such an analysis was required to be between 5-10 meters. Data is handled extremely accurately to ensure no quality loss and highest regard for process runtime optimisation.</p>", "id": "ba820539-46b2-4e33-991d-be9f8b611659"}, {"type": "image", "value": 64, "id": "a190bad4-6a66-4c0f-adae-2df837fa062f"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"qsmv4\\"> </p><h3 data-block-key=\\"96a5v\\"><b>Research into Exploration Geophysics</b></h3><p data-block-key=\\"2q21p\\">mDrift\\u2019s team spent the initial few months learning the field of Exploration Geophysics. This helped the team to not only understand the nuances of the development required but also to communicate with the client much more effectively and efficiently.</p>", "id": "e41a37ed-e0d2-42ca-a2ad-f5101d4a42a2"}, {"type": "image", "value": 66, "id": "e0e6a35f-3c49-45f2-a237-4582ed06a856"}, {"type": "image", "value": 67, "id": "bdb32d37-3d35-4bba-be5e-a9958e372c4d"}, {"type": "full_richtext", "value": "<p data-block-key=\\"qsmv4\\"> </p><h3 data-block-key=\\"5mnkk\\"><b>Huge file sizes</b></h3><p data-block-key=\\"1h1ug\\">SEGY files that contain all the trace data sets that make up the subsurface data set can range in size from 10s to 100s of GBs. The application needed to be able to ingest and maintain the whole data set on memory to quickly visualise and analyse the data set</p>", "id": "fed7bf68-1fc1-4d98-b23d-a730a6719bf2"}, {"type": "full_richtext", "value": "<p data-block-key=\\"qsmv4\\"> </p><h3 data-block-key=\\"86aha\\"><b>Custom triangulation logic</b></h3><p data-block-key=\\"5go3m\\">mDrift had to develop custom triangulation logic to visualize complex underground surface structures. The point cloud data pertaining to the surface is sorted through, analysed and triangulated using a custom logic developed on top of Delaunay triangulation to achieve under visualisation of data under 0.5 second time and posting cutting edge timeline and capabilities.</p>", "id": "87827cb4-8c4f-4d85-bf36-3ee7e86767d2"}, {"type": "image", "value": 68, "id": "17f12a32-1371-4eb4-a8ee-a7624e872543"}, {"type": "image", "value": 69, "id": "db24a800-3c54-4469-a15b-03db21103099"}, {"type": "full_richtext", "value": "<p data-block-key=\\"qsmv4\\"> </p><h3 data-block-key=\\"btgsn\\"><b>OpenGL capabilities</b></h3><p data-block-key=\\"5csf9\\">Application was required to have extensive OpenGL capabilities to visualise such a large data set and handles all this data via intelligent load into memory. This visualised data is used as a base to analyse and operate upon using various tools and functionalities such as volume cutter, surface detection, trajectory analysis, LAS property projection etc.</p>", "id": "1a93d588-db46-4fdb-8b8c-0ede793a07cd"}, {"type": "full_richtext", "value": "<p data-block-key=\\"qsmv4\\"> </p><h3 data-block-key=\\"bb69o\\"><b>Shaders Methodology</b></h3><p data-block-key=\\"2514u\\">Application extensively used the GPU for computation and visualisation while utilising the CPU for organising the information being passed to the GPU</p>", "id": "ffec25f0-aaf6-4db8-bea4-8a0d7adb89c1"}, {"type": "image", "value": 70, "id": "69054b25-a495-453d-a32f-5de240ea2acd"}, {"type": "image", "value": 71, "id": "fce25792-b8ff-4b02-8b4b-f695799c3a4f"}, {"type": "full_richtext", "value": "<p data-block-key=\\"qsmv4\\"> </p><h3 data-block-key=\\"dogoc\\"><b>Multithreading</b></h3><p data-block-key=\\"benjp\\">Application needed to be optimised for multithreaded workload to optimise for multiple parallel functionalities being required of the application.</p>", "id": "e34e2ba7-21be-4387-9aaa-e2a9b6523fff"}]	Geological subsurface visualization tool	windows desktop application, web application	Private Company	nyborhoods	web application,android and ios application	Private Company	http://127.0.0.1:8000/projects/nyborhoods	61	72	73	59	60	62	65
16	Nyborhoods	\N	Nyborhoods	[{"type": "full_richtext", "value": "<p data-block-key=\\"5gnop\\"> Mobile and web application designed to make reward and customer retention programs a cloud based service that businesses can sign up for. Application worked by tracking user\\u2019s location to trigger Points of Interests such as businesses, trail points, touristic sites etc. Businesses could signup and register themselves to offer customer retention and loyalty services such as rewards and special promotions. </p>", "id": "3223e59f-577e-4664-84a7-35507c9f971b"}]	4 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"vcrwn\\"> Web Application, Android and iOS application </p>", "id": "55962c18-cf5a-4ec7-8a7a-32202c080d53"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"wy1j6\\"> User experience design; Application Development; Deployment and Support </p>", "id": "c735499d-cd21-4c83-9ec5-3e2cd7065f55"}]	[{"type": "image", "value": 77, "id": "21da3411-840c-4793-a19c-bd748d0cdb94"}, {"type": "full_richtext", "value": "<p data-block-key=\\"d6l8u\\"> </p><h3 data-block-key=\\"98j0\\"><b>Real time Location Tracking</b></h3><p data-block-key=\\"f5ej9\\">The primary challenge of the application was to track the location of the user and then processing this information with server as efficiently and effectively as possible. We had to ensure minimal battery drainage, network consumption and GPS service usage to ensure that users would use the application without any concerns</p>", "id": "d5ee6d52-48e8-4660-96de-a56015657230"}, {"type": "full_richtext", "value": "<p data-block-key=\\"d6l8u\\"> </p><h3 data-block-key=\\"492dm\\"><b>Persistent Location Service</b></h3><p data-block-key=\\"8v37a\\">Another main challenge was running a persistent location service across multiple brands and handsets. Most android devices monitor and kill services that are consuming persistent network and GPS feeds and the implementation of the same varied across devices. We had to engineer the application\\u2019s service to abide by operating system rule and be non intrusive to users while operating. In cases where the service is killed off, application and service is intelligent enough to restart the request.</p>", "id": "e8a7128d-0e9e-4969-a4b5-57611fd03964"}, {"type": "image", "value": 78, "id": "2731bbdd-4b8c-4a23-8c55-13f79cafdb0e"}]	[{"type": "image", "value": 80, "id": "e9868c7b-2bfe-478f-9e75-2be7b37f1de4"}, {"type": "full_richtext", "value": "<p data-block-key=\\"f78pj\\"> </p><h3 data-block-key=\\"2kh5\\"><b>High spatial resolution</b></h3><p data-block-key=\\"caq2t\\">We also had to achieve very high levels of accuracy in determining between Points of Interest as businesses are usually registered close to each other and need location accuracy in terms of less than 5 meter to achieve required clarity</p>", "id": "0375b73b-bd9b-48eb-9af7-9a38a3c51b1c"}, {"type": "full_richtext", "value": "<p data-block-key=\\"f78pj\\"> </p><h3 data-block-key=\\"2hrlc\\"><b>Intelligent Triggers</b></h3><p data-block-key=\\"d7hhl\\">Intelligent notification system that is capable of learning from past user\\u2019s feedback to improve the accuracy of location based notification trigger.</p>", "id": "d7d50bbb-3d26-4ef6-b18c-346602d429cb"}, {"type": "image", "value": 81, "id": "1869609d-d152-4ea4-8e07-d334de1dbab3"}]	Nyborhoods	web application,android and ios application	Private Company	anti money laundering platform for central banks and fics	web application / analytics platform	Government	http://127.0.0.1:8000/projects/transafe	75	82	83	74	73	76	79
20	Khoon	\N	Khoon Engineering	[{"type": "full_richtext", "value": "<p data-block-key=\\"szw8l\\"> Khoon engineering Pte Ltd from Singapore needed an application to digitise the QA/QC of building constructions undertaken for seamless coordination and quicker issue closure. Application had to have excellent geo-positioning capabilities including built in compass functionalities to map and navigate a large construction project. Application had to handle most of the functionalities locally on mobile due to expected lack of connectivity in development areas </p>", "id": "e195a8c8-4ab9-4e6e-aee5-5fba4f94d1ec"}]	2.5 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"644nu\\"> Android, Web (Python) </p>", "id": "17910343-c958-4f30-be6f-b1867e36256f"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"7t1iw\\"> Web and mobile applications, custom image processing, storage management solutions </p>", "id": "28820a2a-0e90-4172-805d-4107b3300557"}]	[{"type": "image", "value": 116, "id": "3ada8d8b-2b4f-4c9c-9e55-757c5019dd1c"}, {"type": "full_richtext", "value": "<p data-block-key=\\"o13a4\\"> </p><h3 data-block-key=\\"k8k1\\"><b>Detailed POC Approval</b></h3><p data-block-key=\\"3d9ni\\">A Proof Of Concept (POC) application to field test specific functionalities of the application and gather feedback from engineers, mechanics and other workers.</p>", "id": "eb494d67-7dd7-4731-9a3c-ad7e3fa3f848"}, {"type": "full_richtext", "value": "<p data-block-key=\\"o13a4\\"> </p><h3 data-block-key=\\"6a1r6\\"><b>Site Navigation</b></h3><p data-block-key=\\"90hdd\\">Ability to upload sitemaps during project crate for on-site navigational capabilities for app users.</p>", "id": "c07c08ca-173e-49cb-8f3b-81bc694d0a69"}, {"type": "image", "value": 117, "id": "7f657d8d-b7c6-4785-8241-e8a0b90241da"}]	[{"type": "image", "value": 119, "id": "a8ee3f58-e814-4bae-8cc4-05e9ea00bfcd"}, {"type": "full_richtext", "value": "<p data-block-key=\\"dp0ey\\"> </p><h3 data-block-key=\\"eensk\\"><b>Image Editing</b></h3><p data-block-key=\\"7qj72\\">mDrift developed custom image annotation and commenting features for various users to coordinate on-site issues using pictures for clearer communication</p>", "id": "f69af972-2de5-44ea-ad8f-bea53ceceefe"}, {"type": "full_richtext", "value": "<p data-block-key=\\"dp0ey\\"> </p><h3 data-block-key=\\"12jk9\\"><b>Offsite Backup</b></h3><p data-block-key=\\"bl5hj\\">Since QA/QC data is highly important for the client, we developed and implemented offsite backup solution to a remote NAS. This could be used to restore or error correct server data as well.</p>", "id": "69af394b-6e82-49ad-8db9-b5c85d036e16"}, {"type": "image", "value": 120, "id": "8f0b8bf6-33be-4b97-8965-fcddf5abb66e"}]	business process management application for a real estate company	web application / mobile application	Private Conglomerate	core business application for corporate healthcare consultancy	website / web application /enterprise integrations	Private Company	\N	114	121	122	112	113	115	118
17	Ziggy	\N	Ziggy	[{"type": "full_richtext", "value": "<p data-block-key=\\"rt55a\\"> Ziggy Creative Colony is one of Scandinavia\\u2019s leading image consultancy companies. mDrift worked with Ziggy to develop their concept website showcasing the design and UX style and vision of the company. </p>", "id": "2910c4f2-9a01-4041-9783-cae2bb13fed0"}]	3 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"hyegp\\"> Custom WordPress </p>", "id": "cd98594e-31ae-4682-a0dc-b872dfea5cf3"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"1d3h7\\"> custom WordPress website, mobile dedicated pages for specific mobile devices, multilingual options for different geographic areas </p>", "id": "ff1eb63d-1db7-4eeb-b2db-60f9deaf0168"}]	[{"type": "image", "value": 88, "id": "5a268de1-6c26-4236-b599-e2e0ecb00cb7"}, {"type": "full_richtext", "value": "<p data-block-key=\\"c5g05\\"> </p><h3 data-block-key=\\"cn56f\\"><b>In-depth Consulting Sessions</b></h3><p data-block-key=\\"f224d\\">Extensive design consultation sessions were conducted with the client to develop the final website as a cutting edge showcase of what technology and intelligent design can achieve.</p>", "id": "6476a76a-25d6-4832-99bc-ba467fb0b2db"}, {"type": "full_richtext", "value": "<p data-block-key=\\"c5g05\\"> </p><h3 data-block-key=\\"9vdno\\"><b>Custom CMS</b></h3><p data-block-key=\\"a7pr7\\">mDrift developed a custom CMS on python to adhere to the special design requirements of the website. Application could alter design, stricture and navigational features of the page based on content. Included in the system was SEO, to help achieve Google\\u2019s first page SERPs.</p>", "id": "e7143d64-f116-4b94-8da0-4911a4ad0a55"}, {"type": "image", "value": 89, "id": "8ca34230-91f0-407c-812e-a8a32d82a70e"}]	[{"type": "image", "value": 91, "id": "3c743fe0-e0a9-4703-a044-266a6e2d97f7"}, {"type": "full_richtext", "value": "<p data-block-key=\\"2xk8g\\"> </p><h3 data-block-key=\\"bd4gu\\"><b>Responsive Design</b></h3><p data-block-key=\\"bn63j\\">Website is capable of scaling across various devices and providing the best interaction and UX for the particular device. Interactions were custom designed JavaScript to work across devices</p>", "id": "79fb9559-9317-440b-a29a-1b22a31acc8c"}, {"type": "full_richtext", "value": "<p data-block-key=\\"2xk8g\\"> </p><p data-block-key=\\"67spo\\"> </p><h3 data-block-key=\\"dcge9\\"><b>Multilingual Options</b></h3><p data-block-key=\\"4spi9\\">Website supported multiple languages for serving users across various geographies. Automatic translation mechanisms were implemented for quicker content generation.</p>", "id": "e2b3b1d2-c0f1-4411-9f84-4decd5d143d8"}, {"type": "image", "value": 92, "id": "78aa4042-1b94-4b81-a61e-ea65aa38604f"}]	corporate website for scandinavian	website	Private Company	patient queueing and scheduling application for clinics	web application / mobile application /website	Startup	http://127.0.0.1:8000/projects/qaid	86	93	94	84	85	87	90
18	QAid	\N	Q-Aid	[{"type": "full_richtext", "value": "<p data-block-key=\\"h32eo\\"> </p><p data-block-key=\\"6pf3d\\">Q-Aid is a product aimed at reducing wait time at clinics and enable customers to book appointments and arrive at clinics precisely for their appointment. mDrift consulted the entire development and implementation of the application.</p><p data-block-key=\\"aaq3e\\">As the primary aim of the application was to reduce wait time at clinic, application had to efficiently calculate processing times at clinics for each token and balance the commute time from user\\u2019s current location to clinic.</p>", "id": "5bfd43c1-68c6-42fc-bbb6-6bc224f1eebd"}]	3 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"nk7bs\\"> Android, iOS, web (both native apps), data management </p>", "id": "595425cd-b33d-465f-901c-5b07a8f4b502"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"uh2eg\\"> custom web and mobile app, patient application, clinic application, queue management, route management </p>", "id": "7ad8a910-e987-4d64-828a-4713332f9d13"}]	[{"type": "image", "value": 97, "id": "4c4ca6ca-adc2-4a07-9154-98ee40c214ad"}, {"type": "full_richtext", "value": "<p data-block-key=\\"1m2mp\\"> </p><h3 data-block-key=\\"5jt80\\"><b>Clinic Queue Management</b></h3><p data-block-key=\\"3thtu\\">Clinic application deployed at partner clinics enabled them to manage their patient queue electronically and provided reporting options for better insights into the business.</p>", "id": "38ccd5e4-0478-4ae3-8e64-71bd0dde55d6"}, {"type": "full_richtext", "value": "<p data-block-key=\\"1m2mp\\"> </p><h3 data-block-key=\\"a2dcs\\"><b>Custom Notification Triggers</b></h3><p data-block-key=\\"6osbk\\">Users can choose to get notified right on time to start travelling or for any earlier time.</p>", "id": "f85bbb33-4e52-4b96-9c8f-2cc9e0a581f7"}, {"type": "image", "value": 98, "id": "262396de-06ff-4b3e-94fe-131ffac3a529"}]	[{"type": "image", "value": 100, "id": "3f5b2cf2-7452-4db9-9dee-f5b1dfcabfed"}, {"type": "full_richtext", "value": "<p data-block-key=\\"s3s1g\\"> </p><h3 data-block-key=\\"fkhet\\"><b>Clinic Discovery and Rating</b></h3><p data-block-key=\\"chkr9\\">Application also provided end customers to browse various clinics in the region and read reviews about them. This provided users with live token count, expected wait time etc.</p>", "id": "b1faae40-5fcd-411c-8b08-83bf8fb45f21"}, {"type": "full_richtext", "value": "<p data-block-key=\\"s3s1g\\"> </p><h3 data-block-key=\\"fgh1d\\"><b>Custom Functionality</b></h3><p data-block-key=\\"867a5\\">A custom algorithm for efficiently calculating the time user has to start travelling to the clinic. This was done dynamically based on token completion at clinics. Implemented machine learning algorithms to better predict wait time per token at each registered clinic, increasing the accuracy of notifications sent to end users for travelling to the clinic.</p>", "id": "892abea5-1bbe-4b0d-bb7a-1b8787f0a3e6"}, {"type": "image", "value": 101, "id": "85517cab-de09-4e0c-8a86-5cc65e5aafd0"}]	patient queueing and scheduling application for clinics	web application/mobile application/website	Startup	project management tool for sbms	web application	SAAS	http://127.0.0.1:8000/projects/rapiddev	95	102	103	93	94	96	99
19	RapidDev	\N	Rapid Dev	[{"type": "full_richtext", "value": "<p data-block-key=\\"6goqu\\"> RapidDev is a project management application aimed at software companies using high volume agile project management processes. RapidDev integrated all the operation requirements of a company such a project planning, error reporting &amp; CRM into a single package that integrated with each other for state of the art reporting on company status and project financials. </p>", "id": "dd8761f0-fdcd-498e-a10a-c7b8814bda01"}]	4 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"fmper\\"> Python Django </p>", "id": "70026ca3-afa3-4afa-abb3-6d78c32f0cd9"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"xypvh\\"> SaaS architecture for a high performance application, high volume data analytics, mobile responsive </p>", "id": "7ab3fae2-1b53-458b-bd7a-8f57b5990ee8"}]	[{"type": "image", "value": 107, "id": "a72655e8-f45c-499c-b230-01cb125270a2"}, {"type": "full_richtext", "value": "<p data-block-key=\\"9krl5\\"> </p><p data-block-key=\\"4ju86\\"> </p><h3 data-block-key=\\"dt6ld\\"><b>Gantt Chart Interface</b></h3><p data-block-key=\\"9cfh7\\">A gantt chart based interface is developed for users to plan the project across various resources, teams, geographies etc. The UI was designed to work with the latest HTML5 capabilities of the browser requiring less processing power for huge data sets.</p>", "id": "e446bfd9-7ade-45de-a722-168bacc7e3a7"}, {"type": "full_richtext", "value": "<p data-block-key=\\"9krl5\\"> </p><h3 data-block-key=\\"6kldu\\"><b>Bug Reporting</b></h3><p data-block-key=\\"bu6nq\\">Application provides in system functionalities for reporting and tracking bugs in the project.</p>", "id": "b3b518ac-d529-48ef-bf9c-887871fc7545"}, {"type": "image", "value": 108, "id": "c990fabb-f7c6-43f6-9ade-7b6eb831c3fd"}]	[{"type": "image", "value": 110, "id": "665d6f7e-734c-4f36-841a-c7b78ba58635"}, {"type": "full_richtext", "value": "<p data-block-key=\\"fvscv\\"> </p><h3 data-block-key=\\"qdv0\\"><b>BI &amp; Reporting</b></h3><p data-block-key=\\"9kop6\\">Extensive reporting is provided on the projects and task status with more analysis and prediction available on query. Project performance reporting and prediction tools based on Monte Carlo Simulations for analysing the current status of each project.</p>", "id": "65e1f5d6-fa88-432c-84c1-e348edf603f0"}, {"type": "full_richtext", "value": "<p data-block-key=\\"fvscv\\"> </p><h3 data-block-key=\\"el931\\"><b>Extensive User and Role Management</b></h3><p data-block-key=\\"47vv1\\">Application provides extensive user and permission management to customise the application to each company\\u2019s structure and processes.</p>", "id": "46493e13-9b92-4def-a794-15307c7cd495"}, {"type": "image", "value": 111, "id": "6f1e39ea-0dde-4b58-a92d-bcd0c85d5311"}]	project management tool for sbms	web application	SAAS	business process management application for a real estate company	web application / mobile application	Private Conglomerate	http://127.0.0.1:8000/projects/khoon	105	112	113	104	103	106	109
21	LCM	\N	Life Church Missions	[{"type": "full_richtext", "value": "<p data-block-key=\\"yweau\\"> </p><p data-block-key=\\"6thbb\\">Life Church Missions organisation wanted to distribute videos of preachings, seminars and other classes via digital platform that their followers could then use to consume the content, comment and interact with other users on the platform.</p><p data-block-key=\\"bj2h8\\">The organisation had a large library of audio and video recordings compiled over the last few years that had to be organised, labelled and made available for online consumption. Since most of the files were distributed across various platforms such as YouTube, Vimeo, Youku etc, application had to have multiple integrations while preserving user experience</p><p data-block-key=\\"fu2g6\\">iOS and Android applications for end users to consume content. Robust content management platform for admins to publish content irrespective of whether they are hosted on a third party platform or on application server. Labelling and index preparation of client\\u2019s content including tags for optimised search</p>", "id": "cbbe548d-7038-4870-9756-a5a61ba5e0e0"}]	3 Months	[{"type": "full_richtext", "value": "<p data-block-key=\\"4kzvg\\"> Android, iOS, Web (Python) </p>", "id": "2a12af0d-70e2-4d03-8362-5da1df6cdc2f"}]	[{"type": "full_richtext", "value": "<p data-block-key=\\"mhtt7\\"> Web and mobile applications, custom image processing, storage management solutions </p>", "id": "00949bbd-5114-4601-901a-42e2c16c4ec5"}]	[{"type": "image", "value": 127, "id": "3fe247c9-b0a2-48c5-8a88-0acba55c2aeb"}, {"type": "full_richtext", "value": "<p data-block-key=\\"ruaoh\\"> </p><h3 data-block-key=\\"36i6j\\"><b>Custom Video Player</b></h3><p data-block-key=\\"349il\\">Custom video player that could continue with audio when screen is locked with additional functionalities such as playback speed control, resume capabilities etc.</p>", "id": "f79e189b-9858-4ca0-a80f-b236da5412e7"}, {"type": "full_richtext", "value": "<p data-block-key=\\"ruaoh\\"> </p><h3 data-block-key=\\"46go8\\"><b>Forum Commenting</b></h3><p data-block-key=\\"804lg\\">Users could chat on the platform and discuss on topics and video. This promoted community interaction and increased app run time.</p>", "id": "abc736bd-a782-41d2-86e0-cc074dd266c9"}, {"type": "image", "value": 128, "id": "001d77b0-e16a-4c92-9462-3842e1cde483"}]	[{"type": "image", "value": 129, "id": "bb6cf64c-627c-4e41-97c4-b5d63e7cccd7"}, {"type": "full_richtext", "value": "<p data-block-key=\\"tewoa\\"> </p><h3 data-block-key=\\"37mh7\\"><b>Donations &amp; Secured Payment Gateway</b></h3><p data-block-key=\\"2ethj\\">Application had payment gateway integration for accepting donations from users.</p>", "id": "2008fd99-c2bb-4a90-bcbf-0278279af283"}, {"type": "full_richtext", "value": "<p data-block-key=\\"tewoa\\"> </p><h3 data-block-key=\\"2qta9\\"><b>Offline Playback</b></h3><p data-block-key=\\"6v7e9\\">Users could download video and audio files for offline consumption. The files were encrypted and stored on local devices to avoid misuse.</p>", "id": "04a28a87-d375-474b-a021-a79723e20a3b"}, {"type": "image", "value": 130, "id": "0c1c45f5-ba71-4b14-be17-5fedbc954a80"}, {"type": "image", "value": 131, "id": "681952bc-ee05-48f8-9d99-bf4941dc272a"}, {"type": "full_richtext", "value": "<p data-block-key=\\"tewoa\\"> </p><h3 data-block-key=\\"c6240\\"><b>Integration to Third Party Service</b></h3><p data-block-key=\\"8bgmi\\">Application integrated with various video platforms such as YouTube, Vimeo &amp; Youku to provide admins and users with a seamless experience while publishing and consuming content</p>", "id": "f085c667-110d-409f-8437-32af2cbdaa7d"}]	digital content distribution platform for a singapore based church	mobile application	Public Organisation	bearteddy bean bags	website / mobile app	Lifestyle	\N	125	132	\N	123	124	126	128
\.


--
-- Data for Name: project_projectslistingpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.project_projectslistingpage (page_ptr_id, header_title, meta_content) FROM stdin;
10	Projects	\N
\.


--
-- Data for Name: taggit_tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.taggit_tag (id, name, slug) FROM stdin;
\.


--
-- Data for Name: taggit_taggeditem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.taggit_taggeditem (id, object_id, content_type_id, tag_id) FROM stdin;
\.


--
-- Data for Name: wagtailadmin_admin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailadmin_admin (id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_collection; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_collection (id, path, depth, numchild, name) FROM stdin;
1	0001	1	0	Root
\.


--
-- Data for Name: wagtailcore_collectionviewrestriction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_collectionviewrestriction (id, restriction_type, password, collection_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_collectionviewrestriction_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_collectionviewrestriction_groups (id, collectionviewrestriction_id, group_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_comment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_comment (id, text, contentpath, "position", created_at, updated_at, resolved_at, page_id, resolved_by_id, revision_created_id, user_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_commentreply; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_commentreply (id, text, created_at, updated_at, comment_id, user_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_groupapprovaltask; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_groupapprovaltask (task_ptr_id) FROM stdin;
1
\.


--
-- Data for Name: wagtailcore_groupapprovaltask_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_groupapprovaltask_groups (id, groupapprovaltask_id, group_id) FROM stdin;
1	1	1
\.


--
-- Data for Name: wagtailcore_groupcollectionpermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_groupcollectionpermission (id, collection_id, group_id, permission_id) FROM stdin;
1	1	1	2
2	1	2	2
3	1	1	3
4	1	2	3
5	1	1	5
6	1	2	5
7	1	1	6
8	1	2	6
9	1	1	7
10	1	2	7
11	1	1	9
12	1	2	9
\.


--
-- Data for Name: wagtailcore_grouppagepermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_grouppagepermission (id, permission_type, group_id, page_id) FROM stdin;
1	add	1	1
2	edit	1	1
3	publish	1	1
4	add	2	1
5	edit	2	1
6	lock	1	1
7	unlock	1	1
\.


--
-- Data for Name: wagtailcore_locale; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_locale (id, language_code) FROM stdin;
1	en
\.


--
-- Data for Name: wagtailcore_page; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_page (id, path, depth, numchild, title, slug, live, has_unpublished_changes, url_path, seo_title, show_in_menus, search_description, go_live_at, expire_at, expired, content_type_id, owner_id, locked, latest_revision_created_at, first_published_at, live_revision_id, last_published_at, draft_title, locked_at, locked_by_id, translation_key, locale_id, alias_of_id) FROM stdin;
1	0001	1	1	Root	root	t	f	/		f		\N	\N	f	1	\N	f	\N	\N	\N	\N	Root	\N	\N	f19bebd5-a24f-4dd3-93f9-af0054548e5c	1	\N
18	0001000100050008	4	0	qaid	qaid	t	f	/home/projects/qaid/		f		\N	\N	f	49	1	f	2021-09-29 13:03:20.962148+05:30	2021-09-29 13:03:21.014013+05:30	20	2021-09-29 13:03:21.014013+05:30	qaid	\N	\N	0e7b4361-d9b5-4313-96e8-4c5907f23a10	1	\N
4	000100010001	3	0	about us	about-us	t	f	/home/about-us/		f		\N	\N	f	42	1	f	2021-09-29 10:30:54.44987+05:30	2021-09-29 10:30:54.490706+05:30	2	2021-09-29 10:30:54.490706+05:30	about us	\N	\N	df5a0e1b-4fea-4d80-a393-8272403a764b	1	\N
5	000100010002	3	0	technologies	technologies	t	f	/home/technologies/		f		\N	\N	f	43	1	f	2021-09-29 10:31:13.644312+05:30	2021-09-29 10:31:13.680573+05:30	3	2021-09-29 10:31:13.680573+05:30	technologies	\N	\N	4f25754b-950f-4768-afde-3fcf2b2fe72c	1	\N
12	0001000100050002	4	0	isp	isp	t	f	/home/projects/isp/		f		\N	\N	f	49	1	f	2021-09-29 11:56:04.804673+05:30	2021-09-29 11:56:04.899416+05:30	13	2021-09-29 11:56:04.899416+05:30	isp	\N	\N	01294e77-6a62-45e5-be27-fd9d7834b80d	1	\N
11	0001000100050001	4	0	transafe	transafe	t	f	/home/projects/transafe/		f		\N	\N	f	49	1	f	2021-09-29 11:56:58.842769+05:30	2021-09-29 11:14:18.030564+05:30	14	2021-09-29 11:56:58.894632+05:30	transafe	\N	\N	46036e19-13b3-4531-8c0d-f82ace9f2e0a	1	\N
6	000100010003	3	0	contact-us	contact-us	t	f	/home/contact-us/		f		\N	\N	f	45	1	f	2021-09-29 10:41:10.44146+05:30	2021-09-29 10:38:32.370485+05:30	5	2021-09-29 10:41:10.539704+05:30	contact-us	\N	\N	05c9f0b0-11a8-46b0-9277-3946ab2497d5	1	\N
19	0001000100050009	4	0	rapiddev	rapiddev	t	f	/home/projects/rapiddev/		f		\N	\N	f	49	1	f	2021-09-29 13:11:01.224671+05:30	2021-09-29 13:11:01.270549+05:30	21	2021-09-29 13:11:01.270549+05:30	rapiddev	\N	\N	034210df-3ae4-4552-91f4-235169525414	1	\N
13	0001000100050003	4	0	8sidor	8sidor	t	f	/home/projects/8sidor/		f		\N	\N	f	49	1	f	2021-09-29 12:04:29.838709+05:30	2021-09-29 12:04:29.886581+05:30	15	2021-09-29 12:04:29.886581+05:30	8sidor	\N	\N	ed4437e6-97bc-4ea5-a6a9-7f68ff6486a7	1	\N
8	0001000100040001	4	0	How to increase the value of Business data	how-to-increase-the-value-of-business-data	t	f	/home/blogs/how-to-increase-the-value-of-business-data/		f		\N	\N	f	47	1	f	2021-09-29 10:54:05.960475+05:30	2021-09-29 10:54:06.000367+05:30	7	2021-09-29 10:54:06.000367+05:30	How to increase the value of Business data	\N	\N	9b9af197-30e5-4b70-b06e-880c6420feb9	1	\N
9	0001000100040002	4	0	new post	new-post	t	f	/home/blogs/new-post/		f		\N	\N	f	47	1	f	2021-09-29 10:58:05.567921+05:30	2021-09-29 10:58:05.610806+05:30	10	2021-09-29 10:58:05.610806+05:30	new post	\N	\N	7c850728-3a49-4376-bf7f-c632ca1852b2	1	\N
3	00010001	2	5	Home	home	t	f	/home/		f		\N	\N	f	2	\N	f	2021-09-29 10:26:30.333726+05:30	2021-09-29 10:26:30.441323+05:30	1	2021-09-29 10:26:30.441323+05:30	Home	\N	\N	2fd53b22-0abd-4c65-a456-7dd92f9fe4f9	1	\N
14	0001000100050004	4	0	propellerhead	propellerhead	t	f	/home/projects/propellerhead/		f		\N	\N	f	49	1	f	2021-09-29 12:13:46.403473+05:30	2021-09-29 12:13:46.444362+05:30	16	2021-09-29 12:13:46.444362+05:30	propellerhead	\N	\N	ce8ae964-dd76-4c3e-aafd-f9b4a3a43fa3	1	\N
20	000100010005000A	4	0	khoon	khoon	t	f	/home/projects/khoon/		f		\N	\N	f	49	1	f	2021-09-29 13:20:47.883077+05:30	2021-09-29 13:20:47.931946+05:30	22	2021-09-29 13:20:47.931946+05:30	khoon	\N	\N	8020cf78-bc76-4308-b8a6-fdfe05a73df5	1	\N
15	0001000100050005	4	0	rocxia	rocxia	t	f	/home/projects/rocxia/		f		\N	\N	f	49	1	f	2021-09-29 12:41:16.912879+05:30	2021-09-29 12:41:16.96773+05:30	17	2021-09-29 12:41:16.96773+05:30	rocxia	\N	\N	ab985aff-b789-4f9c-a61a-d23cef051713	1	\N
10	000100010005	3	11	projects	projects	t	f	/home/projects/		f		\N	\N	f	48	1	f	2021-09-29 11:00:58.5827+05:30	2021-09-29 11:00:58.621595+05:30	11	2021-09-29 11:00:58.621595+05:30	projects	\N	\N	01070b0a-9310-436d-bf2a-c838b586b100	1	\N
16	0001000100050006	4	0	nyborhoods	nyborhoods	t	f	/home/projects/nyborhoods/		f		\N	\N	f	49	1	f	2021-09-29 12:48:55.58008+05:30	2021-09-29 12:48:55.630944+05:30	18	2021-09-29 12:48:55.630944+05:30	nyborhoods	\N	\N	33c06b7a-651e-4dab-8cf5-a1901628952c	1	\N
17	0001000100050007	4	0	ziggy	ziggy	t	f	/home/projects/ziggy/		f		\N	\N	f	49	1	f	2021-09-29 12:57:03.672186+05:30	2021-09-29 12:57:03.720057+05:30	19	2021-09-29 12:57:03.720057+05:30	ziggy	\N	\N	3a3675da-6c5d-4c48-8b0f-b283872cd1f5	1	\N
21	000100010005000B	4	0	lcm	lcm	t	f	/home/projects/lcm/		f		\N	\N	f	49	1	f	2021-09-29 13:32:12.107174+05:30	2021-09-29 13:32:12.166017+05:30	23	2021-09-29 13:32:12.166017+05:30	lcm	\N	\N	a3b69ad8-92d7-4638-964f-0529ffa46017	1	\N
7	000100010004	3	3	blogs	blogs	t	f	/home/blogs/		f		\N	\N	f	46	1	f	2021-09-29 10:56:45.399836+05:30	2021-09-29 10:49:57.747958+05:30	9	2021-09-29 10:56:45.465176+05:30	blogs	\N	\N	74ca035e-ba98-4b77-81f1-006953473999	1	\N
22	0001000100040003	4	0	new post 3	new-post-3	t	f	/home/blogs/new-post-3/		f		\N	\N	f	47	1	f	2021-09-29 17:55:55.558645+05:30	2021-09-29 17:55:55.594548+05:30	24	2021-09-29 17:55:55.594548+05:30	new post 3	\N	\N	1f449724-6b0a-4311-a9de-b75cddd47a8a	1	\N
\.


--
-- Data for Name: wagtailcore_pagelogentry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pagelogentry (id, label, action, data_json, "timestamp", content_changed, deleted, content_type_id, page_id, revision_id, user_id) FROM stdin;
1	Home	wagtail.edit	""	2021-09-29 10:26:30.386662+05:30	t	f	2	3	1	1
2	Home	wagtail.publish	null	2021-09-29 10:26:30.510023+05:30	t	f	2	3	1	1
3	about us	wagtail.create	""	2021-09-29 10:30:54.420326+05:30	t	f	42	4	\N	1
4	about us	wagtail.publish	null	2021-09-29 10:30:54.553769+05:30	t	f	42	4	2	1
5	technologies	wagtail.create	""	2021-09-29 10:31:13.616305+05:30	t	f	43	5	\N	1
6	technologies	wagtail.publish	null	2021-09-29 10:31:13.740826+05:30	t	f	43	5	3	1
7	contact-us	wagtail.create	""	2021-09-29 10:38:32.255793+05:30	t	f	45	6	\N	1
8	contact-us	wagtail.publish	null	2021-09-29 10:38:32.453265+05:30	t	f	45	6	4	1
9	contact-us	wagtail.edit	""	2021-09-29 10:41:10.474364+05:30	t	f	45	6	5	1
10	contact-us	wagtail.publish	null	2021-09-29 10:41:10.614505+05:30	t	f	45	6	5	1
11	blog	wagtail.create	""	2021-09-29 10:49:57.692107+05:30	t	f	46	7	\N	1
12	blog	wagtail.publish	null	2021-09-29 10:49:57.79384+05:30	t	f	46	7	6	1
13	How to increase the value of Business data	wagtail.create	""	2021-09-29 10:54:05.927563+05:30	t	f	47	8	\N	1
14	How to increase the value of Business data	wagtail.publish	null	2021-09-29 10:54:06.07018+05:30	t	f	47	8	7	1
15	blog	wagtail.edit	""	2021-09-29 10:54:24.229069+05:30	t	f	46	7	8	1
16	blog	wagtail.publish	null	2021-09-29 10:54:24.321821+05:30	t	f	46	7	8	1
17	blogs	wagtail.edit	""	2021-09-29 10:56:45.42776+05:30	t	f	46	7	9	1
18	blogs	wagtail.rename	{"title": {"old": "blog", "new": "blogs"}}	2021-09-29 10:56:45.528016+05:30	f	f	46	7	9	1
19	blogs	wagtail.publish	{"title": {"old": "blog", "new": "blogs"}}	2021-09-29 10:56:45.53599+05:30	t	f	46	7	9	1
20	new post	wagtail.create	""	2021-09-29 10:58:05.532019+05:30	t	f	47	9	\N	1
21	new post	wagtail.publish	null	2021-09-29 10:58:05.665661+05:30	t	f	47	9	10	1
22	projects	wagtail.create	""	2021-09-29 11:00:58.551676+05:30	t	f	48	10	\N	1
23	projects	wagtail.publish	null	2021-09-29 11:00:58.683946+05:30	t	f	48	10	11	1
24	transafe	wagtail.create	""	2021-09-29 11:14:17.94878+05:30	t	f	49	11	\N	1
25	transafe	wagtail.publish	null	2021-09-29 11:14:18.094392+05:30	t	f	49	11	12	1
26	isp	wagtail.create	""	2021-09-29 11:56:04.739846+05:30	t	f	49	12	\N	1
27	isp	wagtail.publish	null	2021-09-29 11:56:04.961252+05:30	t	f	49	12	13	1
28	transafe	wagtail.edit	""	2021-09-29 11:56:58.861719+05:30	t	f	49	11	14	1
29	transafe	wagtail.publish	null	2021-09-29 11:56:58.943501+05:30	t	f	49	11	14	1
30	8sidor	wagtail.create	""	2021-09-29 12:04:29.801808+05:30	t	f	49	13	\N	1
31	8sidor	wagtail.publish	null	2021-09-29 12:04:29.946422+05:30	t	f	49	13	15	1
32	propellerhead	wagtail.create	""	2021-09-29 12:13:46.368566+05:30	t	f	49	14	\N	1
33	propellerhead	wagtail.publish	null	2021-09-29 12:13:46.499216+05:30	t	f	49	14	16	1
34	rocxia	wagtail.create	""	2021-09-29 12:41:16.867998+05:30	t	f	49	15	\N	1
35	rocxia	wagtail.publish	null	2021-09-29 12:41:17.051506+05:30	t	f	49	15	17	1
36	nyborhoods	wagtail.create	""	2021-09-29 12:48:55.535202+05:30	t	f	49	16	\N	1
37	nyborhoods	wagtail.publish	null	2021-09-29 12:48:55.728198+05:30	t	f	49	16	18	1
38	ziggy	wagtail.create	""	2021-09-29 12:57:03.630299+05:30	t	f	49	17	\N	1
39	ziggy	wagtail.publish	null	2021-09-29 12:57:03.786879+05:30	t	f	49	17	19	1
40	qaid	wagtail.create	""	2021-09-29 13:03:20.92126+05:30	t	f	49	18	\N	1
41	qaid	wagtail.publish	null	2021-09-29 13:03:21.080831+05:30	t	f	49	18	20	1
42	rapiddev	wagtail.create	""	2021-09-29 13:11:01.181785+05:30	t	f	49	19	\N	1
43	rapiddev	wagtail.publish	null	2021-09-29 13:11:01.332382+05:30	t	f	49	19	21	1
44	khoon	wagtail.create	""	2021-09-29 13:20:47.821724+05:30	t	f	49	20	\N	1
45	khoon	wagtail.publish	null	2021-09-29 13:20:47.996291+05:30	t	f	49	20	22	1
46	lcm	wagtail.create	""	2021-09-29 13:32:12.058789+05:30	t	f	49	21	\N	1
47	lcm	wagtail.publish	null	2021-09-29 13:32:12.244809+05:30	t	f	49	21	23	1
48	new post 3	wagtail.create	""	2021-09-29 17:55:55.523738+05:30	t	f	47	22	\N	1
49	new post 3	wagtail.publish	null	2021-09-29 17:55:55.65339+05:30	t	f	47	22	24	1
\.


--
-- Data for Name: wagtailcore_pagerevision; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pagerevision (id, submitted_for_moderation, created_at, content_json, approved_go_live_at, page_id, user_id) FROM stdin;
1	f	2021-09-29 10:26:30.333726+05:30	{"pk": 3, "path": "00010001", "depth": 2, "numchild": 0, "translation_key": "2fd53b22-0abd-4c65-a456-7dd92f9fe4f9", "locale": 1, "title": "Home", "draft_title": "Home", "slug": "home", "content_type": 2, "live": true, "has_unpublished_changes": false, "url_path": "/home/", "owner": null, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Home", "meta_content": null, "client_logos": "[{\\"type\\": \\"image\\", \\"value\\": 1, \\"id\\": \\"35e51b42-87fd-4bea-b7e0-d7fee97cb17a\\"}, {\\"type\\": \\"image\\", \\"value\\": 2, \\"id\\": \\"524807e9-44b6-4bdc-94c3-58496732bc5d\\"}, {\\"type\\": \\"image\\", \\"value\\": 3, \\"id\\": \\"b4722ddf-098f-43ad-902e-dc8693d9717b\\"}, {\\"type\\": \\"image\\", \\"value\\": 4, \\"id\\": \\"fe72d63c-b558-466e-946c-52e893513bdd\\"}, {\\"type\\": \\"image\\", \\"value\\": 5, \\"id\\": \\"daceb825-976b-441e-a89b-7485b91e022f\\"}, {\\"type\\": \\"image\\", \\"value\\": 6, \\"id\\": \\"e3ab0cb5-f0a4-405d-bef8-b564dadc3d5e\\"}, {\\"type\\": \\"image\\", \\"value\\": 7, \\"id\\": \\"6a68aff0-5565-40f5-8706-126d4af5a019\\"}, {\\"type\\": \\"image\\", \\"value\\": 8, \\"id\\": \\"9fdcab0f-65ad-4173-8948-a23a74469234\\"}, {\\"type\\": \\"image\\", \\"value\\": 9, \\"id\\": \\"bf4489a9-f5f5-4d8f-ae40-7e67489adcf5\\"}]", "comments": []}	\N	3	1
3	f	2021-09-29 10:31:13.644312+05:30	{"pk": 5, "path": "000100010002", "depth": 3, "numchild": 0, "translation_key": "4f25754b-950f-4768-afde-3fcf2b2fe72c", "locale": 1, "title": "technologies", "draft_title": "technologies", "slug": "technologies", "content_type": 43, "live": true, "has_unpublished_changes": false, "url_path": "/home/technologies/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Technologies", "meta_content": null, "comments": []}	\N	5	1
2	f	2021-09-29 10:30:54.44987+05:30	{"pk": 4, "path": "000100010001", "depth": 3, "numchild": 0, "translation_key": "df5a0e1b-4fea-4d80-a393-8272403a764b", "locale": 1, "title": "about us", "draft_title": "about us", "slug": "about-us", "content_type": 42, "live": true, "has_unpublished_changes": false, "url_path": "/home/about-us/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "About Us", "meta_content": null, "comments": []}	\N	4	1
5	f	2021-09-29 10:41:10.44146+05:30	{"pk": 6, "path": "000100010003", "depth": 3, "numchild": 0, "translation_key": "05c9f0b0-11a8-46b0-9277-3946ab2497d5", "locale": 1, "title": "contact-us", "draft_title": "contact-us", "slug": "contact-us", "content_type": 45, "live": true, "has_unpublished_changes": false, "url_path": "/home/contact-us/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-09-29T05:08:32.370Z", "last_published_at": "2021-09-29T05:08:32.370Z", "latest_revision_created_at": "2021-09-29T05:08:32.300Z", "live_revision": 4, "alias_of": null, "to_address": "ranjuranjitha.1997@gmail.com", "from_address": "archanapc08@gmail.com", "subject": "contact form submission", "intro": "", "thank_you_text": "<p data-block-key=\\"tvshm\\">Thank you</p>", "comments": [], "form_fields": [{"pk": 1, "sort_order": 0, "clean_name": "your_name", "label": "Your Name", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "Name", "page": 6}, {"pk": 2, "sort_order": 1, "clean_name": "your_company", "label": "Your company", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "Company name", "page": 6}, {"pk": 3, "sort_order": 2, "clean_name": "your_email_address", "label": "Your email address", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "E-mail", "page": 6}, {"pk": 4, "sort_order": 3, "clean_name": "when_do_you_want_to_start", "label": "When do you want to start?", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "Select date", "page": 6}, {"pk": 5, "sort_order": 4, "clean_name": "what_is_your_budget", "label": "What is your budget", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "Budget", "page": 6}, {"pk": 6, "sort_order": 5, "clean_name": "describe_your_needs_the_more_we_know_the_better", "label": "Describe your needs, the more we know, the better", "field_type": "multiline", "required": true, "choices": "", "default_value": "", "help_text": "Describe your needs here", "page": 6}]}	\N	6	1
4	f	2021-09-29 10:38:32.300672+05:30	{"pk": 6, "path": "000100010003", "depth": 3, "numchild": 0, "translation_key": "05c9f0b0-11a8-46b0-9277-3946ab2497d5", "locale": 1, "title": "contact-us", "draft_title": "contact-us", "slug": "contact-us", "content_type": 45, "live": true, "has_unpublished_changes": false, "url_path": "/home/contact-us/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "to_address": "ranjuranjitha.1997@gmail.com", "from_address": "archanapc08@gmail.com", "subject": "contact form submission", "intro": "", "thank_you_text": "<p data-block-key=\\"tvshm\\">Thank you</p>", "comments": [], "form_fields": [{"pk": 1, "sort_order": 0, "clean_name": "your_name", "label": "Your Name", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "Name", "page": 6}, {"pk": 2, "sort_order": 1, "clean_name": "your_company", "label": "Your company", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "Company name", "page": 6}, {"pk": 3, "sort_order": 2, "clean_name": "your_email_address", "label": "Your email address", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "E-mail", "page": 6}, {"pk": 4, "sort_order": 3, "clean_name": "when_do_you_want_to_start", "label": "When do you want to start?", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "Select date", "page": 6}, {"pk": 5, "sort_order": 4, "clean_name": "what_is_your_budget", "label": "What is your budget", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "Budget", "page": 6}, {"pk": 6, "sort_order": 5, "clean_name": "describe_your_needs_the_more_we_know_the_better", "label": "Describe your needs, the more we know, the better", "field_type": "singleline", "required": true, "choices": "", "default_value": "", "help_text": "Describe your needs here", "page": 6}]}	\N	6	1
7	f	2021-09-29 10:54:05.960475+05:30	{"pk": 8, "path": "0001000100040001", "depth": 4, "numchild": 0, "translation_key": "9b9af197-30e5-4b70-b06e-880c6420feb9", "locale": 1, "title": "How to increase the value of Business data", "draft_title": "How to increase the value of Business data", "slug": "how-to-increase-the-value-of-business-data", "content_type": 47, "live": true, "has_unpublished_changes": false, "url_path": "/home/blog/how-to-increase-the-value-of-business-data/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "How to increase the value of Business data", "meta_content": null, "blog_detail_image": 10, "blog_list_image": 10, "date": "2021-09-29", "read_time": "7 min read", "content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"tuebo\\\\\\"> </p><p data-block-key=\\\\\\"2o0lv\\\\\\"><b>How to increase the value of Business data</b></p><p data-block-key=\\\\\\"4ul3\\\\\\">Since technology reforms every industry, organizations continue to formulate considerable investments. However, a lot of such investments fail to bring their agreed returns. The outcomes by analyzing various international companies to find out whether improved technology spending could direct enhanced monetary performance show no direct connection between technology investments and revenue growth. Spending on technology does not automatically direct to improved financial performance. This by itself is not a surprise, but the results show a relationship between technology and revenue development if the assets are focused on targeted abilities, boosted with the true operating model and execution skills.</p><p data-block-key=\\\\\\"bpbrh\\\\\\">To begin thinking more tactically about your data, visualize you\\\\u2019re on an analytics drive that takes you to some thrilling places. Every stop along the way will assist you to connect better value from the data produced by your automation systems, incorporating project resource planning, CRM, e-commerce etc. The accurate value of client data lies in its capability to give context. This can merely happen when that data is linked across silos.</p><p data-block-key=\\\\\\"fkvct\\\\\\">There are several ways that big data can produce revenue, stimulate product and service improvement, cut costs, advance operations as well as business models. The most frequent area is product innovation, yet the chief gains approach from business model innovation along with data monetization. They both will have a basic impact across an organization.</p><p data-block-key=\\\\\\"1bmng\\\\\\">Here are the areas to consider for transforming the performance which boosts revenue and customer satisfaction:</p><ul><li data-block-key=\\\\\\"6bsl7\\\\\\">Strategy</li></ul><embed alt=\\\\\\"Strategy.png\\\\\\" embedtype=\\\\\\"image\\\\\\" format=\\\\\\"left\\\\\\" id=\\\\\\"11\\\\\\"/><p data-block-key=\\\\\\"3ot67\\\\\\"></p><p data-block-key=\\\\\\"8tek0\\\\\\">The term Strategic analytics implies choices that are made, capital which is invested, and plans for data along with analytics that is generated based on the requirements of a business. Analytical solutions in the present business environment are fundamental since they let users think intentionally about how an organization constructs its core competencies and makes value. This not only notifies the whole process, saves time, effort and money, but also guides to value creation.</p><ul><li data-block-key=\\\\\\"6pb21\\\\\\">Sales Forecasting</li></ul><embed alt=\\\\\\"sales-forecasting.png\\\\\\" embedtype=\\\\\\"image\\\\\\" format=\\\\\\"left\\\\\\" id=\\\\\\"12\\\\\\"/><p data-block-key=\\\\\\"ea1ud\\\\\\"></p><p data-block-key=\\\\\\"e47pr\\\\\\">Companies can advance the exactness of their sales forecasts with predictive analytics. Predictive analytics can assist to expose styles and models in a company\\\\u2019s past sales data and monetary performance. Employing those styles and models, analysts can guess profits based on the company\\\\u2019s recent sales pipeline.</p><p data-block-key=\\\\\\"639r1\\\\\\">Fresh companies with small or no historical monetary data can also forecast with predictive analytics. In these cases, the predictive models frequently rely on diverse types of data like industry sales statistics, economic indicators, market demographics, or the financial performance of parallel organizations.</p><ul><li data-block-key=\\\\\\"72vv7\\\\\\">Improved Advertising</li></ul><embed alt=\\\\\\"Improved-Advertsing.png\\\\\\" embedtype=\\\\\\"image\\\\\\" format=\\\\\\"left\\\\\\" id=\\\\\\"13\\\\\\"/><p data-block-key=\\\\\\"9q4j8\\\\\\"></p><p data-block-key=\\\\\\"7l094\\\\\\">All advertisement is A/B and even C split-tested. Every landing page, pop-ups, and even product pictures are reviewed for their efficiency with tweaks being made to guarantee the highest results. Yet the placing of products on the website is calculated to recognize the finest location to assist drive engagement and sales.</p><p data-block-key=\\\\\\"2ek6a\\\\\\">Advertising can be costly, and it\\\\u2019s significant to know how to get the greatest return on investment.</p><ul><li data-block-key=\\\\\\"301p2\\\\\\">Measure everything</li></ul><embed alt=\\\\\\"Measure-everything.png\\\\\\" embedtype=\\\\\\"image\\\\\\" format=\\\\\\"left\\\\\\" id=\\\\\\"14\\\\\\"/><p data-block-key=\\\\\\"boo6v\\\\\\"></p><p data-block-key=\\\\\\"off\\\\\\">Without measuring each part of your sales analytics efforts, you will not have the clue whether it\\\\u2019s functioning or not to help to make improved business choices. This denotes that everything ought to be computed adjacent to baselines and end goals.</p><p data-block-key=\\\\\\"18ut3\\\\\\">In calculating different parts of your sales procedure, you will be better situated to solve what works and what doesn\\\\u2019t. You can spot specific features that positively influence your bottom line and others that may be damaging it. You will get a better idea of how to best assign future resources.</p><ul><li data-block-key=\\\\\\"ekinr\\\\\\">Optimize Marketing Strategies</li></ul><embed alt=\\\\\\"optimize-marketing-strategies.png\\\\\\" embedtype=\\\\\\"image\\\\\\" format=\\\\\\"left\\\\\\" id=\\\\\\"15\\\\\\"/><p data-block-key=\\\\\\"ev8mg\\\\\\"></p><p data-block-key=\\\\\\"2qk6h\\\\\\">Organizations can boost their outcome by exploiting the return on their marketing investments. The rising number of companies is gathering data from their marketing attempts and applying predictive analytics to better understand their clientele and how to interact with them successfully.</p><p data-block-key=\\\\\\"9ehqv\\\\\\">Marketers these days can gather important data about customers through website analytics tools, social media activities, online forms and surveys etc. Companies can also record consumer purchase patterns themselves or buy comparable data from third-party resources. By applying predictive analytics, sellers can change this data into priceless insights, like who their most hopeful leads are, and who in their market is likely to buy a specific product or service.</p><p data-block-key=\\\\\\"bi1ma\\\\\\">With this information, organizations can aim for different parts in their target market with more efficient, modified messaging. Predictive analytics can likewise illustrate you which marketing campaigns and channels are successful at making sales. Eventually, this can aid you to assign more of your marketing funds to efforts that produce a higher return.</p><ul><li data-block-key=\\\\\\"1ks72\\\\\\">Technical ability to execute</li></ul><embed alt=\\\\\\"Technical-ability-to-execute.png\\\\\\" embedtype=\\\\\\"image\\\\\\" format=\\\\\\"left\\\\\\" id=\\\\\\"16\\\\\\"/><p data-block-key=\\\\\\"3m4bs\\\\\\"></p><p data-block-key=\\\\\\"1jthn\\\\\\">One of the key priorities is the skill to deliver difficult projects. As companies in every industry look for altering themselves through technology, the aptitude to execute large-scale IT plans will turn out to be critical. By winning organizations to recognize the skills required for executing their technology plan, they can compose a truthful evaluation of their present strengths and weaknesses.</p><p data-block-key=\\\\\\"f8t2s\\\\\\">In the end, numerous companies need a technology road map that holds their general strategy. This road map is not simply a sequencing diagram for how a company\\\\u2019s existing initiatives are likely to cooperate in the future, yet rather a description of future abilities, the technology style that is necessary to stand up these abilities.</p><ul><li data-block-key=\\\\\\"7co71\\\\\\">Retain Customers</li></ul><p data-block-key=\\\\\\"30ddd\\\\\\">Predictive analytics can assist you not only draw new business but also aid you retain the customers you attract and transform your first-time sales into a frequent income.</p><p data-block-key=\\\\\\"2emta\\\\\\">To apply predictive analytics for customer retention, organizations initially want to gather data on their consumers, including information about the products and services a client purchased, demographic and geographic data, and if they are first-time or frequent clients. If you know which clients are more likely to revisit, you can target them with personalized marketing campaigns, such as discount offers or definite product or service proposals. Giving personalized messages can assist foster faithfulness and remain customers away from your opponents.</p><p data-block-key=\\\\\\"dpqpb\\\\\\">Companies are becoming more advanced and data-driven than earlier. Predictive analytics has been demonstrated to have a major positive impact on an organization\\\\u2019s financial performance. Moreover, several companies that have already implemented predictive analytics report have helped them to increase an edge against their struggle. Progressed analytical approaches that can deal with the top priority opportunities are by far the fastest path to worth over the short term. This is factual in the parts of client management, operation and supply chain management, risk management etc. When you catch time to dig up into the details of your operations, you can find its incredible better opportunities as well as tools for excellent data analytics!</p>\\", \\"id\\": \\"7b4b2b4e-b11a-459c-a61f-0150478f6df3\\"}]", "comments": []}	\N	8	1
6	f	2021-09-29 10:49:57.717044+05:30	{"pk": 7, "path": "000100010004", "depth": 3, "numchild": 0, "translation_key": "74ca035e-ba98-4b77-81f1-006953473999", "locale": 1, "title": "blog", "draft_title": "blog", "slug": "blog", "content_type": 46, "live": true, "has_unpublished_changes": false, "url_path": "/home/blog/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Blogs", "meta_content": null, "comments": []}	\N	7	1
10	f	2021-09-29 10:58:05.567921+05:30	{"pk": 9, "path": "0001000100040002", "depth": 4, "numchild": 0, "translation_key": "7c850728-3a49-4376-bf7f-c632ca1852b2", "locale": 1, "title": "new post", "draft_title": "new post", "slug": "new-post", "content_type": 47, "live": true, "has_unpublished_changes": false, "url_path": "/home/blogs/new-post/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "new post", "meta_content": null, "blog_detail_image": 16, "blog_list_image": 15, "date": "2021-09-28", "read_time": "7 min read", "content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1tdlm\\\\\\">new post</p>\\", \\"id\\": \\"803a8f3d-986b-4273-bd88-ae6b073f454c\\"}]", "comments": []}	\N	9	1
8	f	2021-09-29 10:54:24.212115+05:30	{"pk": 7, "path": "000100010004", "depth": 3, "numchild": 1, "translation_key": "74ca035e-ba98-4b77-81f1-006953473999", "locale": 1, "title": "blog", "draft_title": "blog", "slug": "blogs", "content_type": 46, "live": true, "has_unpublished_changes": false, "url_path": "/home/blog/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-09-29T05:19:57.747Z", "last_published_at": "2021-09-29T05:19:57.747Z", "latest_revision_created_at": "2021-09-29T05:19:57.717Z", "live_revision": 6, "alias_of": null, "header_title": "Blogs", "meta_content": null, "comments": []}	\N	7	1
9	f	2021-09-29 10:56:45.399836+05:30	{"pk": 7, "path": "000100010004", "depth": 3, "numchild": 1, "translation_key": "74ca035e-ba98-4b77-81f1-006953473999", "locale": 1, "title": "blogs", "draft_title": "blog", "slug": "blogs", "content_type": 46, "live": true, "has_unpublished_changes": false, "url_path": "/home/blogs/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-09-29T05:19:57.747Z", "last_published_at": "2021-09-29T05:24:24.264Z", "latest_revision_created_at": "2021-09-29T05:24:24.212Z", "live_revision": 8, "alias_of": null, "header_title": "Blogs", "meta_content": null, "comments": []}	\N	7	1
11	f	2021-09-29 11:00:58.5827+05:30	{"pk": 10, "path": "000100010005", "depth": 3, "numchild": 0, "translation_key": "01070b0a-9310-436d-bf2a-c838b586b100", "locale": 1, "title": "projects", "draft_title": "projects", "slug": "projects", "content_type": 48, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Projects", "meta_content": null, "comments": []}	\N	10	1
18	f	2021-09-29 12:48:55.58008+05:30	{"pk": 16, "path": "0001000100050006", "depth": 4, "numchild": 0, "translation_key": "33c06b7a-651e-4dab-8cf5-a1901628952c", "locale": 1, "title": "nyborhoods", "draft_title": "nyborhoods", "slug": "nyborhoods", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/nyborhoods/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Nyborhoods", "meta_content": null, "right_content_heading": "Nyborhoods", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5gnop\\\\\\"> Mobile and web application designed to make reward and customer retention programs a cloud based service that businesses can sign up for. Application worked by tracking user\\\\u2019s location to trigger Points of Interests such as businesses, trail points, touristic sites etc. Businesses could signup and register themselves to offer customer retention and loyalty services such as rewards and special promotions. </p>\\", \\"id\\": \\"3223e59f-577e-4664-84a7-35507c9f971b\\"}]", "timeline": "4 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"vcrwn\\\\\\"> Web Application, Android and iOS application </p>\\", \\"id\\": \\"55962c18-cf5a-4ec7-8a7a-32202c080d53\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"wy1j6\\\\\\"> User experience design; Application Development; Deployment and Support </p>\\", \\"id\\": \\"c735499d-cd21-4c83-9ec5-3e2cd7065f55\\"}]", "section1_image": 76, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 77, \\"id\\": \\"21da3411-840c-4793-a19c-bd748d0cdb94\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"d6l8u\\\\\\"> </p><h3 data-block-key=\\\\\\"98j0\\\\\\"><b>Real time Location Tracking</b></h3><p data-block-key=\\\\\\"f5ej9\\\\\\">The primary challenge of the application was to track the location of the user and then processing this information with server as efficiently and effectively as possible. We had to ensure minimal battery drainage, network consumption and GPS service usage to ensure that users would use the application without any concerns</p>\\", \\"id\\": \\"d5ee6d52-48e8-4660-96de-a56015657230\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"d6l8u\\\\\\"> </p><h3 data-block-key=\\\\\\"492dm\\\\\\"><b>Persistent Location Service</b></h3><p data-block-key=\\\\\\"8v37a\\\\\\">Another main challenge was running a persistent location service across multiple brands and handsets. Most android devices monitor and kill services that are consuming persistent network and GPS feeds and the implementation of the same varied across devices. We had to engineer the application\\\\u2019s service to abide by operating system rule and be non intrusive to users while operating. In cases where the service is killed off, application and service is intelligent enough to restart the request.</p>\\", \\"id\\": \\"e8a7128d-0e9e-4969-a4b5-57611fd03964\\"}, {\\"type\\": \\"image\\", \\"value\\": 78, \\"id\\": \\"2731bbdd-4b8c-4a23-8c55-13f79cafdb0e\\"}]", "section2_image": 79, "section2_content": "[{\\"type\\": \\"image\\", \\"value\\": 80, \\"id\\": \\"e9868c7b-2bfe-478f-9e75-2be7b37f1de4\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"f78pj\\\\\\"> </p><h3 data-block-key=\\\\\\"2kh5\\\\\\"><b>High spatial resolution</b></h3><p data-block-key=\\\\\\"caq2t\\\\\\">We also had to achieve very high levels of accuracy in determining between Points of Interest as businesses are usually registered close to each other and need location accuracy in terms of less than 5 meter to achieve required clarity</p>\\", \\"id\\": \\"0375b73b-bd9b-48eb-9af7-9a38a3c51b1c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"f78pj\\\\\\"> </p><h3 data-block-key=\\\\\\"2hrlc\\\\\\"><b>Intelligent Triggers</b></h3><p data-block-key=\\\\\\"d7hhl\\\\\\">Intelligent notification system that is capable of learning from past user\\\\u2019s feedback to improve the accuracy of location based notification trigger.</p>\\", \\"id\\": \\"d7d50bbb-3d26-4ef6-b18c-346602d429cb\\"}, {\\"type\\": \\"image\\", \\"value\\": 81, \\"id\\": \\"1869609d-d152-4ea4-8e07-d334de1dbab3\\"}]", "project_list_bg_image": 74, "project_list_logo": 73, "project_list_heading": "Nyborhoods", "project_list_title": "web application,android and ios application", "project_list_subtitle": "Private Company", "bg_color": 75, "nextpage_bg_image": 82, "nextpage_logo": 83, "nextpage_heading": "anti money laundering platform for central banks and fics", "nextpage_title": "web application / analytics platform", "nextpage_subtitle": "Government", "nextpage_url": "http://127.0.0.1:8000/projects/transafe", "comments": []}	\N	16	1
13	f	2021-09-29 11:56:04.804673+05:30	{"pk": 12, "path": "0001000100050002", "depth": 4, "numchild": 0, "translation_key": "01294e77-6a62-45e5-be27-fd9d7834b80d", "locale": 1, "title": "isp", "draft_title": "isp", "slug": "isp", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/isp/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "ISP", "meta_content": null, "right_content_heading": "User and Billing Management application for ISPs", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"ov8d8\\\\\\"> Metis Subscriber Management System (SMS) and billing platform was developed for the largest broadband and cable TV distribution network of South India, Kerala Vision. Platform provided unique facilities to Internet Service Providers (ISPs) such as multi level partner system management, modular billing, marketing and promotional campaign management modules etc </p>\\", \\"id\\": \\"cc520515-459e-4d4e-9735-a14282e69067\\"}]", "timeline": "4 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"z4jy2\\\\\\"> Web Application, Mobile Apllication, Enterprise Integrations </p>\\", \\"id\\": \\"25299355-c4b7-4505-a8aa-0659f6ffa100\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1me9v\\\\\\"> Business process consulting; User experience design; Application Development; Partner Training Programs, Deployment and Support. </p>\\", \\"id\\": \\"5134b6e2-e028-40c4-b653-e5f9cd4e64f3\\"}]", "section1_image": 30, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 31, \\"id\\": \\"ee3df15e-5b5b-4c4e-9ecd-46fdc06cc417\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"zahrr\\\\\\"> </p><h3 data-block-key=\\\\\\"4mo3i\\\\\\"><b>Multi Tier Partner Management</b></h3><p data-block-key=\\\\\\"bruca\\\\\\">Application enables the central ISP to create, manage and serve its multi layer distribution network. Various users are present in the system according to various functions and hierarchy within the system</p>\\", \\"id\\": \\"8fec84e3-2e66-48c2-b629-890e7444ae90\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"zahrr\\\\\\"> </p><h3 data-block-key=\\\\\\"sbga\\\\\\"><b>Subscriber Management</b></h3><p data-block-key=\\\\\\"5o4r3\\\\\\">Application manages the entire lifecycle of an end-user acquired by the ISP. A subscriber\\\\u2019s recharges, plan changes, static IP allocation, auto login configurations, issue tracking and resolution etc are handled through various workflows within the application.</p>\\", \\"id\\": \\"19f05b27-2b05-467a-9599-3d025cbb45b2\\"}, {\\"type\\": \\"image\\", \\"value\\": 32, \\"id\\": \\"8d645b88-d4f5-4b80-b9f5-28e464898002\\"}]", "section2_image": 33, "section2_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"kpafw\\\\\\"> </p><h3 data-block-key=\\\\\\"e62ih\\\\\\"><b>Billing Management</b></h3><p data-block-key=\\\\\\"7kmca\\\\\\">Application also manages the billing, payment collection and revenue share functionalities for the ISP network. From billing and account management of individual subscribers to managing the overall ledger for the parent company, the application is designed to handle the entire billing system with multiple integrations into third party applications and services used by Kerala Vision.</p>\\", \\"id\\": \\"43935166-0de0-4c5b-a56a-57a8ee041070\\"}, {\\"type\\": \\"image\\", \\"value\\": 34, \\"id\\": \\"74e1593a-1a31-4924-8793-ae2fa99be117\\"}, {\\"type\\": \\"image\\", \\"value\\": 35, \\"id\\": \\"df77ec79-47d6-42af-83f4-28f1fdb5f857\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"kpafw\\\\\\"> </p><h3 data-block-key=\\\\\\"5j3je\\\\\\"><b>Self-Service portal</b></h3><p data-block-key=\\\\\\"6emch\\\\\\">Subscribers have their own self service portal and mobile application to not only monitor their account but also configure their plans, accounts and subscriptions of their choice.</p>\\", \\"id\\": \\"f73c58a0-1ef5-44fa-9214-d1d65d87f8a2\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"kpafw\\\\\\"> </p><h3 data-block-key=\\\\\\"clip4\\\\\\"><b>Extensive Role and Permission management</b></h3><p data-block-key=\\\\\\"63ms8\\\\\\">Highly configurable and secure role and permission management system is implemented in the application to enable the admin to create users with specific access into the system without any technical modification or interventions.</p>\\", \\"id\\": \\"2610c6a5-71a8-40ed-8955-7bc715a3fb19\\"}, {\\"type\\": \\"image\\", \\"value\\": 36, \\"id\\": \\"99569e46-c0e9-4bdb-9e9b-e8ceafa10929\\"}, {\\"type\\": \\"image\\", \\"value\\": 37, \\"id\\": \\"14962c68-a987-471c-adb5-ad37f734a28b\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"kpafw\\\\\\"> </p><h3 data-block-key=\\\\\\"6omsj\\\\\\"><b>Payment Gateway</b></h3><p data-block-key=\\\\\\"d2sql\\\\\\">Multiple payment gateway providers are integrated into the system to facilitate easy purchase and checkout for the customers while providing the necessary reporting and financial management tools for the management.</p>\\", \\"id\\": \\"006c390d-0524-43b5-80e4-1c45aec8ac6d\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"kpafw\\\\\\"> </p><h3 data-block-key=\\\\\\"fn8kn\\\\\\"><b>Automated Workflows</b></h3><p data-block-key=\\\\\\"d95o9\\\\\\">mDrift has automated most of the processes in the system, enabling the client to operate the network with minimal staff. Workflows such as revenue share, recharges, plan-changes, billing, issue resolution, account deactivation due to unauthorised activities etc are performed by the system automatically. Admin interventions are carefully designed to be crucial to monitoring and maintaining the system while being non-intrusive to provide the best experience to end-users.</p>\\", \\"id\\": \\"2f90a8c5-6e05-43fa-9519-b9925754aa0b\\"}, {\\"type\\": \\"image\\", \\"value\\": 38, \\"id\\": \\"031db736-88d3-4f1a-8aeb-72b5ad1d0e3f\\"}]", "project_list_bg_image": 27, "project_list_logo": 28, "project_list_heading": "user management and billing application for internet service providers", "project_list_title": "web application / mobile application / enterprise integrations", "project_list_subtitle": "Large Private Organisation", "bg_color": 29, "nextpage_bg_image": 39, "nextpage_logo": 40, "nextpage_heading": "news website for visually challenged persons", "nextpage_title": "web application / mobile application", "nextpage_subtitle": "Government Organisation", "nextpage_url": "http://127.0.0.1:8000/projects/8sidor", "comments": []}	\N	12	1
19	f	2021-09-29 12:57:03.672186+05:30	{"pk": 17, "path": "0001000100050007", "depth": 4, "numchild": 0, "translation_key": "3a3675da-6c5d-4c48-8b0f-b283872cd1f5", "locale": 1, "title": "ziggy", "draft_title": "ziggy", "slug": "ziggy", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/ziggy/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Ziggy", "meta_content": null, "right_content_heading": "Ziggy", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rt55a\\\\\\"> Ziggy Creative Colony is one of Scandinavia\\\\u2019s leading image consultancy companies. mDrift worked with Ziggy to develop their concept website showcasing the design and UX style and vision of the company. </p>\\", \\"id\\": \\"2910c4f2-9a01-4041-9783-cae2bb13fed0\\"}]", "timeline": "3 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hyegp\\\\\\"> Custom WordPress </p>\\", \\"id\\": \\"cd98594e-31ae-4682-a0dc-b872dfea5cf3\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1d3h7\\\\\\"> custom WordPress website, mobile dedicated pages for specific mobile devices, multilingual options for different geographic areas </p>\\", \\"id\\": \\"ff1eb63d-1db7-4eeb-b2db-60f9deaf0168\\"}]", "section1_image": 87, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 88, \\"id\\": \\"5a268de1-6c26-4236-b599-e2e0ecb00cb7\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"c5g05\\\\\\"> </p><h3 data-block-key=\\\\\\"cn56f\\\\\\"><b>In-depth Consulting Sessions</b></h3><p data-block-key=\\\\\\"f224d\\\\\\">Extensive design consultation sessions were conducted with the client to develop the final website as a cutting edge showcase of what technology and intelligent design can achieve.</p>\\", \\"id\\": \\"6476a76a-25d6-4832-99bc-ba467fb0b2db\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"c5g05\\\\\\"> </p><h3 data-block-key=\\\\\\"9vdno\\\\\\"><b>Custom CMS</b></h3><p data-block-key=\\\\\\"a7pr7\\\\\\">mDrift developed a custom CMS on python to adhere to the special design requirements of the website. Application could alter design, stricture and navigational features of the page based on content. Included in the system was SEO, to help achieve Google\\\\u2019s first page SERPs.</p>\\", \\"id\\": \\"e7143d64-f116-4b94-8da0-4911a4ad0a55\\"}, {\\"type\\": \\"image\\", \\"value\\": 89, \\"id\\": \\"8ca34230-91f0-407c-812e-a8a32d82a70e\\"}]", "section2_image": 90, "section2_content": "[{\\"type\\": \\"image\\", \\"value\\": 91, \\"id\\": \\"3c743fe0-e0a9-4703-a044-266a6e2d97f7\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"2xk8g\\\\\\"> </p><h3 data-block-key=\\\\\\"bd4gu\\\\\\"><b>Responsive Design</b></h3><p data-block-key=\\\\\\"bn63j\\\\\\">Website is capable of scaling across various devices and providing the best interaction and UX for the particular device. Interactions were custom designed JavaScript to work across devices</p>\\", \\"id\\": \\"79fb9559-9317-440b-a29a-1b22a31acc8c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"2xk8g\\\\\\"> </p><p data-block-key=\\\\\\"67spo\\\\\\"> </p><h3 data-block-key=\\\\\\"dcge9\\\\\\"><b>Multilingual Options</b></h3><p data-block-key=\\\\\\"4spi9\\\\\\">Website supported multiple languages for serving users across various geographies. Automatic translation mechanisms were implemented for quicker content generation.</p>\\", \\"id\\": \\"e2b3b1d2-c0f1-4411-9f84-4decd5d143d8\\"}, {\\"type\\": \\"image\\", \\"value\\": 92, \\"id\\": \\"78aa4042-1b94-4b81-a61e-ea65aa38604f\\"}]", "project_list_bg_image": 84, "project_list_logo": 85, "project_list_heading": "corporate website for scandinavian", "project_list_title": "website", "project_list_subtitle": "Private Company", "bg_color": 86, "nextpage_bg_image": 93, "nextpage_logo": 94, "nextpage_heading": "patient queueing and scheduling application for clinics", "nextpage_title": "web application / mobile application /website", "nextpage_subtitle": "Startup", "nextpage_url": "http://127.0.0.1:8000/projects/qaid", "comments": []}	\N	17	1
12	f	2021-09-29 11:14:17.986681+05:30	{"pk": 11, "path": "0001000100050001", "depth": 4, "numchild": 0, "translation_key": "46036e19-13b3-4531-8c0d-f82ace9f2e0a", "locale": 1, "title": "transafe", "draft_title": "transafe", "slug": "transafe", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/transafe/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Transafe", "meta_content": null, "right_content_heading": "Anti Money Laundering platform", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"tz7wc\\\\\\"> AML platform connects the Retails Banks and Non Banking Financial Institutes (NBFIs) with Financial Intelligence Centers (FICs) run under economic ministry of a country. Daily transaction reports are collected and analysed in real time to provide predictive AML analysis and transaction history investigation. </p>\\", \\"id\\": \\"3010b82d-4d86-4a6f-a5c4-1c2510db1a76\\"}]", "timeline": "6 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9fqi6\\\\\\"> Web application </p>\\", \\"id\\": \\"a30ce3e1-73a3-43ab-8f5a-cca8bd58d184\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"wlu3o\\\\\\"> Business process consulting; User experience design; Application Development; Deployment and Support </p>\\", \\"id\\": \\"f8ed0dec-af12-4318-a26e-c08a5a78b788\\"}]", "section1_image": 20, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 21, \\"id\\": \\"82186a6b-e652-44e9-8783-d5c958363401\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"eteko\\\\\\"> </p><h3 data-block-key=\\\\\\"6pvoj\\\\\\"><b>Data Collection</b></h3><p data-block-key=\\\\\\"97h3n\\\\\\">Daily transactional data from banks and NBFIs are collected into the system via Financial Institute portal. User accounts and special workflows are implemented in the system for financial institutes to ensure data accuracy and legal compliance.</p>\\", \\"id\\": \\"9340adbc-5e03-4558-937f-e4816a75016a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"eteko\\\\\\"> </p><h3 data-block-key=\\\\\\"dq19k\\\\\\"><b>Compliance Validation</b></h3><p data-block-key=\\\\\\"8mkpl\\\\\\">Application validates all uploaded transactional data with extensive rules customisable by FIC admins. Validation rules are configurable for specific report types such as Cash Transaction Report (CTRs), Electronic Transaction Report (ETR), Suspicious Transaction Report (STR) etc.</p>\\", \\"id\\": \\"647da9ad-0acc-4c4c-a819-2b175a6a4d1c\\"}, {\\"type\\": \\"image\\", \\"value\\": 22, \\"id\\": \\"f8ddcb75-78d4-4fff-bc28-f40c317957f4\\"}]", "section2_image": 23, "section2_content": "[{\\"type\\": \\"image\\", \\"value\\": 24, \\"id\\": \\"b6b29310-2c79-4e49-b7a2-ab67e007b69c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"vi43n\\\\\\"> </p><h3 data-block-key=\\\\\\"ctgne\\\\\\"><b>Link Analysis</b></h3><p data-block-key=\\\\\\"dgvsm\\\\\\">Analysts at FICs can search for transactions within the system and link analysis the cash flow between multiple persons and accounts for investigative process. Link analysis is based on pattern recognition and smart transactional modeling of gross cash flow.</p>\\", \\"id\\": \\"3278f0b3-8b4d-4573-b281-884153a1631a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"vi43n\\\\\\"> </p><h3 data-block-key=\\\\\\"eqpgk\\\\\\"><b>Statistical Reporting</b></h3><p data-block-key=\\\\\\"9ns7n\\\\\\">Platform provides extensive reporting on activities in the system and business intelligence based on transactional data. Report types include employee logs, Financial Institute performance, Document statistics and preconfigured national transactional reporting.</p>\\", \\"id\\": \\"93ef941b-8cec-4db0-9d5b-f5ee5835a196\\"}, {\\"type\\": \\"image\\", \\"value\\": 25, \\"id\\": \\"2cc7652a-4e9e-4143-bf0e-7cabe4909b50\\"}, {\\"type\\": \\"image\\", \\"value\\": 26, \\"id\\": \\"5e6035c0-97d0-4740-a721-655f7337c966\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"vi43n\\\\\\"> </p><h3 data-block-key=\\\\\\"1o5rq\\\\\\"><b>Behaviour Detection</b></h3><p data-block-key=\\\\\\"d8tmb\\\\\\">Rule based transactional modelling to identify customer behaviour and classification. A custom scoring system provides analysts with critical information on various user characters</p>\\", \\"id\\": \\"b014134b-0618-4b89-a1e8-c1e254d2ef7c\\"}]", "project_list_bg_image": 17, "project_list_logo": 18, "project_list_heading": "Anti Money Laundering platform for Central Banks and FICS", "project_list_title": "Web Application / Analytics Platform", "project_list_subtitle": "Government", "bg_color": 19, "nextpage_bg_image": 27, "nextpage_logo": 28, "nextpage_heading": "user management and billing application for internet service providers", "nextpage_title": "web application / mobile application / enterprise integration", "nextpage_subtitle": "Large private organisation", "nextpage_url": "http://127.0.0.1:8000/projects/transafe", "comments": []}	\N	11	1
14	f	2021-09-29 11:56:58.842769+05:30	{"pk": 11, "path": "0001000100050001", "depth": 4, "numchild": 0, "translation_key": "46036e19-13b3-4531-8c0d-f82ace9f2e0a", "locale": 1, "title": "transafe", "draft_title": "transafe", "slug": "transafe", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/transafe/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": "2021-09-29T05:44:18.030Z", "last_published_at": "2021-09-29T05:44:18.030Z", "latest_revision_created_at": "2021-09-29T05:44:17.986Z", "live_revision": 12, "alias_of": null, "header_title": "Transafe", "meta_content": null, "right_content_heading": "Anti Money Laundering platform", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"tz7wc\\\\\\">AML platform connects the Retails Banks and Non Banking Financial Institutes (NBFIs) with Financial Intelligence Centers (FICs) run under economic ministry of a country. Daily transaction reports are collected and analysed in real time to provide predictive AML analysis and transaction history investigation.</p>\\", \\"id\\": \\"3010b82d-4d86-4a6f-a5c4-1c2510db1a76\\"}]", "timeline": "6 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9fqi6\\\\\\">Web application</p>\\", \\"id\\": \\"a30ce3e1-73a3-43ab-8f5a-cca8bd58d184\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"wlu3o\\\\\\">Business process consulting; User experience design; Application Development; Deployment and Support</p>\\", \\"id\\": \\"f8ed0dec-af12-4318-a26e-c08a5a78b788\\"}]", "section1_image": 20, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 21, \\"id\\": \\"82186a6b-e652-44e9-8783-d5c958363401\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"eteko\\\\\\"></p><h3 data-block-key=\\\\\\"6pvoj\\\\\\"><b>Data Collection</b></h3><p data-block-key=\\\\\\"97h3n\\\\\\">Daily transactional data from banks and NBFIs are collected into the system via Financial Institute portal. User accounts and special workflows are implemented in the system for financial institutes to ensure data accuracy and legal compliance.</p>\\", \\"id\\": \\"9340adbc-5e03-4558-937f-e4816a75016a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"eteko\\\\\\"></p><h3 data-block-key=\\\\\\"dq19k\\\\\\"><b>Compliance Validation</b></h3><p data-block-key=\\\\\\"8mkpl\\\\\\">Application validates all uploaded transactional data with extensive rules customisable by FIC admins. Validation rules are configurable for specific report types such as Cash Transaction Report (CTRs), Electronic Transaction Report (ETR), Suspicious Transaction Report (STR) etc.</p>\\", \\"id\\": \\"647da9ad-0acc-4c4c-a819-2b175a6a4d1c\\"}, {\\"type\\": \\"image\\", \\"value\\": 22, \\"id\\": \\"f8ddcb75-78d4-4fff-bc28-f40c317957f4\\"}]", "section2_image": 23, "section2_content": "[{\\"type\\": \\"image\\", \\"value\\": 24, \\"id\\": \\"b6b29310-2c79-4e49-b7a2-ab67e007b69c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"vi43n\\\\\\"></p><h3 data-block-key=\\\\\\"ctgne\\\\\\"><b>Link Analysis</b></h3><p data-block-key=\\\\\\"dgvsm\\\\\\">Analysts at FICs can search for transactions within the system and link analysis the cash flow between multiple persons and accounts for investigative process. Link analysis is based on pattern recognition and smart transactional modeling of gross cash flow.</p>\\", \\"id\\": \\"3278f0b3-8b4d-4573-b281-884153a1631a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"vi43n\\\\\\"></p><h3 data-block-key=\\\\\\"eqpgk\\\\\\"><b>Statistical Reporting</b></h3><p data-block-key=\\\\\\"9ns7n\\\\\\">Platform provides extensive reporting on activities in the system and business intelligence based on transactional data. Report types include employee logs, Financial Institute performance, Document statistics and preconfigured national transactional reporting.</p>\\", \\"id\\": \\"93ef941b-8cec-4db0-9d5b-f5ee5835a196\\"}, {\\"type\\": \\"image\\", \\"value\\": 25, \\"id\\": \\"2cc7652a-4e9e-4143-bf0e-7cabe4909b50\\"}, {\\"type\\": \\"image\\", \\"value\\": 26, \\"id\\": \\"5e6035c0-97d0-4740-a721-655f7337c966\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"vi43n\\\\\\"></p><h3 data-block-key=\\\\\\"1o5rq\\\\\\"><b>Behaviour Detection</b></h3><p data-block-key=\\\\\\"d8tmb\\\\\\">Rule based transactional modelling to identify customer behaviour and classification. A custom scoring system provides analysts with critical information on various user characters</p>\\", \\"id\\": \\"b014134b-0618-4b89-a1e8-c1e254d2ef7c\\"}]", "project_list_bg_image": 17, "project_list_logo": 18, "project_list_heading": "Anti Money Laundering platform for Central Banks and FICS", "project_list_title": "Web Application / Analytics Platform", "project_list_subtitle": "Government", "bg_color": 19, "nextpage_bg_image": 27, "nextpage_logo": 28, "nextpage_heading": "user management and billing application for internet service providers", "nextpage_title": "web application / mobile application / enterprise integration", "nextpage_subtitle": "Large private organisation", "nextpage_url": "http://127.0.0.1:8000/projects/isp", "comments": []}	\N	11	1
15	f	2021-09-29 12:04:29.838709+05:30	{"pk": 13, "path": "0001000100050003", "depth": 4, "numchild": 0, "translation_key": "ed4437e6-97bc-4ea5-a6a9-7f68ff6486a7", "locale": 1, "title": "8sidor", "draft_title": "8sidor", "slug": "8sidor", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/8sidor/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "8Sidor", "meta_content": null, "right_content_heading": "8 Sidor", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hx6uw\\\\\\"> 8sidor.se is a Swedish news portal that seeks to serve people with visual and auditory disabilities. mDrift implemented special tools and features into the website to ensure that everybody could navigate the website and consume news articles to stay updated on current events and other reported content. 8Sidor news agency was later then acquired by Swedish government due to its much revered service to their citizens with disabilities. </p>\\", \\"id\\": \\"b83a4605-6ba0-4187-bc53-e1190fcc2d31\\"}]", "timeline": "1.5 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"ls071\\\\\\"> WordPress CMS, Mobile Application, Custom tools and experience </p>\\", \\"id\\": \\"ae36313d-8541-4743-9ba4-78b095a68526\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"977l5\\\\\\"> Use cases development, User Experience Design, Application Development, Support and Maintenance </p>\\", \\"id\\": \\"121cdb39-624c-4fb6-b58f-530bc9db9c2b\\"}]", "section1_image": 42, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 43, \\"id\\": \\"dee97a88-0018-4674-b578-2a106278cf1a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5c9u5\\\\\\"> </p><h3 data-block-key=\\\\\\"cc9j\\\\\\"><b>Custom navigation design</b></h3><p data-block-key=\\\\\\"2nncr\\\\\\">8Sidor website had to be designed primarily for use by people with disabilities and hence navigation of the website was of primary concern from the get go. mDrift designed and implemented various tools such as JavaScript based on-screen readouts, automatically video subtitling, navigational callouts, automatic alt name generators for images in articles etc</p>\\", \\"id\\": \\"e934d9c5-86cd-46e2-9690-2b6928ed0b9d\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5c9u5\\\\\\"> </p><h3 data-block-key=\\\\\\"9rpjl\\\\\\"><b>Custom Search</b></h3><p data-block-key=\\\\\\"6mbuf\\\\\\">Due to expected lack of tech savviness of target user group and high categorization of news in the website, search logic was custom built from scratch to search for and present results using ranking on readability and importance.</p>\\", \\"id\\": \\"802b4420-300b-4ebd-9cb8-fb359a2589ad\\"}, {\\"type\\": \\"image\\", \\"value\\": 44, \\"id\\": \\"e13933b2-dd00-48e9-bc16-ce822e339ac5\\"}]", "section2_image": 45, "section2_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hv0d4\\\\\\"> </p><h3 data-block-key=\\\\\\"19vrl\\\\\\"><b>Migrating &amp; merging legacy content</b></h3><p data-block-key=\\\\\\"71vho\\\\\\">Original 8Sidor website &amp; its political news section \\\\u2018Alla Valjare\\\\u2019 was built on separate platforms Umbraco &amp; WordPress respectively. mDrift migrated &amp; merged both websites and ran custom scripting to identify and remove duplicate content published on both website.</p>\\", \\"id\\": \\"26b86eb1-9619-4a4f-b73a-6e84e4ecb9d3\\"}, {\\"type\\": \\"image\\", \\"value\\": 46, \\"id\\": \\"3e43cdc0-6fe4-4bd0-a031-8161a156c4b0\\"}, {\\"type\\": \\"image\\", \\"value\\": 47, \\"id\\": \\"a15b06e0-6230-4c96-a374-95d265c2ab7a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"hv0d4\\\\\\"> </p><h3 data-block-key=\\\\\\"nbhe\\\\\\"><b>Large Database</b></h3><p data-block-key=\\\\\\"8sr53\\\\\\">When both the clients websites were merged together, the database resulted in a combined size not suitable for WordPress Content Management System preferred by the client. We did extensive optimization on WordPress core as well as frontend JavaScript to make the website nimble and responsive.</p>\\", \\"id\\": \\"5ba7b6c0-c36c-4cf9-826c-1b42f7cb9e19\\"}]", "project_list_bg_image": 39, "project_list_logo": 40, "project_list_heading": "news website for visually challenged persons", "project_list_title": "web application / mobile application", "project_list_subtitle": "Government Organisation", "bg_color": 41, "nextpage_bg_image": 48, "nextpage_logo": 49, "nextpage_heading": "music editing hardware and software platform", "nextpage_title": "website / e-commerce store", "nextpage_subtitle": "Private Company", "nextpage_url": "http://127.0.0.1:8000/projects/propellerhead", "comments": []}	\N	13	1
20	f	2021-09-29 13:03:20.962148+05:30	{"pk": 18, "path": "0001000100050008", "depth": 4, "numchild": 0, "translation_key": "0e7b4361-d9b5-4313-96e8-4c5907f23a10", "locale": 1, "title": "qaid", "draft_title": "qaid", "slug": "qaid", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/qaid/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "QAid", "meta_content": null, "right_content_heading": "Q-Aid", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"h32eo\\\\\\"> </p><p data-block-key=\\\\\\"6pf3d\\\\\\">Q-Aid is a product aimed at reducing wait time at clinics and enable customers to book appointments and arrive at clinics precisely for their appointment. mDrift consulted the entire development and implementation of the application.</p><p data-block-key=\\\\\\"aaq3e\\\\\\">As the primary aim of the application was to reduce wait time at clinic, application had to efficiently calculate processing times at clinics for each token and balance the commute time from user\\\\u2019s current location to clinic.</p>\\", \\"id\\": \\"5bfd43c1-68c6-42fc-bbb6-6bc224f1eebd\\"}]", "timeline": "3 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"nk7bs\\\\\\"> Android, iOS, web (both native apps), data management </p>\\", \\"id\\": \\"595425cd-b33d-465f-901c-5b07a8f4b502\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"uh2eg\\\\\\"> custom web and mobile app, patient application, clinic application, queue management, route management </p>\\", \\"id\\": \\"7ad8a910-e987-4d64-828a-4713332f9d13\\"}]", "section1_image": 96, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 97, \\"id\\": \\"4c4ca6ca-adc2-4a07-9154-98ee40c214ad\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1m2mp\\\\\\"> </p><h3 data-block-key=\\\\\\"5jt80\\\\\\"><b>Clinic Queue Management</b></h3><p data-block-key=\\\\\\"3thtu\\\\\\">Clinic application deployed at partner clinics enabled them to manage their patient queue electronically and provided reporting options for better insights into the business.</p>\\", \\"id\\": \\"38ccd5e4-0478-4ae3-8e64-71bd0dde55d6\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"1m2mp\\\\\\"> </p><h3 data-block-key=\\\\\\"a2dcs\\\\\\"><b>Custom Notification Triggers</b></h3><p data-block-key=\\\\\\"6osbk\\\\\\">Users can choose to get notified right on time to start travelling or for any earlier time.</p>\\", \\"id\\": \\"f85bbb33-4e52-4b96-9c8f-2cc9e0a581f7\\"}, {\\"type\\": \\"image\\", \\"value\\": 98, \\"id\\": \\"262396de-06ff-4b3e-94fe-131ffac3a529\\"}]", "section2_image": 99, "section2_content": "[{\\"type\\": \\"image\\", \\"value\\": 100, \\"id\\": \\"3f5b2cf2-7452-4db9-9dee-f5b1dfcabfed\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"s3s1g\\\\\\"> </p><h3 data-block-key=\\\\\\"fkhet\\\\\\"><b>Clinic Discovery and Rating</b></h3><p data-block-key=\\\\\\"chkr9\\\\\\">Application also provided end customers to browse various clinics in the region and read reviews about them. This provided users with live token count, expected wait time etc.</p>\\", \\"id\\": \\"b1faae40-5fcd-411c-8b08-83bf8fb45f21\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"s3s1g\\\\\\"> </p><h3 data-block-key=\\\\\\"fgh1d\\\\\\"><b>Custom Functionality</b></h3><p data-block-key=\\\\\\"867a5\\\\\\">A custom algorithm for efficiently calculating the time user has to start travelling to the clinic. This was done dynamically based on token completion at clinics. Implemented machine learning algorithms to better predict wait time per token at each registered clinic, increasing the accuracy of notifications sent to end users for travelling to the clinic.</p>\\", \\"id\\": \\"892abea5-1bbe-4b0d-bb7a-1b8787f0a3e6\\"}, {\\"type\\": \\"image\\", \\"value\\": 101, \\"id\\": \\"85517cab-de09-4e0c-8a86-5cc65e5aafd0\\"}]", "project_list_bg_image": 93, "project_list_logo": 94, "project_list_heading": "patient queueing and scheduling application for clinics", "project_list_title": "web application/mobile application/website", "project_list_subtitle": "Startup", "bg_color": 95, "nextpage_bg_image": 102, "nextpage_logo": 103, "nextpage_heading": "project management tool for sbms", "nextpage_title": "web application", "nextpage_subtitle": "SAAS", "nextpage_url": "http://127.0.0.1:8000/projects/rapiddev", "comments": []}	\N	18	1
16	f	2021-09-29 12:13:46.403473+05:30	{"pk": 14, "path": "0001000100050004", "depth": 4, "numchild": 0, "translation_key": "ce8ae964-dd76-4c3e-aafd-f9b4a3a43fa3", "locale": 1, "title": "propellerhead", "draft_title": "propellerhead", "slug": "propellerhead", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/propellerhead/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Propellerhead", "meta_content": null, "right_content_heading": "Propellerhead", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"ibbqu\\\\\\"> Propellerheads, a swedish music company, offers software and hardware products for music editing, playback etc. mDrift worked very closely with Propellerheads technical team to design and develop a new look and feel to their online business while making sure their existing services are well integrated into the platform so as to minimise the impact on day to day company operations. </p>\\", \\"id\\": \\"e2719908-db9b-457c-a228-ea594eebba06\\"}]", "timeline": "4 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"gk3ln\\\\\\"> Web application, Mobile Application, Enterprise Integrations </p>\\", \\"id\\": \\"9f00c085-6118-4f53-9230-d7d91cc37611\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"32u0h\\\\\\"> Business process consulting; User experience design; Application Development; Partner Training Programs, Deployment and Support </p>\\", \\"id\\": \\"155834e2-fa7a-40ed-96eb-ab06740b6295\\"}]", "section1_image": 51, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 52, \\"id\\": \\"4480b281-aec4-44f5-ae42-0503c59fcdab\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rxdua\\\\\\"> </p><h3 data-block-key=\\\\\\"3a32g\\\\\\"><b>Client Customizability</b></h3><p data-block-key=\\\\\\"38bbf\\\\\\">Since the application was custom built on Python and client already had a technical team managing their product and environment, one of the targets from initial days was the code to be highly modular and configurable. This allowed client\\\\u2019s technical team to re-organise content and functionalities on the website without external help. Custom development on python was chosen as no existing CMS could cater to the client\\\\u2019s specific requirements and integrations.</p>\\", \\"id\\": \\"d74eddd9-9931-4e62-a051-98d576c36747\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"rxdua\\\\\\"> </p><h3 data-block-key=\\\\\\"9puut\\\\\\"><b>Hardware and Software based product sales</b></h3><p data-block-key=\\\\\\"85h4c\\\\\\">User experience on the website was designed to be consistent across purchase of software and hardware products. A visitor should feel comfortable about purchasing an online subscription and go right to ordering an equipment to home and providing the shipping details.</p>\\", \\"id\\": \\"7416d7a8-262d-43ce-86c1-83aa3c00fe6e\\"}, {\\"type\\": \\"image\\", \\"value\\": 53, \\"id\\": \\"849f0cb0-34ff-4ea3-91dc-9fca9867feb8\\"}]", "section2_image": 55, "section2_content": "[{\\"type\\": \\"image\\", \\"value\\": 56, \\"id\\": \\"c9e7db76-4b06-4910-b617-bf100cd3661d\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5vsy8\\\\\\"> </p><h3 data-block-key=\\\\\\"ftkh8\\\\\\"><b>Enterprise Integration</b></h3><p data-block-key=\\\\\\"8nbj1\\\\\\">Integrated into multiple existing services, systems and third party services subscribed to by client to make the new web experience as simple to transition onto for both customers and internal team.</p>\\", \\"id\\": \\"dfde8ca8-df2d-4dd1-bc0f-ea15f71528c6\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5vsy8\\\\\\"> </p><h3 data-block-key=\\\\\\"94i4c\\\\\\"><b>Custom inline music player</b></h3><p data-block-key=\\\\\\"aahm3\\\\\\">Specially designed custom music player was developed to integrate into client\\\\u2019s existing proprietary content distribution platform. This allowed the visitors on the website to play and hear music without external player load or wait times.</p>\\", \\"id\\": \\"62da4366-d11d-462f-aab2-abbef214e645\\"}, {\\"type\\": \\"image\\", \\"value\\": 57, \\"id\\": \\"e436d3ee-be70-417f-b194-f02acd7dd046\\"}, {\\"type\\": \\"image\\", \\"value\\": 58, \\"id\\": \\"3f395184-03cd-4502-9c3b-8ac3792f5c40\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"5vsy8\\\\\\"> </p><h3 data-block-key=\\\\\\"bjtos\\\\\\"><b>Custom Blog Module</b></h3><p data-block-key=\\\\\\"bdbh9\\\\\\">Custom blog module was developed for admin with capabilities such as automatic content validations, publisher management, user rewards module etc.</p>\\", \\"id\\": \\"f2def0e0-37dd-40ef-9683-73ed1161abc7\\"}]", "project_list_bg_image": 48, "project_list_logo": 49, "project_list_heading": "music editing hardware and software platform", "project_list_title": "website/ e-commerce store", "project_list_subtitle": "Private Company", "bg_color": 50, "nextpage_bg_image": 59, "nextpage_logo": 60, "nextpage_heading": "geological subsurface visualization tool", "nextpage_title": "windows desktop application,web application", "nextpage_subtitle": "Private Company", "nextpage_url": "http://127.0.0.1:8000/projects/rocxia", "comments": []}	\N	14	1
17	f	2021-09-29 12:41:16.912879+05:30	{"pk": 15, "path": "0001000100050005", "depth": 4, "numchild": 0, "translation_key": "ab985aff-b789-4f9c-a61a-d23cef051713", "locale": 1, "title": "rocxia", "draft_title": "rocxia", "slug": "rocxia", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/rocxia/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Rocxia", "meta_content": null, "right_content_heading": "ROCXIA", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"k64xn\\\\\\"> ROCXIA is a geological subsurface visualization tool used in analysis and discovery of oil and other rare elements. Development based on core C++ and visualizations powered by OpenGL stack let mDrift develop a high performance application that is also efficient in system resource utilization. Along with a web based project management application built on Python Django, administrators could remotely employee and manages personnel and operate a subsurface analysis team. </p>\\", \\"id\\": \\"8efc9ffb-63a1-4631-9dbc-05b0838e6a55\\"}]", "timeline": "11 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"gi8im\\\\\\"> Windows Desktop Application, Web Application </p>\\", \\"id\\": \\"7710d7b9-c0a3-4fc4-9655-c1b91f1c9b3c\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"e2gfi\\\\\\"> Scientific Research, Subject Matter Expert Panel, From scratch development, User Experience design </p>\\", \\"id\\": \\"944aa116-c4c5-4976-95df-de32a2ab6969\\"}]", "section1_image": 62, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 63, \\"id\\": \\"6500b339-2d78-4257-a28b-e28d17f8f471\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"7t48a\\\\\\"> </p><h3 data-block-key=\\\\\\"300fn\\\\\\"><b>Core C++ Development</b></h3><p data-block-key=\\\\\\"a2f7b\\\\\\">Entire project was developed on C++ to get microscopic control of the project and its deliverables. One of the major challenges of application development was to handle the huge data sizes and maintain control on processes to ensure optimal runtime and efficiency</p>\\", \\"id\\": \\"3bfc1ffb-dd13-4b44-a534-3bddf68d8413\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"7t48a\\\\\\"> </p><h3 data-block-key=\\\\\\"9599p\\\\\\"><b>High Accuracy</b></h3><p data-block-key=\\\\\\"6ibot\\\\\\">Application was intended to be used by companies to accurately predict the location of oil within subsurface layers and develop well trajectories for drilling crews. The accuracy of such an analysis was required to be between 5-10 meters. Data is handled extremely accurately to ensure no quality loss and highest regard for process runtime optimisation.</p>\\", \\"id\\": \\"ba820539-46b2-4e33-991d-be9f8b611659\\"}, {\\"type\\": \\"image\\", \\"value\\": 64, \\"id\\": \\"a190bad4-6a66-4c0f-adae-2df837fa062f\\"}]", "section2_image": 65, "section2_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"qsmv4\\\\\\"> </p><h3 data-block-key=\\\\\\"96a5v\\\\\\"><b>Research into Exploration Geophysics</b></h3><p data-block-key=\\\\\\"2q21p\\\\\\">mDrift\\\\u2019s team spent the initial few months learning the field of Exploration Geophysics. This helped the team to not only understand the nuances of the development required but also to communicate with the client much more effectively and efficiently.</p>\\", \\"id\\": \\"e41a37ed-e0d2-42ca-a2ad-f5101d4a42a2\\"}, {\\"type\\": \\"image\\", \\"value\\": 66, \\"id\\": \\"e0e6a35f-3c49-45f2-a237-4582ed06a856\\"}, {\\"type\\": \\"image\\", \\"value\\": 67, \\"id\\": \\"bdb32d37-3d35-4bba-be5e-a9958e372c4d\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"qsmv4\\\\\\"> </p><h3 data-block-key=\\\\\\"5mnkk\\\\\\"><b>Huge file sizes</b></h3><p data-block-key=\\\\\\"1h1ug\\\\\\">SEGY files that contain all the trace data sets that make up the subsurface data set can range in size from 10s to 100s of GBs. The application needed to be able to ingest and maintain the whole data set on memory to quickly visualise and analyse the data set</p>\\", \\"id\\": \\"fed7bf68-1fc1-4d98-b23d-a730a6719bf2\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"qsmv4\\\\\\"> </p><h3 data-block-key=\\\\\\"86aha\\\\\\"><b>Custom triangulation logic</b></h3><p data-block-key=\\\\\\"5go3m\\\\\\">mDrift had to develop custom triangulation logic to visualize complex underground surface structures. The point cloud data pertaining to the surface is sorted through, analysed and triangulated using a custom logic developed on top of Delaunay triangulation to achieve under visualisation of data under 0.5 second time and posting cutting edge timeline and capabilities.</p>\\", \\"id\\": \\"87827cb4-8c4f-4d85-bf36-3ee7e86767d2\\"}, {\\"type\\": \\"image\\", \\"value\\": 68, \\"id\\": \\"17f12a32-1371-4eb4-a8ee-a7624e872543\\"}, {\\"type\\": \\"image\\", \\"value\\": 69, \\"id\\": \\"db24a800-3c54-4469-a15b-03db21103099\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"qsmv4\\\\\\"> </p><h3 data-block-key=\\\\\\"btgsn\\\\\\"><b>OpenGL capabilities</b></h3><p data-block-key=\\\\\\"5csf9\\\\\\">Application was required to have extensive OpenGL capabilities to visualise such a large data set and handles all this data via intelligent load into memory. This visualised data is used as a base to analyse and operate upon using various tools and functionalities such as volume cutter, surface detection, trajectory analysis, LAS property projection etc.</p>\\", \\"id\\": \\"1a93d588-db46-4fdb-8b8c-0ede793a07cd\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"qsmv4\\\\\\"> </p><h3 data-block-key=\\\\\\"bb69o\\\\\\"><b>Shaders Methodology</b></h3><p data-block-key=\\\\\\"2514u\\\\\\">Application extensively used the GPU for computation and visualisation while utilising the CPU for organising the information being passed to the GPU</p>\\", \\"id\\": \\"ffec25f0-aaf6-4db8-bea4-8a0d7adb89c1\\"}, {\\"type\\": \\"image\\", \\"value\\": 70, \\"id\\": \\"69054b25-a495-453d-a32f-5de240ea2acd\\"}, {\\"type\\": \\"image\\", \\"value\\": 71, \\"id\\": \\"fce25792-b8ff-4b02-8b4b-f695799c3a4f\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"qsmv4\\\\\\"> </p><h3 data-block-key=\\\\\\"dogoc\\\\\\"><b>Multithreading</b></h3><p data-block-key=\\\\\\"benjp\\\\\\">Application needed to be optimised for multithreaded workload to optimise for multiple parallel functionalities being required of the application.</p>\\", \\"id\\": \\"e34e2ba7-21be-4387-9aaa-e2a9b6523fff\\"}]", "project_list_bg_image": 59, "project_list_logo": 60, "project_list_heading": "Geological subsurface visualization tool", "project_list_title": "windows desktop application, web application", "project_list_subtitle": "Private Company", "bg_color": 61, "nextpage_bg_image": 72, "nextpage_logo": 73, "nextpage_heading": "nyborhoods", "nextpage_title": "web application,android and ios application", "nextpage_subtitle": "Private Company", "nextpage_url": "http://127.0.0.1:8000/projects/nyborhoods", "comments": []}	\N	15	1
21	f	2021-09-29 13:11:01.224671+05:30	{"pk": 19, "path": "0001000100050009", "depth": 4, "numchild": 0, "translation_key": "034210df-3ae4-4552-91f4-235169525414", "locale": 1, "title": "rapiddev", "draft_title": "rapiddev", "slug": "rapiddev", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/rapiddev/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "RapidDev", "meta_content": null, "right_content_heading": "Rapid Dev", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"6goqu\\\\\\"> RapidDev is a project management application aimed at software companies using high volume agile project management processes. RapidDev integrated all the operation requirements of a company such a project planning, error reporting &amp; CRM into a single package that integrated with each other for state of the art reporting on company status and project financials. </p>\\", \\"id\\": \\"dd8761f0-fdcd-498e-a10a-c7b8814bda01\\"}]", "timeline": "4 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fmper\\\\\\"> Python Django </p>\\", \\"id\\": \\"70026ca3-afa3-4afa-abb3-6d78c32f0cd9\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"xypvh\\\\\\"> SaaS architecture for a high performance application, high volume data analytics, mobile responsive </p>\\", \\"id\\": \\"7ab3fae2-1b53-458b-bd7a-8f57b5990ee8\\"}]", "section1_image": 106, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 107, \\"id\\": \\"a72655e8-f45c-499c-b230-01cb125270a2\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9krl5\\\\\\"> </p><p data-block-key=\\\\\\"4ju86\\\\\\"> </p><h3 data-block-key=\\\\\\"dt6ld\\\\\\"><b>Gantt Chart Interface</b></h3><p data-block-key=\\\\\\"9cfh7\\\\\\">A gantt chart based interface is developed for users to plan the project across various resources, teams, geographies etc. The UI was designed to work with the latest HTML5 capabilities of the browser requiring less processing power for huge data sets.</p>\\", \\"id\\": \\"e446bfd9-7ade-45de-a722-168bacc7e3a7\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"9krl5\\\\\\"> </p><h3 data-block-key=\\\\\\"6kldu\\\\\\"><b>Bug Reporting</b></h3><p data-block-key=\\\\\\"bu6nq\\\\\\">Application provides in system functionalities for reporting and tracking bugs in the project.</p>\\", \\"id\\": \\"b3b518ac-d529-48ef-bf9c-887871fc7545\\"}, {\\"type\\": \\"image\\", \\"value\\": 108, \\"id\\": \\"c990fabb-f7c6-43f6-9ade-7b6eb831c3fd\\"}]", "section2_image": 109, "section2_content": "[{\\"type\\": \\"image\\", \\"value\\": 110, \\"id\\": \\"665d6f7e-734c-4f36-841a-c7b78ba58635\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fvscv\\\\\\"> </p><h3 data-block-key=\\\\\\"qdv0\\\\\\"><b>BI &amp; Reporting</b></h3><p data-block-key=\\\\\\"9kop6\\\\\\">Extensive reporting is provided on the projects and task status with more analysis and prediction available on query. Project performance reporting and prediction tools based on Monte Carlo Simulations for analysing the current status of each project.</p>\\", \\"id\\": \\"65e1f5d6-fa88-432c-84c1-e348edf603f0\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"fvscv\\\\\\"> </p><h3 data-block-key=\\\\\\"el931\\\\\\"><b>Extensive User and Role Management</b></h3><p data-block-key=\\\\\\"47vv1\\\\\\">Application provides extensive user and permission management to customise the application to each company\\\\u2019s structure and processes.</p>\\", \\"id\\": \\"46493e13-9b92-4def-a794-15307c7cd495\\"}, {\\"type\\": \\"image\\", \\"value\\": 111, \\"id\\": \\"6f1e39ea-0dde-4b58-a92d-bcd0c85d5311\\"}]", "project_list_bg_image": 104, "project_list_logo": 103, "project_list_heading": "project management tool for sbms", "project_list_title": "web application", "project_list_subtitle": "SAAS", "bg_color": 105, "nextpage_bg_image": 112, "nextpage_logo": 113, "nextpage_heading": "business process management application for a real estate company", "nextpage_title": "web application / mobile application", "nextpage_subtitle": "Private Conglomerate", "nextpage_url": "http://127.0.0.1:8000/projects/khoon", "comments": []}	\N	19	1
22	f	2021-09-29 13:20:47.883077+05:30	{"pk": 20, "path": "000100010005000A", "depth": 4, "numchild": 0, "translation_key": "8020cf78-bc76-4308-b8a6-fdfe05a73df5", "locale": 1, "title": "khoon", "draft_title": "khoon", "slug": "khoon", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/khoon/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "Khoon", "meta_content": null, "right_content_heading": "Khoon Engineering", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"szw8l\\\\\\"> Khoon engineering Pte Ltd from Singapore needed an application to digitise the QA/QC of building constructions undertaken for seamless coordination and quicker issue closure. Application had to have excellent geo-positioning capabilities including built in compass functionalities to map and navigate a large construction project. Application had to handle most of the functionalities locally on mobile due to expected lack of connectivity in development areas </p>\\", \\"id\\": \\"e195a8c8-4ab9-4e6e-aee5-5fba4f94d1ec\\"}]", "timeline": "2.5 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"644nu\\\\\\"> Android, Web (Python) </p>\\", \\"id\\": \\"17910343-c958-4f30-be6f-b1867e36256f\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"7t1iw\\\\\\"> Web and mobile applications, custom image processing, storage management solutions </p>\\", \\"id\\": \\"28820a2a-0e90-4172-805d-4107b3300557\\"}]", "section1_image": 115, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 116, \\"id\\": \\"3ada8d8b-2b4f-4c9c-9e55-757c5019dd1c\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o13a4\\\\\\"> </p><h3 data-block-key=\\\\\\"k8k1\\\\\\"><b>Detailed POC Approval</b></h3><p data-block-key=\\\\\\"3d9ni\\\\\\">A Proof Of Concept (POC) application to field test specific functionalities of the application and gather feedback from engineers, mechanics and other workers.</p>\\", \\"id\\": \\"eb494d67-7dd7-4731-9a3c-ad7e3fa3f848\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"o13a4\\\\\\"> </p><h3 data-block-key=\\\\\\"6a1r6\\\\\\"><b>Site Navigation</b></h3><p data-block-key=\\\\\\"90hdd\\\\\\">Ability to upload sitemaps during project crate for on-site navigational capabilities for app users.</p>\\", \\"id\\": \\"c07c08ca-173e-49cb-8f3b-81bc694d0a69\\"}, {\\"type\\": \\"image\\", \\"value\\": 117, \\"id\\": \\"7f657d8d-b7c6-4785-8241-e8a0b90241da\\"}]", "section2_image": 118, "section2_content": "[{\\"type\\": \\"image\\", \\"value\\": 119, \\"id\\": \\"a8ee3f58-e814-4bae-8cc4-05e9ea00bfcd\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"dp0ey\\\\\\"> </p><h3 data-block-key=\\\\\\"eensk\\\\\\"><b>Image Editing</b></h3><p data-block-key=\\\\\\"7qj72\\\\\\">mDrift developed custom image annotation and commenting features for various users to coordinate on-site issues using pictures for clearer communication</p>\\", \\"id\\": \\"f69af972-2de5-44ea-ad8f-bea53ceceefe\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"dp0ey\\\\\\"> </p><h3 data-block-key=\\\\\\"12jk9\\\\\\"><b>Offsite Backup</b></h3><p data-block-key=\\\\\\"bl5hj\\\\\\">Since QA/QC data is highly important for the client, we developed and implemented offsite backup solution to a remote NAS. This could be used to restore or error correct server data as well.</p>\\", \\"id\\": \\"69af394b-6e82-49ad-8db9-b5c85d036e16\\"}, {\\"type\\": \\"image\\", \\"value\\": 120, \\"id\\": \\"8f0b8bf6-33be-4b97-8965-fcddf5abb66e\\"}]", "project_list_bg_image": 112, "project_list_logo": 113, "project_list_heading": "business process management application for a real estate company", "project_list_title": "web application / mobile application", "project_list_subtitle": "Private Conglomerate", "bg_color": 114, "nextpage_bg_image": 121, "nextpage_logo": 122, "nextpage_heading": "core business application for corporate healthcare consultancy", "nextpage_title": "website / web application /enterprise integrations", "nextpage_subtitle": "Private Company", "nextpage_url": null, "comments": []}	\N	20	1
23	f	2021-09-29 13:32:12.107174+05:30	{"pk": 21, "path": "000100010005000B", "depth": 4, "numchild": 0, "translation_key": "a3b69ad8-92d7-4638-964f-0529ffa46017", "locale": 1, "title": "lcm", "draft_title": "lcm", "slug": "lcm", "content_type": 49, "live": true, "has_unpublished_changes": false, "url_path": "/home/projects/lcm/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": "LCM", "meta_content": null, "right_content_heading": "Life Church Missions", "right_content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"yweau\\\\\\"> </p><p data-block-key=\\\\\\"6thbb\\\\\\">Life Church Missions organisation wanted to distribute videos of preachings, seminars and other classes via digital platform that their followers could then use to consume the content, comment and interact with other users on the platform.</p><p data-block-key=\\\\\\"bj2h8\\\\\\">The organisation had a large library of audio and video recordings compiled over the last few years that had to be organised, labelled and made available for online consumption. Since most of the files were distributed across various platforms such as YouTube, Vimeo, Youku etc, application had to have multiple integrations while preserving user experience</p><p data-block-key=\\\\\\"fu2g6\\\\\\">iOS and Android applications for end users to consume content. Robust content management platform for admins to publish content irrespective of whether they are hosted on a third party platform or on application server. Labelling and index preparation of client\\\\u2019s content including tags for optimised search</p>\\", \\"id\\": \\"cbbe548d-7038-4870-9756-a5a61ba5e0e0\\"}]", "timeline": "3 Months", "platform": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"4kzvg\\\\\\"> Android, iOS, Web (Python) </p>\\", \\"id\\": \\"2a12af0d-70e2-4d03-8362-5da1df6cdc2f\\"}]", "deliverables": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"mhtt7\\\\\\"> Web and mobile applications, custom image processing, storage management solutions </p>\\", \\"id\\": \\"00949bbd-5114-4601-901a-42e2c16c4ec5\\"}]", "section1_image": 126, "section1_content": "[{\\"type\\": \\"image\\", \\"value\\": 127, \\"id\\": \\"3fe247c9-b0a2-48c5-8a88-0acba55c2aeb\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"ruaoh\\\\\\"> </p><h3 data-block-key=\\\\\\"36i6j\\\\\\"><b>Custom Video Player</b></h3><p data-block-key=\\\\\\"349il\\\\\\">Custom video player that could continue with audio when screen is locked with additional functionalities such as playback speed control, resume capabilities etc.</p>\\", \\"id\\": \\"f79e189b-9858-4ca0-a80f-b236da5412e7\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"ruaoh\\\\\\"> </p><h3 data-block-key=\\\\\\"46go8\\\\\\"><b>Forum Commenting</b></h3><p data-block-key=\\\\\\"804lg\\\\\\">Users could chat on the platform and discuss on topics and video. This promoted community interaction and increased app run time.</p>\\", \\"id\\": \\"abc736bd-a782-41d2-86e0-cc074dd266c9\\"}, {\\"type\\": \\"image\\", \\"value\\": 128, \\"id\\": \\"001d77b0-e16a-4c92-9462-3842e1cde483\\"}]", "section2_image": 128, "section2_content": "[{\\"type\\": \\"image\\", \\"value\\": 129, \\"id\\": \\"bb6cf64c-627c-4e41-97c4-b5d63e7cccd7\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"tewoa\\\\\\"> </p><h3 data-block-key=\\\\\\"37mh7\\\\\\"><b>Donations &amp; Secured Payment Gateway</b></h3><p data-block-key=\\\\\\"2ethj\\\\\\">Application had payment gateway integration for accepting donations from users.</p>\\", \\"id\\": \\"2008fd99-c2bb-4a90-bcbf-0278279af283\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"tewoa\\\\\\"> </p><h3 data-block-key=\\\\\\"2qta9\\\\\\"><b>Offline Playback</b></h3><p data-block-key=\\\\\\"6v7e9\\\\\\">Users could download video and audio files for offline consumption. The files were encrypted and stored on local devices to avoid misuse.</p>\\", \\"id\\": \\"04a28a87-d375-474b-a021-a79723e20a3b\\"}, {\\"type\\": \\"image\\", \\"value\\": 130, \\"id\\": \\"0c1c45f5-ba71-4b14-be17-5fedbc954a80\\"}, {\\"type\\": \\"image\\", \\"value\\": 131, \\"id\\": \\"681952bc-ee05-48f8-9d99-bf4941dc272a\\"}, {\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"tewoa\\\\\\"> </p><h3 data-block-key=\\\\\\"c6240\\\\\\"><b>Integration to Third Party Service</b></h3><p data-block-key=\\\\\\"8bgmi\\\\\\">Application integrated with various video platforms such as YouTube, Vimeo &amp; Youku to provide admins and users with a seamless experience while publishing and consuming content</p>\\", \\"id\\": \\"f085c667-110d-409f-8437-32af2cbdaa7d\\"}]", "project_list_bg_image": 123, "project_list_logo": 124, "project_list_heading": "digital content distribution platform for a singapore based church", "project_list_title": "mobile application", "project_list_subtitle": "Public Organisation", "bg_color": 125, "nextpage_bg_image": 132, "nextpage_logo": null, "nextpage_heading": "bearteddy bean bags", "nextpage_title": "website / mobile app", "nextpage_subtitle": "Lifestyle", "nextpage_url": null, "comments": []}	\N	21	1
24	f	2021-09-29 17:55:55.558645+05:30	{"pk": 22, "path": "0001000100040003", "depth": 4, "numchild": 0, "translation_key": "1f449724-6b0a-4311-a9de-b75cddd47a8a", "locale": 1, "title": "new post 3", "draft_title": "new post 3", "slug": "new-post-3", "content_type": 47, "live": true, "has_unpublished_changes": false, "url_path": "/home/blogs/new-post-3/", "owner": 1, "seo_title": "", "show_in_menus": false, "search_description": "", "go_live_at": null, "expire_at": null, "expired": false, "locked": false, "locked_at": null, "locked_by": null, "first_published_at": null, "last_published_at": null, "latest_revision_created_at": null, "live_revision": null, "alias_of": null, "header_title": null, "meta_content": null, "blog_detail_image": 131, "blog_list_image": 126, "date": "2021-09-01", "read_time": "7 min read", "content": "[{\\"type\\": \\"full_richtext\\", \\"value\\": \\"<p data-block-key=\\\\\\"2t0p4\\\\\\">new post 3</p>\\", \\"id\\": \\"55601ad1-f160-46bc-9620-83ca20cb74d2\\"}]", "comments": []}	\N	22	1
\.


--
-- Data for Name: wagtailcore_pagesubscription; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pagesubscription (id, comment_notifications, page_id, user_id) FROM stdin;
1	f	3	1
2	t	4	1
3	t	5	1
4	t	6	1
6	t	8	1
5	t	7	1
7	t	9	1
8	t	10	1
10	t	12	1
9	t	11	1
11	t	13	1
12	t	14	1
13	t	15	1
14	t	16	1
15	t	17	1
16	t	18	1
17	t	19	1
18	t	20	1
19	t	21	1
20	t	22	1
\.


--
-- Data for Name: wagtailcore_pageviewrestriction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pageviewrestriction (id, password, page_id, restriction_type) FROM stdin;
\.


--
-- Data for Name: wagtailcore_pageviewrestriction_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_pageviewrestriction_groups (id, pageviewrestriction_id, group_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_site (id, hostname, port, is_default_site, root_page_id, site_name) FROM stdin;
2	localhost	80	t	3	
\.


--
-- Data for Name: wagtailcore_task; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_task (id, name, active, content_type_id) FROM stdin;
1	Moderators approval	t	4
\.


--
-- Data for Name: wagtailcore_taskstate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_taskstate (id, status, started_at, finished_at, content_type_id, page_revision_id, task_id, workflow_state_id, finished_by_id, comment) FROM stdin;
\.


--
-- Data for Name: wagtailcore_workflow; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_workflow (id, name, active) FROM stdin;
1	Moderators approval	t
\.


--
-- Data for Name: wagtailcore_workflowpage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_workflowpage (page_id, workflow_id) FROM stdin;
1	1
\.


--
-- Data for Name: wagtailcore_workflowstate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_workflowstate (id, status, created_at, current_task_state_id, page_id, requested_by_id, workflow_id) FROM stdin;
\.


--
-- Data for Name: wagtailcore_workflowtask; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailcore_workflowtask (id, sort_order, task_id, workflow_id) FROM stdin;
1	0	1	1
\.


--
-- Data for Name: wagtaildocs_document; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtaildocs_document (id, title, file, created_at, uploaded_by_user_id, collection_id, file_size, file_hash) FROM stdin;
\.


--
-- Data for Name: wagtaildocs_uploadeddocument; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtaildocs_uploadeddocument (id, file, uploaded_by_user_id) FROM stdin;
\.


--
-- Data for Name: wagtailembeds_embed; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailembeds_embed (id, url, max_width, type, html, title, author_name, provider_name, thumbnail_url, width, height, last_updated, hash, cache_until) FROM stdin;
\.


--
-- Data for Name: wagtailforms_formsubmission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailforms_formsubmission (id, form_data, submit_time, page_id) FROM stdin;
1	{"your_name": "archana", "your_company": "mdrift", "your_email_address": "rmanju@dgmail.com", "when_do_you_want_to_start": "20-09-2021", "what_is_your_budget": "2000", "describe_your_needs_the_more_we_know_the_better": "hhhhhhhh"}	2021-09-29 10:41:24.753263+05:30	6
2	{"your_name": "ranjitha", "your_company": "mdrift", "your_email_address": "rmanju@dgmail.com", "when_do_you_want_to_start": "20-09-2021", "what_is_your_budget": "10000", "describe_your_needs_the_more_we_know_the_better": "hi"}	2021-09-29 11:38:16.853577+05:30	6
3	{"your_name": "ranjitha", "your_company": "mdrift", "your_email_address": "rmanju@dgmail.com", "when_do_you_want_to_start": "20-09-2021", "what_is_your_budget": "10000", "describe_your_needs_the_more_we_know_the_better": "hii"}	2021-09-29 12:31:35.854758+05:30	6
4	{"your_name": "ranjitha", "your_company": "mdrift", "your_email_address": "rmanju@dgmail.com", "when_do_you_want_to_start": "20-09-2021", "what_is_your_budget": "10000", "describe_your_needs_the_more_we_know_the_better": "hi"}	2021-09-29 17:50:03.930957+05:30	6
\.


--
-- Data for Name: wagtailimages_image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailimages_image (id, title, file, width, height, created_at, focal_point_x, focal_point_y, focal_point_width, focal_point_height, uploaded_by_user_id, file_size, collection_id, file_hash) FROM stdin;
1	1.png	original_images/1.png	160	52	2021-09-29 10:24:21.196536+05:30	\N	\N	\N	\N	1	4846	1	64663c58a23abbd5dcdedbf47406a2e2d5763cd2
2	2.png	original_images/2.png	160	52	2021-09-29 10:24:31.506957+05:30	\N	\N	\N	\N	1	4381	1	0e125f5bc40fa6cba165dbee0d92a98e24a1c9d0
3	3.png	original_images/3.png	160	52	2021-09-29 10:24:41.822864+05:30	\N	\N	\N	\N	1	5949	1	227494683caecf0c54823d6f8dd6608569d3c1fc
4	4.png	original_images/4.png	160	52	2021-09-29 10:24:52.451157+05:30	\N	\N	\N	\N	1	3905	1	c0d896a2fc72a1f6d9be18011c1beb4ae9e358a4
5	5.png	original_images/5.png	160	52	2021-09-29 10:25:09.620216+05:30	\N	\N	\N	\N	1	3752	1	d1257b35c72a1cdfa033a4f8e7dec05ae7174d1d
6	6.png	original_images/6.png	160	52	2021-09-29 10:25:50.415647+05:30	\N	\N	\N	\N	1	4628	1	561f8b1ab19beede55aeadae8f69a66ff667e6b8
7	7.png	original_images/7.png	160	52	2021-09-29 10:26:03.76443+05:30	\N	\N	\N	\N	1	6427	1	7b86726669a355ee00a755d08b2549e40396485e
8	8.png	original_images/8.png	160	52	2021-09-29 10:26:14.557202+05:30	\N	\N	\N	\N	1	3425	1	eb9c6d8f62ae0889ab3646dd69187ef92461f7e7
9	9.png	original_images/9.png	160	52	2021-09-29 10:26:26.982294+05:30	\N	\N	\N	\N	1	6635	1	68b575a50bac8953ff9c564d4bd390c81a906847
10	blog1.jpeg	original_images/blog1.jpeg	1920	1080	2021-09-29 10:50:59.005381+05:30	\N	\N	\N	\N	1	231339	1	37cd867a9ba7ffd1a38154c2f4a9d511d1d8fd08
11	Strategy.png	original_images/Strategy.png	821	320	2021-09-29 10:51:54.822756+05:30	\N	\N	\N	\N	1	315072	1	ef0455866e2c538d2d74157bb3773081c4682400
12	sales-forecasting.png	original_images/sales-forecasting.png	821	320	2021-09-29 10:52:21.17646+05:30	\N	\N	\N	\N	1	232165	1	4af6034c0c4157304345c337f7957b022ac3e906
13	Improved-Advertsing.png	original_images/Improved-Advertsing.png	821	320	2021-09-29 10:52:36.259849+05:30	\N	\N	\N	\N	1	364022	1	d87dad1a756ab58b6cd93d0f5bc4c871b0e0ca72
14	Measure-everything.png	original_images/Measure-everything.png	821	320	2021-09-29 10:53:01.971595+05:30	\N	\N	\N	\N	1	372743	1	2539d74cad7cf7d590f81300c76b921006381442
15	optimize-marketing-strategies.png	original_images/optimize-marketing-strategies.png	821	320	2021-09-29 10:53:27.917387+05:30	\N	\N	\N	\N	1	365090	1	f6ed4a3ea1bcecd040a7023118926e8ac34915c3
16	Technical-ability-to-execute.png	original_images/Technical-ability-to-execute.png	821	320	2021-09-29 10:53:46.181128+05:30	\N	\N	\N	\N	1	403063	1	3abf596ed520f882955acfa8334827be517f858a
17	transafe-bg.png	original_images/transafe-bg.png	1230	533	2021-09-29 11:05:22.305929+05:30	\N	\N	\N	\N	1	302308	1	710dac04bced8b260ef145eb15bff8de54770a97
18	transafe-logo.png	original_images/transafe-logo.png	111	17	2021-09-29 11:05:58.414567+05:30	\N	\N	\N	\N	1	1021	1	06ea5f22772e9c62649fc73cc3bff0017acf6ef3
19	bg-transafe.jpg	original_images/bg-transafe.jpg	2880	1800	2021-09-29 11:07:33.00588+05:30	\N	\N	\N	\N	1	171887	1	a81be7ac42e6175fc82cb67cca7c38d3f856b096
20	parallax-bg.png	original_images/parallax-bg.png	1920	696	2021-09-29 11:09:02.270773+05:30	\N	\N	\N	\N	1	2202507	1	f079a725dfd5ccf009cafeb095b1509bd8de5b63
21	img1.png	original_images/img1.png	730	574	2021-09-29 11:09:32.839619+05:30	\N	\N	\N	\N	1	120205	1	b4680a7740ef4fd406a8a2f91a5a890d0473af85
22	img2.png	original_images/img2.png	730	574	2021-09-29 11:10:09.917638+05:30	\N	\N	\N	\N	1	94901	1	e20b8a83bf2fb1ec58e8b0f1a9bdf4aba938bb58
23	img3.png	original_images/img3.png	1080	497	2021-09-29 11:10:24.97135+05:30	\N	\N	\N	\N	1	92539	1	da19827b90a0b50789ca71aa1306f8a682b252d2
24	img4.png	original_images/img4.png	730	574	2021-09-29 11:10:48.992296+05:30	\N	\N	\N	\N	1	130881	1	a8c41d00aa0b1aaf4a38ebca73d233313fe2ff97
25	img5.png	original_images/img5.png	730	574	2021-09-29 11:11:23.452592+05:30	\N	\N	\N	\N	1	73951	1	69749400b1cf32c124abce26847664fd5a58596c
26	img6.png	original_images/img6.png	730	574	2021-09-29 11:11:44.188238+05:30	\N	\N	\N	\N	1	205944	1	bce124363b31e77c876ceded89474592e9ab7d72
27	isp-bg.png	original_images/isp-bg.png	1230	533	2021-09-29 11:12:21.760295+05:30	\N	\N	\N	\N	1	184291	1	d70ba45c8a4d613a093adb6eaee2a53d98414a86
28	isp-logo.png	original_images/isp-logo.png	108	15	2021-09-29 11:12:32.083009+05:30	\N	\N	\N	\N	1	847	1	26657f71e666d20da2ef7bba7b332b4a41be1493
29	bg-isp.jpg	original_images/bg-isp.jpg	2880	1800	2021-09-29 11:50:26.296634+05:30	\N	\N	\N	\N	1	165582	1	2555b351f0a4bb8d39d42814c851f1822d7efc1b
30	parallax-bg.png	original_images/parallax-bg_e1A2IqZ.png	1940	699	2021-09-29 11:51:12.011157+05:30	\N	\N	\N	\N	1	891072	1	c7efd4831a862ff8c44ec7e39cc7e766eb6baa1c
31	img1.png	original_images/img1_8CIgpqu.png	730	574	2021-09-29 11:51:25.761902+05:30	\N	\N	\N	\N	1	54535	1	1718cf8e335229cd25473d4a32904b27395c2c4a
32	img2.png	original_images/img2_DiLzl8h.png	730	574	2021-09-29 11:51:56.292338+05:30	\N	\N	\N	\N	1	83039	1	d32e32be8fd1d68f195c071e6323b68e992bf6c1
33	img3.png	original_images/img3_58YYlI7.png	1049	488	2021-09-29 11:52:08.484449+05:30	\N	\N	\N	\N	1	169244	1	0f245784e9aa263178131a9a92124d8ac3ff0e40
34	img4.png	original_images/img4_BbnraY8.png	730	574	2021-09-29 11:52:29.46468+05:30	\N	\N	\N	\N	1	123003	1	ceb50f62bfb645e98c77901b651e80a4b40dc944
35	img5.png	original_images/img5_8h86qz6.png	730	574	2021-09-29 11:52:38.286288+05:30	\N	\N	\N	\N	1	59790	1	a83e882f2cebb9b290b92644a4c016192caf6a32
36	img6.png	original_images/img6_lVbZrjs.png	730	574	2021-09-29 11:53:06.586231+05:30	\N	\N	\N	\N	1	88790	1	554730295915dc09996ca979b483ef6e11969f84
37	img7.png	original_images/img7.png	730	574	2021-09-29 11:53:16.258313+05:30	\N	\N	\N	\N	1	106861	1	22cf0f4ce8e980bd49724732bb82c004bc1cda1e
38	img8.png	original_images/img8.png	730	574	2021-09-29 11:53:54.015643+05:30	\N	\N	\N	\N	1	163350	1	9b766246f3817ee968e349fa57771e8a5f3dee11
39	8sidor-bg.png	original_images/8sidor-bg.png	1230	533	2021-09-29 11:54:19.413275+05:30	\N	\N	\N	\N	1	113005	1	f10893566feaace53e9300514ca6504115202f23
40	8sidor-logo.png	original_images/8sidor-logo.png	139	25	2021-09-29 11:54:33.370494+05:30	\N	\N	\N	\N	1	2701	1	36c18f7952abc49cc4a66f5a8adc96282278c812
41	sidor-bg-peach.jpg	original_images/sidor-bg-peach.jpg	2880	1800	2021-09-29 12:00:28.874127+05:30	\N	\N	\N	\N	1	172372	1	77ffbb427deb22b942b377b24552b593efdc21a5
42	parallax-bg.png	original_images/parallax-bg_d7qPkzM.png	1920	698	2021-09-29 12:01:11.780463+05:30	\N	\N	\N	\N	1	2091541	1	cc8ad17922fa355dd3fc70e21df53261f3d2bccf
43	img1.png	original_images/img1_OuAP0Ii.png	730	574	2021-09-29 12:01:26.503011+05:30	\N	\N	\N	\N	1	240173	1	442d166f666c1a6f0c85e08df678f0461718c189
44	img2.png	original_images/img2_qmG7f8k.png	730	574	2021-09-29 12:01:49.449959+05:30	\N	\N	\N	\N	1	75613	1	b640e89a57a205660fcd4c8cca0397fd089d564c
45	img3.png	original_images/img3_Zzx8SJa.png	977	506	2021-09-29 12:01:55.998402+05:30	\N	\N	\N	\N	1	327517	1	4f0ed111b611b35e5033a1443cc9773ac9a5e3c0
46	img4.png	original_images/img4_UeuWND6.png	730	574	2021-09-29 12:02:14.156519+05:30	\N	\N	\N	\N	1	149208	1	97ae9ffd7268421954d08ea3344859095bbd5a38
47	img5.png	original_images/img5_RM85PxI.png	730	574	2021-09-29 12:02:25.157681+05:30	\N	\N	\N	\N	1	241606	1	36311de8fa7b82ced41150f94f47a95ab5e5404c
48	propellerhead-bg.png	original_images/propellerhead-bg.png	1230	533	2021-09-29 12:02:53.074204+05:30	\N	\N	\N	\N	1	172142	1	a9394e7e27c110e40b2ec5c1d035065abbf6ac7e
49	propellerhead-logo.png	original_images/propellerhead-logo.png	151	21	2021-09-29 12:03:02.221136+05:30	\N	\N	\N	\N	1	2448	1	5e674bbd32d23a1ce514a2d6848645fb28e3d1b7
50	bg-propellerhead.jpg	original_images/bg-propellerhead.jpg	2880	1800	2021-09-29 12:08:04.505373+05:30	\N	\N	\N	\N	1	165623	1	ec2756f5ee6f31bbf657fdf6afb12b68dda05649
51	parallax-bg.jpg	original_images/parallax-bg.jpg	1200	794	2021-09-29 12:09:02.68191+05:30	\N	\N	\N	\N	1	224405	1	4b006fb759ad87c152e6af4d93c605e9a5855eff
52	img1.png	original_images/img1_ESHxnW7.png	730	574	2021-09-29 12:09:21.832114+05:30	\N	\N	\N	\N	1	163490	1	461d712f5ff63db7ad3e8f712931ec7ba5a8f7f1
53	img2.png	original_images/img2_pGzk1bp.png	730	574	2021-09-29 12:09:46.398722+05:30	\N	\N	\N	\N	1	137181	1	70efedbb2292fb2b9f10a8aa4f1d4eb84d48b3dd
54	img3.png	original_images/img3_B7nchjt.png	730	574	2021-09-29 12:09:56.710213+05:30	\N	\N	\N	\N	1	366722	1	9e87f126ada2056313c21bbf7de42f3431af5e8a
55	autoscroll.png	original_images/autoscroll.png	1366	3525	2021-09-29 12:10:11.11436+05:30	\N	\N	\N	\N	1	3481276	1	4926888054a70027bcc6befdf8d7ec5adcffd3d5
56	img4.png	original_images/img4_unCidCu.png	730	574	2021-09-29 12:10:31.708815+05:30	\N	\N	\N	\N	1	240801	1	603dbdb6af68e164ec2c81879c3e5a1a8d60460c
57	img5.png	original_images/img5_hnQzqLU.png	730	574	2021-09-29 12:11:16.679469+05:30	\N	\N	\N	\N	1	284333	1	fb9d2d40f99176cbd694b1e0f1b1329db1584a6d
58	img1.png	original_images/img1_svzPaJh.png	730	574	2021-09-29 12:11:31.265327+05:30	\N	\N	\N	\N	1	163490	1	461d712f5ff63db7ad3e8f712931ec7ba5a8f7f1
59	rocxia-bg.png	original_images/rocxia-bg.png	1230	533	2021-09-29 12:12:01.765364+05:30	\N	\N	\N	\N	1	206872	1	83936fc7f330e78e3d5ff41cdb1d56d53c120c1d
60	rocxia-logo.png	original_images/rocxia-logo.png	103	20	2021-09-29 12:12:16.926921+05:30	\N	\N	\N	\N	1	1464	1	1614eb8b06db1251706afa8fc41762e78b094081
61	bg-rocxia.jpg	original_images/bg-rocxia.jpg	2880	1800	2021-09-29 12:16:02.267632+05:30	\N	\N	\N	\N	1	171936	1	37664cca906a06d0618e76145da48f6a92656b33
62	parallax-bg.png	original_images/parallax-bg_s2zphS9.png	1920	699	2021-09-29 12:16:54.365687+05:30	\N	\N	\N	\N	1	1367949	1	5c3d64440b4a426ea050307d9c0f5a53e3602a23
63	img1.png	original_images/img1_2kcQ9Y9.png	730	574	2021-09-29 12:35:09.619471+05:30	\N	\N	\N	\N	1	195220	1	4354ca7538fbe60081ab4ac0d806850ccb5bf338
64	img2.png	original_images/img2_uMiX1o6.png	730	574	2021-09-29 12:36:06.929124+05:30	\N	\N	\N	\N	1	241915	1	784aef13f05e0392cd87ca54441deb1e87bd1351
65	img3.png	original_images/img3_OT3dUiF.png	986	556	2021-09-29 12:36:17.318359+05:30	\N	\N	\N	\N	1	840764	1	e8d043315cf2cf136560721fb9c6e0aa25ae36e5
66	img4.png	original_images/img4_8W2e8BQ.png	730	574	2021-09-29 12:36:56.690651+05:30	\N	\N	\N	\N	1	459391	1	458640ca9dac59c8525b6ba799213c156e36c94a
67	img5.png	original_images/img5_UCA0lHe.png	730	574	2021-09-29 12:37:32.312151+05:30	\N	\N	\N	\N	1	284309	1	f1676f15baf71e4b0f4c594a190311ccae6d88f6
68	img6.png	original_images/img6_xwYjIAZ.png	730	574	2021-09-29 12:38:10.520557+05:30	\N	\N	\N	\N	1	116225	1	07b8190a4ba32287a222da9dfd84259c7585d8bf
69	img7.png	original_images/img7_ks1zC2Y.png	730	574	2021-09-29 12:38:27.717442+05:30	\N	\N	\N	\N	1	160611	1	d976045b27ddd0e4e5746d5cc3e7accc597935ad
70	img8.png	original_images/img8_iCqNdaB.png	730	574	2021-09-29 12:38:56.921271+05:30	\N	\N	\N	\N	1	139420	1	1929da1aa38c822cfdcec3a38e2ea435f50d2326
71	img9.png	original_images/img9.png	730	574	2021-09-29 12:39:06.908347+05:30	\N	\N	\N	\N	1	174128	1	4ffe71f00eb4b7584da240d890713d1b804c9e48
72	nyborhoods-bg.png	original_images/nyborhoods-bg.png	1230	533	2021-09-29 12:39:29.595287+05:30	\N	\N	\N	\N	1	136126	1	77d1eec27938b30cf9706e1a5213df1899264547
73	nyborhoods-logo.png	original_images/nyborhoods-logo.png	148	60	2021-09-29 12:39:45.572451+05:30	\N	\N	\N	\N	1	9538	1	9bd95129322287dfdc95a8b54c61bfd061ac4a14
74	nyborhoods-bg.png	original_images/nyborhoods-bg_KuTwhI5.png	1230	533	2021-09-29 12:42:56.99278+05:30	\N	\N	\N	\N	1	136126	1	77d1eec27938b30cf9706e1a5213df1899264547
75	bg-nyborhood.jpg	original_images/bg-nyborhood.jpg	2880	1800	2021-09-29 12:44:45.853191+05:30	\N	\N	\N	\N	1	165594	1	a1b098c19063672e6463360d148dc3c13f043a18
76	parallax-bg.png	original_images/parallax-bg_0xZdCxL.png	1920	699	2021-09-29 12:45:09.53424+05:30	\N	\N	\N	\N	1	3154665	1	95e8de9b3ca62b8cd761703aaefe6b5aaedf1c46
77	img1.png	original_images/img1_iVtC2dP.png	730	574	2021-09-29 12:45:30.596528+05:30	\N	\N	\N	\N	1	190206	1	1843b554146b92d4eaa05e5d9ee40cc3ddb2ac07
78	img2.png	original_images/img2_0e2Mv6S.png	730	574	2021-09-29 12:45:58.80379+05:30	\N	\N	\N	\N	1	204160	1	28407b542f9a67867fde47500d5015e547319aec
79	img3.png	original_images/img3_FCXhxAX.png	821	584	2021-09-29 12:46:11.257725+05:30	\N	\N	\N	\N	1	181152	1	64e3d849b343df46e5545018976d4d7fa96e2cc9
80	img4.png	original_images/img4_WnqqJKF.png	730	574	2021-09-29 12:46:24.233764+05:30	\N	\N	\N	\N	1	238584	1	0d5b1e6b8250f2016b1425579184340ce89dbbf4
81	img5.png	original_images/img5_lnDb8Wg.png	730	574	2021-09-29 12:46:50.738035+05:30	\N	\N	\N	\N	1	104481	1	74612f3f11cbb5442712b6194e19513a8f2f2c04
82	transafe-bg.png	original_images/transafe-bg_3kQKQSs.png	1230	533	2021-09-29 12:47:15.657767+05:30	\N	\N	\N	\N	1	302308	1	710dac04bced8b260ef145eb15bff8de54770a97
83	transafe-logo.png	original_images/transafe-logo_63Yo70z.png	111	17	2021-09-29 12:47:27.531042+05:30	\N	\N	\N	\N	1	1021	1	06ea5f22772e9c62649fc73cc3bff0017acf6ef3
84	ziggy-bg.png	original_images/ziggy-bg.png	1230	533	2021-09-29 12:50:06.534671+05:30	\N	\N	\N	\N	1	232498	1	638abe9b12556834e5f79d7dae2aa246c743328b
85	ziggy-logo.png	original_images/ziggy-logo.png	108	41	2021-09-29 12:50:25.187099+05:30	\N	\N	\N	\N	1	4469	1	00430464b1fce0d110b7dfdcf3d2aacf109a78e0
86	bg-ziggy.jpg	original_images/bg-ziggy.jpg	2880	1800	2021-09-29 12:51:22.800818+05:30	\N	\N	\N	\N	1	165607	1	8752f9c8d5812a62bbab1e88889dd1190af8fd54
87	img1.png	original_images/img1_3Cr8P5Q.png	1200	794	2021-09-29 12:53:03.383065+05:30	\N	\N	\N	\N	1	2585879	1	dbf518506999196d701ad8d1a88050825641c74b
88	img2.png	original_images/img2_1G0JXQw.png	730	480	2021-09-29 12:53:25.093715+05:30	\N	\N	\N	\N	1	398282	1	e91382c81278c1760d3a6b0666de25bc9c2fa056
89	img3.png	original_images/img3_OU4RtFr.png	730	480	2021-09-29 12:53:49.079957+05:30	\N	\N	\N	\N	1	308760	1	c9ab6f921ccd766f18c3cd1d9a21293cc28ccfdb
90	ziggy.jpg	original_images/ziggy.jpg	5831	722	2021-09-29 12:54:12.964851+05:30	\N	\N	\N	\N	1	1653019	1	748c42f4e45511773e593168966f9e56a5fa997b
91	img4.png	original_images/img4_Ubb9HXa.png	730	480	2021-09-29 12:54:29.102907+05:30	\N	\N	\N	\N	1	208275	1	6b4065a0e7bb2f641411b2e7bfb0e89db61da4a3
92	img5.png	original_images/img5_t0LJhOi.png	730	480	2021-09-29 12:54:56.836981+05:30	\N	\N	\N	\N	1	34486	1	1530cfe1fa26e3e4305cc4f23daa613ff66299fc
93	qaid-bg.png	original_images/qaid-bg.png	1230	533	2021-09-29 12:55:23.483996+05:30	\N	\N	\N	\N	1	207515	1	84e77e6f5ef294c1304a387b1b087521a00c61f2
94	qaid-logo.png	original_images/qaid-logo.png	134	32	2021-09-29 12:55:36.435036+05:30	\N	\N	\N	\N	1	2873	1	897a71f333f4b46842fc8a401a91d29a9ccc7b18
95	bg-qaid.jpg	original_images/bg-qaid.jpg	2880	1800	2021-09-29 12:59:20.43097+05:30	\N	\N	\N	\N	1	165605	1	dd696866352ccf26c5466923908c26a4a190d78f
96	img1.jpg	original_images/img1.jpg	1200	794	2021-09-29 13:00:10.706556+05:30	\N	\N	\N	\N	1	319682	1	307812d2c463d13e3d8d421b4a5b93858a1cf8c6
97	img2.png	original_images/img2_TrW2PV4.png	730	480	2021-09-29 13:00:22.009155+05:30	\N	\N	\N	\N	1	63696	1	94a35dc31223b6b9436413e8cc86a1119e88fe7d
98	img3.png	original_images/img3_DWwETW0.png	730	480	2021-09-29 13:00:45.166987+05:30	\N	\N	\N	\N	1	90962	1	4ef94369bec4c3f5deca8b98526f899ed391c396
99	img4.png	original_images/img4_UsE7U85.png	965	728	2021-09-29 13:00:55.100516+05:30	\N	\N	\N	\N	1	187001	1	037d9eb6cf1eb739d24cb8bb6702737aded5025f
100	img5.png	original_images/img5_2Nk9bul.png	730	480	2021-09-29 13:01:07.476821+05:30	\N	\N	\N	\N	1	112283	1	d3d53799ad56924e1a32b3b0a60d476dbe07b641
101	img6.png	original_images/img6_7tRuzb8.png	730	480	2021-09-29 13:01:29.607071+05:30	\N	\N	\N	\N	1	57774	1	a7a327077f379f3d9b5a6f3edf52d351b323affd
102	rapiddev-bg.png	original_images/rapiddev-bg.png	1230	533	2021-09-29 13:01:50.026891+05:30	\N	\N	\N	\N	1	291315	1	45012b3ab9cfdd09dc5f925f39125f1c91e77c68
103	rapiddev-logo.png	original_images/rapiddev-logo.png	91	14	2021-09-29 13:02:02.194418+05:30	\N	\N	\N	\N	1	794	1	97b9568b6ed5197fcd9cd3fb28f9c511d147ce24
104	rapiddev-bg.png	original_images/rapiddev-bg_IJ8kkoc.png	1230	533	2021-09-29 13:04:26.417478+05:30	\N	\N	\N	\N	1	291315	1	45012b3ab9cfdd09dc5f925f39125f1c91e77c68
105	bg-rapiddev.jpg	original_images/bg-rapiddev.jpg	2880	1800	2021-09-29 13:05:09.925277+05:30	\N	\N	\N	\N	1	171937	1	353d412f03156145eaacc7b7791180bd636f6748
106	img1.png	original_images/img1_FGdnH2E.png	1200	794	2021-09-29 13:05:52.30531+05:30	\N	\N	\N	\N	1	1312906	1	4df6e2a7f9121fc691eee4b21cae0d0d3d047aef
107	img2.png	original_images/img2_52GKEkp.png	730	480	2021-09-29 13:06:06.399421+05:30	\N	\N	\N	\N	1	67685	1	bac067e013d6812ba044362d1f15aed0dc011755
108	img3.png	original_images/img3_OF7tKH6.png	730	480	2021-09-29 13:06:50.628237+05:30	\N	\N	\N	\N	1	102612	1	620faf9e0e018bb13384a25d2c558de318e906bd
109	img4.png	original_images/img4_T57SXcP.png	965	728	2021-09-29 13:07:08.892126+05:30	\N	\N	\N	\N	1	103715	1	893512245c28ae3d7b00744b3930a38d89b3b2bf
110	img5.png	original_images/img5_wzYToPp.png	730	480	2021-09-29 13:07:23.046417+05:30	\N	\N	\N	\N	1	83061	1	ad6bdf35c093640f17043f2b830c30456e31ba5e
111	img6.png	original_images/img6_4Ka5tIJ.png	730	480	2021-09-29 13:07:47.94838+05:30	\N	\N	\N	\N	1	63204	1	3da7181c48f47ca076805439f4698a07fe3bf524
112	khoon-bg.png	original_images/khoon-bg.png	1230	533	2021-09-29 13:08:05.575359+05:30	\N	\N	\N	\N	1	142058	1	542b5b0a68cff9cbc50abc32963d45ebecae4978
113	khoon-logo.png	original_images/khoon-logo.png	223	31	2021-09-29 13:08:19.319164+05:30	\N	\N	\N	\N	1	2423	1	1228155551537273be8003ecf0e6bea2b90b0717
114	bg-khoon.jpg	original_images/bg-khoon.jpg	2880	1800	2021-09-29 13:14:04.054535+05:30	\N	\N	\N	\N	1	165607	1	9ec22300c45e9642ad363b2c961629fe422ea244
115	img1.png	original_images/img1_lepkFK1.png	1200	794	2021-09-29 13:15:07.485284+05:30	\N	\N	\N	\N	1	1063055	1	f21941aa41039ca3cb1545131c669574b1da3f11
116	img2.png	original_images/img2_oOkejXi.png	730	480	2021-09-29 13:15:21.433823+05:30	\N	\N	\N	\N	1	199628	1	40c26a124b8abcde8d138d2680c49265ac192140
117	img3.png	original_images/img3_NM779zE.png	730	480	2021-09-29 13:15:56.067549+05:30	\N	\N	\N	\N	1	311057	1	5a6d29cd475b14437e0dfd1c3da230c7df8b30ed
118	img4.png	original_images/img4_QibtV3D.png	965	728	2021-09-29 13:16:22.015915+05:30	\N	\N	\N	\N	1	615093	1	32b5f42665b8fde325dd410f66064f64d319b224
119	img5.png	original_images/img5_oCQLSbX.png	730	480	2021-09-29 13:16:47.94001+05:30	\N	\N	\N	\N	1	180781	1	1f03db8cd123ac8374fe2eaa8b0f2a2383b67850
120	img6.png	original_images/img6_JzPq0Vr.png	730	480	2021-09-29 13:17:33.580317+05:30	\N	\N	\N	\N	1	21363	1	f7963b63c8a68acac2d9dfd262cfe206a15b9548
121	twitch-bg.png	original_images/twitch-bg.png	1230	533	2021-09-29 13:18:03.340593+05:30	\N	\N	\N	\N	1	255415	1	8e74b43b074123ab856e47f7152def5666922c24
122	twitch-logo.png	original_images/twitch-logo.png	133	58	2021-09-29 13:18:18.302247+05:30	\N	\N	\N	\N	1	4645	1	88fc00facd0621548de71322416ae7bc2a1fd103
123	lcm-bg.png	original_images/lcm-bg.png	1230	533	2021-09-29 13:26:59.098015+05:30	\N	\N	\N	\N	1	161156	1	42b78e72a5a7c4a2750733aea2c01bb9d9a585c1
124	lcm-logo.png	original_images/lcm-logo.png	227	39	2021-09-29 13:27:08.17401+05:30	\N	\N	\N	\N	1	4877	1	740a651a5ab9c7b715db0cdc395e1a9030b76f7f
125	bg-lcm.jpg	original_images/bg-lcm.jpg	2880	1800	2021-09-29 13:27:52.026755+05:30	\N	\N	\N	\N	1	165603	1	3e9caee99d1b1869cc8873c5158af7abbb8f649c
126	img1.png	original_images/img1_lAllJkW.png	1200	794	2021-09-29 13:28:34.489863+05:30	\N	\N	\N	\N	1	1201338	1	c894c5df204da0ed7b5cf7e508e1be38128fe0d8
127	img2.png	original_images/img2_a95NUyI.png	730	480	2021-09-29 13:28:45.556705+05:30	\N	\N	\N	\N	1	122225	1	6a3af6ecb1000510fae47a3fe64cdc3dde881466
128	img3.png	original_images/img3_rXgvrvE.png	730	480	2021-09-29 13:29:08.362771+05:30	\N	\N	\N	\N	1	67227	1	dcc63eda37f9c9730950763b6bb75389575426d6
129	img5.png	original_images/img5_PQHE3nk.png	730	480	2021-09-29 13:29:35.642694+05:30	\N	\N	\N	\N	1	187003	1	b37fd221ccb33f47bd65352bdf0c588d12b0ac4b
130	img6.png	original_images/img6_BlXTtkb.png	730	480	2021-09-29 13:30:07.596216+05:30	\N	\N	\N	\N	1	75951	1	c19b7bd15745114b768f3b928139d3257a392983
131	img7.png	original_images/img7_e88z1bV.png	730	480	2021-09-29 13:30:19.751872+05:30	\N	\N	\N	\N	1	271727	1	2f7fb9844b2e81e100aded9ec2b40c2893b1658b
132	isp-bg.png	original_images/isp-bg_BvEBIj8.png	1230	533	2021-09-29 13:30:45.81348+05:30	\N	\N	\N	\N	1	184291	1	d70ba45c8a4d613a093adb6eaee2a53d98414a86
\.


--
-- Data for Name: wagtailimages_rendition; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailimages_rendition (id, file, width, height, focal_point_key, filter_spec, image_id) FROM stdin;
1	images/1.max-165x165.png	160	52		max-165x165	1
2	images/2.max-165x165.png	160	52		max-165x165	2
3	images/3.max-165x165.png	160	52		max-165x165	3
4	images/4.max-165x165.png	160	52		max-165x165	4
5	images/5.max-165x165.png	160	52		max-165x165	5
6	images/6.max-165x165.png	160	52		max-165x165	6
7	images/7.max-165x165.png	160	52		max-165x165	7
8	images/8.max-165x165.png	160	52		max-165x165	8
9	images/9.max-165x165.png	160	52		max-165x165	9
10	images/1.width-530.png	160	52		width-530	1
11	images/2.width-530.png	160	52		width-530	2
12	images/3.width-530.png	160	52		width-530	3
13	images/4.width-530.png	160	52		width-530	4
14	images/5.width-530.png	160	52		width-530	5
15	images/6.width-530.png	160	52		width-530	6
16	images/7.width-530.png	160	52		width-530	7
17	images/8.width-530.png	160	52		width-530	8
18	images/9.width-530.png	160	52		width-530	9
19	images/blog1.max-165x165.jpg	165	92		max-165x165	10
20	images/Strategy.max-800x600.png	800	311		max-800x600	11
21	images/Strategy.width-500.png	500	194		width-500	11
22	images/Strategy.max-165x165.png	165	64		max-165x165	11
23	images/sales-forecasting.max-800x600.png	800	311		max-800x600	12
24	images/sales-forecasting.width-500.png	500	194		width-500	12
25	images/sales-forecasting.max-165x165.png	165	64		max-165x165	12
26	images/Improved-Advertsing.max-800x600.png	800	311		max-800x600	13
27	images/Improved-Advertsing.width-500.png	500	194		width-500	13
28	images/Improved-Advertsing.max-165x165.png	165	64		max-165x165	13
29	images/Measure-everything.max-800x600.png	800	311		max-800x600	14
30	images/Measure-everything.width-500.png	500	194		width-500	14
31	images/Measure-everything.max-165x165.png	165	64		max-165x165	14
32	images/optimize-marketing-strategies.max-800x600.png	800	311		max-800x600	15
33	images/optimize-marketing-strategies.width-500.png	500	194		width-500	15
34	images/optimize-marketing-strategies.max-165x165.png	165	64		max-165x165	15
35	images/Technical-ability-to-execute.max-800x600.png	800	311		max-800x600	16
36	images/Technical-ability-to-execute.width-500.png	500	194		width-500	16
37	images/blog1.2e16d0ba.fill-320x180.jpg	320	180	2e16d0ba	fill-320x180	10
38	images/blog1.2e16d0ba.fill-800x400.jpg	800	400	2e16d0ba	fill-800x400	10
39	images/Technical-ability-to-execute.max-165x165.png	165	64		max-165x165	16
40	images/optimize-marketing-strategies.2e16d0ba.fill-320x180.png	320	180	2e16d0ba	fill-320x180	15
41	images/Technical-ability-to-execute.2e16d0ba.fill-800x400.png	641	320	2e16d0ba	fill-800x400	16
42	images/transafe-bg.max-165x165.png	165	71		max-165x165	17
43	images/transafe-logo.max-165x165.png	111	17		max-165x165	18
44	images/bg-transafe.max-165x165.jpg	165	103		max-165x165	19
45	images/parallax-bg.max-165x165.png	165	59		max-165x165	20
46	images/img1.max-165x165.png	165	129		max-165x165	21
47	images/img2.max-165x165.png	165	129		max-165x165	22
48	images/img3.max-165x165.png	165	75		max-165x165	23
49	images/img4.max-165x165.png	165	129		max-165x165	24
50	images/img5.max-165x165.png	165	129		max-165x165	25
51	images/img6.max-165x165.png	165	129		max-165x165	26
52	images/isp-bg.max-165x165.png	165	71		max-165x165	27
53	images/isp-logo.max-165x165.png	108	15		max-165x165	28
54	images/transafe-bg.original.png	1230	533		original	17
55	images/transafe-logo.original.png	111	17		original	18
56	images/bg-transafe.original.jpg	2880	1800		original	19
57	images/parallax-bg.original.png	1920	696		original	20
58	images/img1.width-530.png	530	416		width-530	21
59	images/img2.width-530.png	530	416		width-530	22
60	images/img3.2e16d0ba.fill-800x400.png	800	400	2e16d0ba	fill-800x400	23
61	images/img4.width-530.png	530	416		width-530	24
62	images/img5.width-530.png	530	416		width-530	25
63	images/img6.width-530.png	530	416		width-530	26
64	images/isp-bg.original.png	1230	533		original	27
65	images/isp-logo.original.png	108	15		original	28
66	images/bg-isp.max-165x165.jpg	165	103		max-165x165	29
67	images/parallax-bg_e1A2IqZ.max-165x165.png	165	59		max-165x165	30
68	images/img1_8CIgpqu.max-165x165.png	165	129		max-165x165	31
69	images/img2_DiLzl8h.max-165x165.png	165	129		max-165x165	32
70	images/img3_58YYlI7.max-165x165.png	165	76		max-165x165	33
71	images/img4_BbnraY8.max-165x165.png	165	129		max-165x165	34
72	images/img5_8h86qz6.max-165x165.png	165	129		max-165x165	35
73	images/img6_lVbZrjs.max-165x165.png	165	129		max-165x165	36
74	images/img7.max-165x165.png	165	129		max-165x165	37
75	images/img8.max-165x165.png	165	129		max-165x165	38
76	images/8sidor-bg.max-165x165.png	165	71		max-165x165	39
77	images/8sidor-logo.max-165x165.png	139	25		max-165x165	40
78	images/bg-isp.original.jpg	2880	1800		original	29
79	images/parallax-bg_e1A2IqZ.original.png	1940	699		original	30
80	images/img1_8CIgpqu.width-530.png	530	416		width-530	31
81	images/img2_DiLzl8h.width-530.png	530	416		width-530	32
82	images/img3_58YYlI7.2e16d0ba.fill-800x400.png	800	400	2e16d0ba	fill-800x400	33
83	images/img4_BbnraY8.width-530.png	530	416		width-530	34
84	images/img5_8h86qz6.width-530.png	530	416		width-530	35
85	images/img6_lVbZrjs.width-530.png	530	416		width-530	36
86	images/img7.width-530.png	530	416		width-530	37
87	images/img8.width-530.png	530	416		width-530	38
88	images/8sidor-bg.original.png	1230	533		original	39
89	images/8sidor-logo.original.png	139	25		original	40
90	images/sidor-bg-peach.max-165x165.jpg	165	103		max-165x165	41
91	images/parallax-bg_d7qPkzM.max-165x165.png	165	59		max-165x165	42
92	images/img1_OuAP0Ii.max-165x165.png	165	129		max-165x165	43
93	images/img2_qmG7f8k.max-165x165.png	165	129		max-165x165	44
94	images/img3_Zzx8SJa.max-165x165.png	165	85		max-165x165	45
95	images/img4_UeuWND6.max-165x165.png	165	129		max-165x165	46
96	images/img5_RM85PxI.max-165x165.png	165	129		max-165x165	47
97	images/propellerhead-bg.max-165x165.png	165	71		max-165x165	48
98	images/propellerhead-logo.max-165x165.png	151	21		max-165x165	49
99	images/sidor-bg-peach.original.jpg	2880	1800		original	41
100	images/parallax-bg_d7qPkzM.original.png	1920	698		original	42
101	images/img1_OuAP0Ii.width-530.png	530	416		width-530	43
102	images/img2_qmG7f8k.width-530.png	530	416		width-530	44
103	images/img3_Zzx8SJa.2e16d0ba.fill-800x400.png	800	400	2e16d0ba	fill-800x400	45
104	images/img4_UeuWND6.width-530.png	530	416		width-530	46
105	images/img5_RM85PxI.width-530.png	530	416		width-530	47
106	images/propellerhead-bg.original.png	1230	533		original	48
107	images/propellerhead-logo.original.png	151	21		original	49
108	images/bg-propellerhead.max-165x165.jpg	165	103		max-165x165	50
109	images/parallax-bg.max-165x165.jpg	165	109		max-165x165	51
110	images/img1_ESHxnW7.max-165x165.png	165	129		max-165x165	52
111	images/img2_pGzk1bp.max-165x165.png	165	129		max-165x165	53
112	images/img3_B7nchjt.max-165x165.png	165	129		max-165x165	54
113	images/autoscroll.max-165x165.png	63	165		max-165x165	55
114	images/img4_unCidCu.max-165x165.png	165	129		max-165x165	56
115	images/img5_hnQzqLU.max-165x165.png	165	129		max-165x165	57
116	images/img1_svzPaJh.max-165x165.png	165	129		max-165x165	58
117	images/rocxia-bg.max-165x165.png	165	71		max-165x165	59
118	images/rocxia-logo.max-165x165.png	103	20		max-165x165	60
119	images/bg-propellerhead.original.jpg	2880	1800		original	50
120	images/parallax-bg.original.jpg	1200	794		original	51
121	images/img1_ESHxnW7.width-530.png	530	416		width-530	52
122	images/img2_pGzk1bp.width-530.png	530	416		width-530	53
123	images/autoscroll.2e16d0ba.fill-800x400.png	800	400	2e16d0ba	fill-800x400	55
124	images/img4_unCidCu.width-530.png	530	416		width-530	56
125	images/img5_hnQzqLU.width-530.png	530	416		width-530	57
126	images/img1_svzPaJh.width-530.png	530	416		width-530	58
127	images/rocxia-bg.original.png	1230	533		original	59
128	images/rocxia-logo.original.png	103	20		original	60
129	images/bg-rocxia.max-165x165.jpg	165	103		max-165x165	61
130	images/parallax-bg_s2zphS9.max-165x165.png	165	60		max-165x165	62
131	images/img1_2kcQ9Y9.max-165x165.png	165	129		max-165x165	63
132	images/img2_uMiX1o6.max-165x165.png	165	129		max-165x165	64
133	images/img3_OT3dUiF.max-165x165.png	165	93		max-165x165	65
134	images/img4_8W2e8BQ.max-165x165.png	165	129		max-165x165	66
135	images/img5_UCA0lHe.max-165x165.png	165	129		max-165x165	67
136	images/img6_xwYjIAZ.max-165x165.png	165	129		max-165x165	68
137	images/img7_ks1zC2Y.max-165x165.png	165	129		max-165x165	69
138	images/img8_iCqNdaB.max-165x165.png	165	129		max-165x165	70
139	images/img9.max-165x165.png	165	129		max-165x165	71
140	images/nyborhoods-bg.max-165x165.png	165	71		max-165x165	72
141	images/nyborhoods-logo.max-165x165.png	148	60		max-165x165	73
142	images/bg-rocxia.original.jpg	2880	1800		original	61
143	images/parallax-bg_s2zphS9.original.png	1920	699		original	62
144	images/img1_2kcQ9Y9.width-530.png	530	416		width-530	63
145	images/img2_uMiX1o6.width-530.png	530	416		width-530	64
146	images/img3_OT3dUiF.2e16d0ba.fill-800x400.png	800	400	2e16d0ba	fill-800x400	65
147	images/img4_8W2e8BQ.width-530.png	530	416		width-530	66
148	images/img5_UCA0lHe.width-530.png	530	416		width-530	67
149	images/img6_xwYjIAZ.width-530.png	530	416		width-530	68
150	images/img7_ks1zC2Y.width-530.png	530	416		width-530	69
151	images/img8_iCqNdaB.width-530.png	530	416		width-530	70
152	images/img9.width-530.png	530	416		width-530	71
153	images/nyborhoods-bg.original.png	1230	533		original	72
154	images/nyborhoods-logo.original.png	148	60		original	73
155	images/nyborhoods-bg_KuTwhI5.max-165x165.png	165	71		max-165x165	74
156	images/bg-nyborhood.max-165x165.jpg	165	103		max-165x165	75
157	images/parallax-bg_0xZdCxL.max-165x165.png	165	60		max-165x165	76
158	images/img1_iVtC2dP.max-165x165.png	165	129		max-165x165	77
159	images/img2_0e2Mv6S.max-165x165.png	165	129		max-165x165	78
160	images/img3_FCXhxAX.max-165x165.png	165	117		max-165x165	79
161	images/img4_WnqqJKF.max-165x165.png	165	129		max-165x165	80
162	images/img5_lnDb8Wg.max-165x165.png	165	129		max-165x165	81
163	images/transafe-bg_3kQKQSs.max-165x165.png	165	71		max-165x165	82
164	images/transafe-logo_63Yo70z.max-165x165.png	111	17		max-165x165	83
165	images/nyborhoods-bg_KuTwhI5.original.png	1230	533		original	74
166	images/bg-nyborhood.original.jpg	2880	1800		original	75
167	images/parallax-bg_0xZdCxL.original.png	1920	699		original	76
168	images/img1_iVtC2dP.width-530.png	530	416		width-530	77
169	images/img2_0e2Mv6S.width-530.png	530	416		width-530	78
170	images/img3_FCXhxAX.2e16d0ba.fill-800x400.png	800	400	2e16d0ba	fill-800x400	79
171	images/img4_WnqqJKF.width-530.png	530	416		width-530	80
172	images/img5_lnDb8Wg.width-530.png	530	416		width-530	81
173	images/transafe-bg_3kQKQSs.original.png	1230	533		original	82
174	images/transafe-logo_63Yo70z.original.png	111	17		original	83
175	images/ziggy-bg.max-165x165.png	165	71		max-165x165	84
176	images/ziggy-logo.max-165x165.png	108	41		max-165x165	85
177	images/bg-ziggy.max-165x165.jpg	165	103		max-165x165	86
178	images/img1_3Cr8P5Q.max-165x165.png	165	109		max-165x165	87
179	images/img2_1G0JXQw.max-165x165.png	165	108		max-165x165	88
180	images/img3_OU4RtFr.max-165x165.png	165	108		max-165x165	89
181	images/ziggy.max-165x165.jpg	165	20		max-165x165	90
182	images/img4_Ubb9HXa.max-165x165.png	165	108		max-165x165	91
183	images/img5_t0LJhOi.max-165x165.png	165	108		max-165x165	92
184	images/qaid-bg.max-165x165.png	165	71		max-165x165	93
185	images/qaid-logo.max-165x165.png	134	32		max-165x165	94
186	images/ziggy-bg.original.png	1230	533		original	84
187	images/ziggy-logo.original.png	108	41		original	85
188	images/bg-ziggy.original.jpg	2880	1800		original	86
189	images/img1_3Cr8P5Q.original.png	1200	794		original	87
190	images/img2_1G0JXQw.width-530.png	530	348		width-530	88
191	images/img3_OU4RtFr.width-530.png	530	348		width-530	89
192	images/ziggy.2e16d0ba.fill-800x400.jpg	800	400	2e16d0ba	fill-800x400	90
193	images/img4_Ubb9HXa.width-530.png	530	348		width-530	91
194	images/img5_t0LJhOi.width-530.png	530	348		width-530	92
195	images/qaid-bg.original.png	1230	533		original	93
196	images/qaid-logo.original.png	134	32		original	94
197	images/bg-qaid.max-165x165.jpg	165	103		max-165x165	95
198	images/img1.max-165x165.jpg	165	109		max-165x165	96
199	images/img2_TrW2PV4.max-165x165.png	165	108		max-165x165	97
200	images/img3_DWwETW0.max-165x165.png	165	108		max-165x165	98
201	images/img4_UsE7U85.max-165x165.png	165	124		max-165x165	99
202	images/img5_2Nk9bul.max-165x165.png	165	108		max-165x165	100
203	images/img6_7tRuzb8.max-165x165.png	165	108		max-165x165	101
204	images/rapiddev-bg.max-165x165.png	165	71		max-165x165	102
205	images/rapiddev-logo.max-165x165.png	91	14		max-165x165	103
206	images/bg-qaid.original.jpg	2880	1800		original	95
207	images/img1.original.jpg	1200	794		original	96
208	images/img2_TrW2PV4.width-530.png	530	348		width-530	97
209	images/img3_DWwETW0.width-530.png	530	348		width-530	98
210	images/img4_UsE7U85.2e16d0ba.fill-800x400.png	800	400	2e16d0ba	fill-800x400	99
211	images/img5_2Nk9bul.width-530.png	530	348		width-530	100
212	images/img6_7tRuzb8.width-530.png	530	348		width-530	101
213	images/rapiddev-bg.original.png	1230	533		original	102
214	images/rapiddev-logo.original.png	91	14		original	103
215	images/rapiddev-bg_IJ8kkoc.max-165x165.png	165	71		max-165x165	104
216	images/bg-rapiddev.max-165x165.jpg	165	103		max-165x165	105
217	images/img1_FGdnH2E.max-165x165.png	165	109		max-165x165	106
218	images/img2_52GKEkp.max-165x165.png	165	108		max-165x165	107
219	images/img3_OF7tKH6.max-165x165.png	165	108		max-165x165	108
220	images/img4_T57SXcP.max-165x165.png	165	124		max-165x165	109
221	images/img5_wzYToPp.max-165x165.png	165	108		max-165x165	110
222	images/img6_4Ka5tIJ.max-165x165.png	165	108		max-165x165	111
223	images/khoon-bg.max-165x165.png	165	71		max-165x165	112
224	images/khoon-logo.max-165x165.png	165	22		max-165x165	113
225	images/rapiddev-bg_IJ8kkoc.original.png	1230	533		original	104
226	images/bg-rapiddev.original.jpg	2880	1800		original	105
227	images/img1_FGdnH2E.original.png	1200	794		original	106
228	images/img2_52GKEkp.width-530.png	530	348		width-530	107
229	images/img3_OF7tKH6.width-530.png	530	348		width-530	108
230	images/img4_T57SXcP.2e16d0ba.fill-800x400.png	800	400	2e16d0ba	fill-800x400	109
231	images/img5_wzYToPp.width-530.png	530	348		width-530	110
232	images/img6_4Ka5tIJ.width-530.png	530	348		width-530	111
233	images/khoon-bg.original.png	1230	533		original	112
234	images/khoon-logo.original.png	223	31		original	113
235	images/bg-khoon.max-165x165.jpg	165	103		max-165x165	114
236	images/img1_lepkFK1.max-165x165.png	165	109		max-165x165	115
237	images/img2_oOkejXi.max-165x165.png	165	108		max-165x165	116
238	images/img3_NM779zE.max-165x165.png	165	108		max-165x165	117
239	images/img4_QibtV3D.max-165x165.png	165	124		max-165x165	118
240	images/img5_oCQLSbX.max-165x165.png	165	108		max-165x165	119
241	images/img6_JzPq0Vr.max-165x165.png	165	108		max-165x165	120
242	images/twitch-bg.max-165x165.png	165	71		max-165x165	121
243	images/twitch-logo.max-165x165.png	133	58		max-165x165	122
244	images/bg-khoon.original.jpg	2880	1800		original	114
245	images/img1_lepkFK1.original.png	1200	794		original	115
246	images/img2_oOkejXi.width-530.png	530	348		width-530	116
247	images/img3_NM779zE.width-530.png	530	348		width-530	117
248	images/img4_QibtV3D.2e16d0ba.fill-800x400.png	800	400	2e16d0ba	fill-800x400	118
249	images/img5_oCQLSbX.width-530.png	530	348		width-530	119
250	images/img6_JzPq0Vr.width-530.png	530	348		width-530	120
251	images/twitch-bg.original.png	1230	533		original	121
252	images/twitch-logo.original.png	133	58		original	122
253	images/lcm-bg.max-165x165.png	165	71		max-165x165	123
254	images/lcm-logo.max-165x165.png	165	28		max-165x165	124
255	images/bg-lcm.max-165x165.jpg	165	103		max-165x165	125
256	images/img1_lAllJkW.max-165x165.png	165	109		max-165x165	126
257	images/img2_a95NUyI.max-165x165.png	165	108		max-165x165	127
258	images/img3_rXgvrvE.max-165x165.png	165	108		max-165x165	128
259	images/img5_PQHE3nk.max-165x165.png	165	108		max-165x165	129
260	images/img6_BlXTtkb.max-165x165.png	165	108		max-165x165	130
261	images/img7_e88z1bV.max-165x165.png	165	108		max-165x165	131
262	images/isp-bg_BvEBIj8.max-165x165.png	165	71		max-165x165	132
263	images/lcm-bg.original.png	1230	533		original	123
264	images/lcm-logo.original.png	227	39		original	124
265	images/bg-lcm.original.jpg	2880	1800		original	125
266	images/img1_lAllJkW.original.png	1200	794		original	126
267	images/img2_a95NUyI.width-530.png	530	348		width-530	127
268	images/img3_rXgvrvE.width-530.png	530	348		width-530	128
269	images/img3_rXgvrvE.2e16d0ba.fill-800x400.png	730	366	2e16d0ba	fill-800x400	128
270	images/img5_PQHE3nk.width-530.png	530	348		width-530	129
271	images/img6_BlXTtkb.width-530.png	530	348		width-530	130
272	images/img7_e88z1bV.width-530.png	530	348		width-530	131
273	images/isp-bg_BvEBIj8.original.png	1230	533		original	132
274	images/img1_lAllJkW.2e16d0ba.fill-320x180.png	320	180	2e16d0ba	fill-320x180	126
275	images/img7_e88z1bV.2e16d0ba.fill-800x400.png	730	366	2e16d0ba	fill-800x400	131
\.


--
-- Data for Name: wagtailimages_uploadedimage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailimages_uploadedimage (id, file, uploaded_by_user_id) FROM stdin;
\.


--
-- Data for Name: wagtailredirects_redirect; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailredirects_redirect (id, old_path, is_permanent, redirect_link, redirect_page_id, site_id) FROM stdin;
\.


--
-- Data for Name: wagtailsearch_editorspick; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailsearch_editorspick (id, sort_order, description, page_id, query_id) FROM stdin;
\.


--
-- Data for Name: wagtailsearch_query; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailsearch_query (id, query_string) FROM stdin;
\.


--
-- Data for Name: wagtailsearch_querydailyhits; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailsearch_querydailyhits (id, date, hits, query_id) FROM stdin;
\.


--
-- Data for Name: wagtailusers_userprofile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.wagtailusers_userprofile (id, submitted_notifications, approved_notifications, rejected_notifications, user_id, preferred_language, current_time_zone, avatar, updated_comments_notifications) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 2, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 18, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 195, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 2, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 2, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: contact_formfield_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.contact_formfield_id_seq', 6, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 49, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 164, true);


--
-- Name: taggit_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.taggit_tag_id_seq', 1, false);


--
-- Name: taggit_taggeditem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.taggit_taggeditem_id_seq', 1, false);


--
-- Name: wagtailadmin_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailadmin_admin_id_seq', 1, false);


--
-- Name: wagtailcore_collection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_collection_id_seq', 1, true);


--
-- Name: wagtailcore_collectionviewrestriction_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_collectionviewrestriction_groups_id_seq', 1, false);


--
-- Name: wagtailcore_collectionviewrestriction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_collectionviewrestriction_id_seq', 1, false);


--
-- Name: wagtailcore_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_comment_id_seq', 1, false);


--
-- Name: wagtailcore_commentreply_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_commentreply_id_seq', 1, false);


--
-- Name: wagtailcore_groupapprovaltask_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_groupapprovaltask_groups_id_seq', 1, true);


--
-- Name: wagtailcore_groupcollectionpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_groupcollectionpermission_id_seq', 12, true);


--
-- Name: wagtailcore_grouppagepermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_grouppagepermission_id_seq', 7, true);


--
-- Name: wagtailcore_locale_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_locale_id_seq', 1, true);


--
-- Name: wagtailcore_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_page_id_seq', 22, true);


--
-- Name: wagtailcore_pagelogentry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pagelogentry_id_seq', 49, true);


--
-- Name: wagtailcore_pagerevision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pagerevision_id_seq', 24, true);


--
-- Name: wagtailcore_pagesubscription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pagesubscription_id_seq', 20, true);


--
-- Name: wagtailcore_pageviewrestriction_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pageviewrestriction_groups_id_seq', 1, false);


--
-- Name: wagtailcore_pageviewrestriction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_pageviewrestriction_id_seq', 1, false);


--
-- Name: wagtailcore_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_site_id_seq', 2, true);


--
-- Name: wagtailcore_task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_task_id_seq', 1, true);


--
-- Name: wagtailcore_taskstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_taskstate_id_seq', 1, false);


--
-- Name: wagtailcore_workflow_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_workflow_id_seq', 1, true);


--
-- Name: wagtailcore_workflowstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_workflowstate_id_seq', 1, false);


--
-- Name: wagtailcore_workflowtask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailcore_workflowtask_id_seq', 1, true);


--
-- Name: wagtaildocs_document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtaildocs_document_id_seq', 1, false);


--
-- Name: wagtaildocs_uploadeddocument_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtaildocs_uploadeddocument_id_seq', 1, false);


--
-- Name: wagtailembeds_embed_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailembeds_embed_id_seq', 1, false);


--
-- Name: wagtailforms_formsubmission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailforms_formsubmission_id_seq', 4, true);


--
-- Name: wagtailimages_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailimages_image_id_seq', 132, true);


--
-- Name: wagtailimages_rendition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailimages_rendition_id_seq', 275, true);


--
-- Name: wagtailimages_uploadedimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailimages_uploadedimage_id_seq', 1, false);


--
-- Name: wagtailredirects_redirect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailredirects_redirect_id_seq', 1, false);


--
-- Name: wagtailsearch_editorspick_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailsearch_editorspick_id_seq', 1, false);


--
-- Name: wagtailsearch_query_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailsearch_query_id_seq', 1, false);


--
-- Name: wagtailsearch_querydailyhits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailsearch_querydailyhits_id_seq', 1, false);


--
-- Name: wagtailusers_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wagtailusers_userprofile_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: blog_blogdetailpage blog_blogdetailpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_blogdetailpage
    ADD CONSTRAINT blog_blogdetailpage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: blog_bloglistingpage blog_bloglistingpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_bloglistingpage
    ADD CONSTRAINT blog_bloglistingpage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: contact_contactpage contact_contactpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_contactpage
    ADD CONSTRAINT contact_contactpage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: contact_formfield contact_formfield_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_formfield
    ADD CONSTRAINT contact_formfield_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: home_aboutpage home_aboutpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home_aboutpage
    ADD CONSTRAINT home_aboutpage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: home_homepage home_homepage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home_homepage
    ADD CONSTRAINT home_homepage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: home_technologiespage home_technologiespage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home_technologiespage
    ADD CONSTRAINT home_technologiespage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: project_projectsdetailpage project_projectsdetailpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectsdetailpage
    ADD CONSTRAINT project_projectsdetailpage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: project_projectslistingpage project_projectslistingpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectslistingpage
    ADD CONSTRAINT project_projectslistingpage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: taggit_tag taggit_tag_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_tag
    ADD CONSTRAINT taggit_tag_name_key UNIQUE (name);


--
-- Name: taggit_tag taggit_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_tag
    ADD CONSTRAINT taggit_tag_pkey PRIMARY KEY (id);


--
-- Name: taggit_tag taggit_tag_slug_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_tag
    ADD CONSTRAINT taggit_tag_slug_key UNIQUE (slug);


--
-- Name: taggit_taggeditem taggit_taggeditem_content_type_id_object_i_4bb97a8e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_content_type_id_object_i_4bb97a8e_uniq UNIQUE (content_type_id, object_id, tag_id);


--
-- Name: taggit_taggeditem taggit_taggeditem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_pkey PRIMARY KEY (id);


--
-- Name: wagtailadmin_admin wagtailadmin_admin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailadmin_admin
    ADD CONSTRAINT wagtailadmin_admin_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_collection wagtailcore_collection_path_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collection
    ADD CONSTRAINT wagtailcore_collection_path_key UNIQUE (path);


--
-- Name: wagtailcore_collection wagtailcore_collection_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collection
    ADD CONSTRAINT wagtailcore_collection_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_collectionviewrestriction_groups wagtailcore_collectionvi_collectionviewrestrictio_988995ae_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups
    ADD CONSTRAINT wagtailcore_collectionvi_collectionviewrestrictio_988995ae_uniq UNIQUE (collectionviewrestriction_id, group_id);


--
-- Name: wagtailcore_collectionviewrestriction_groups wagtailcore_collectionviewrestriction_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups
    ADD CONSTRAINT wagtailcore_collectionviewrestriction_groups_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_collectionviewrestriction wagtailcore_collectionviewrestriction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction
    ADD CONSTRAINT wagtailcore_collectionviewrestriction_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_comment wagtailcore_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_commentreply wagtailcore_commentreply_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_commentreply
    ADD CONSTRAINT wagtailcore_commentreply_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_groupapprovaltask_groups wagtailcore_groupapprova_groupapprovaltask_id_gro_bb5ee7eb_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups
    ADD CONSTRAINT wagtailcore_groupapprova_groupapprovaltask_id_gro_bb5ee7eb_uniq UNIQUE (groupapprovaltask_id, group_id);


--
-- Name: wagtailcore_groupapprovaltask_groups wagtailcore_groupapprovaltask_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups
    ADD CONSTRAINT wagtailcore_groupapprovaltask_groups_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_groupapprovaltask wagtailcore_groupapprovaltask_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask
    ADD CONSTRAINT wagtailcore_groupapprovaltask_pkey PRIMARY KEY (task_ptr_id);


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcollect_group_id_collection_id_p_a21cefe9_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcollect_group_id_collection_id_p_a21cefe9_uniq UNIQUE (group_id, collection_id, permission_id);


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcollectionpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcollectionpermission_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_grouppagepermission wagtailcore_grouppageper_group_id_page_id_permiss_0898bdf8_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission
    ADD CONSTRAINT wagtailcore_grouppageper_group_id_page_id_permiss_0898bdf8_uniq UNIQUE (group_id, page_id, permission_type);


--
-- Name: wagtailcore_grouppagepermission wagtailcore_grouppagepermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission
    ADD CONSTRAINT wagtailcore_grouppagepermission_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_locale wagtailcore_locale_language_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_locale
    ADD CONSTRAINT wagtailcore_locale_language_code_key UNIQUE (language_code);


--
-- Name: wagtailcore_locale wagtailcore_locale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_locale
    ADD CONSTRAINT wagtailcore_locale_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_page wagtailcore_page_path_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_path_key UNIQUE (path);


--
-- Name: wagtailcore_page wagtailcore_page_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_page wagtailcore_page_translation_key_locale_id_9b041bad_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_translation_key_locale_id_9b041bad_uniq UNIQUE (translation_key, locale_id);


--
-- Name: wagtailcore_pagelogentry wagtailcore_pagelogentry_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagelogentry
    ADD CONSTRAINT wagtailcore_pagelogentry_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_pagerevision wagtailcore_pagerevision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagerevision
    ADD CONSTRAINT wagtailcore_pagerevision_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_pagesubscription wagtailcore_pagesubscription_page_id_user_id_0cef73ed_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription
    ADD CONSTRAINT wagtailcore_pagesubscription_page_id_user_id_0cef73ed_uniq UNIQUE (page_id, user_id);


--
-- Name: wagtailcore_pagesubscription wagtailcore_pagesubscription_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription
    ADD CONSTRAINT wagtailcore_pagesubscription_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_pageviewrestriction_groups wagtailcore_pageviewrest_pageviewrestriction_id_g_d23f80bb_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups
    ADD CONSTRAINT wagtailcore_pageviewrest_pageviewrestriction_id_g_d23f80bb_uniq UNIQUE (pageviewrestriction_id, group_id);


--
-- Name: wagtailcore_pageviewrestriction_groups wagtailcore_pageviewrestriction_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups
    ADD CONSTRAINT wagtailcore_pageviewrestriction_groups_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_pageviewrestriction wagtailcore_pageviewrestriction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction
    ADD CONSTRAINT wagtailcore_pageviewrestriction_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_site wagtailcore_site_hostname_port_2c626d70_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_site
    ADD CONSTRAINT wagtailcore_site_hostname_port_2c626d70_uniq UNIQUE (hostname, port);


--
-- Name: wagtailcore_site wagtailcore_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_site
    ADD CONSTRAINT wagtailcore_site_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_task wagtailcore_task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_task
    ADD CONSTRAINT wagtailcore_task_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_taskstate wagtailcore_taskstate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstate_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_workflow wagtailcore_workflow_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflow
    ADD CONSTRAINT wagtailcore_workflow_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_workflowpage wagtailcore_workflowpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowpage
    ADD CONSTRAINT wagtailcore_workflowpage_pkey PRIMARY KEY (page_id);


--
-- Name: wagtailcore_workflowstate wagtailcore_workflowstate_current_task_state_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflowstate_current_task_state_id_key UNIQUE (current_task_state_id);


--
-- Name: wagtailcore_workflowstate wagtailcore_workflowstate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflowstate_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_workflowtask wagtailcore_workflowtask_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask
    ADD CONSTRAINT wagtailcore_workflowtask_pkey PRIMARY KEY (id);


--
-- Name: wagtailcore_workflowtask wagtailcore_workflowtask_workflow_id_task_id_4ec7a62b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask
    ADD CONSTRAINT wagtailcore_workflowtask_workflow_id_task_id_4ec7a62b_uniq UNIQUE (workflow_id, task_id);


--
-- Name: wagtaildocs_document wagtaildocs_document_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_document
    ADD CONSTRAINT wagtaildocs_document_pkey PRIMARY KEY (id);


--
-- Name: wagtaildocs_uploadeddocument wagtaildocs_uploadeddocument_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_uploadeddocument
    ADD CONSTRAINT wagtaildocs_uploadeddocument_pkey PRIMARY KEY (id);


--
-- Name: wagtailembeds_embed wagtailembeds_embed_hash_c9bd8c9a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailembeds_embed
    ADD CONSTRAINT wagtailembeds_embed_hash_c9bd8c9a_uniq UNIQUE (hash);


--
-- Name: wagtailembeds_embed wagtailembeds_embed_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailembeds_embed
    ADD CONSTRAINT wagtailembeds_embed_pkey PRIMARY KEY (id);


--
-- Name: wagtailforms_formsubmission wagtailforms_formsubmission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailforms_formsubmission
    ADD CONSTRAINT wagtailforms_formsubmission_pkey PRIMARY KEY (id);


--
-- Name: wagtailimages_image wagtailimages_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_image
    ADD CONSTRAINT wagtailimages_image_pkey PRIMARY KEY (id);


--
-- Name: wagtailimages_rendition wagtailimages_rendition_image_id_filter_spec_foc_323c8fe0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_rendition
    ADD CONSTRAINT wagtailimages_rendition_image_id_filter_spec_foc_323c8fe0_uniq UNIQUE (image_id, filter_spec, focal_point_key);


--
-- Name: wagtailimages_rendition wagtailimages_rendition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_rendition
    ADD CONSTRAINT wagtailimages_rendition_pkey PRIMARY KEY (id);


--
-- Name: wagtailimages_uploadedimage wagtailimages_uploadedimage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_uploadedimage
    ADD CONSTRAINT wagtailimages_uploadedimage_pkey PRIMARY KEY (id);


--
-- Name: wagtailredirects_redirect wagtailredirects_redirect_old_path_site_id_783622d7_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect
    ADD CONSTRAINT wagtailredirects_redirect_old_path_site_id_783622d7_uniq UNIQUE (old_path, site_id);


--
-- Name: wagtailredirects_redirect wagtailredirects_redirect_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect
    ADD CONSTRAINT wagtailredirects_redirect_pkey PRIMARY KEY (id);


--
-- Name: wagtailsearch_editorspick wagtailsearch_editorspick_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_editorspick
    ADD CONSTRAINT wagtailsearch_editorspick_pkey PRIMARY KEY (id);


--
-- Name: wagtailsearch_query wagtailsearch_query_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_query
    ADD CONSTRAINT wagtailsearch_query_pkey PRIMARY KEY (id);


--
-- Name: wagtailsearch_query wagtailsearch_query_query_string_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_query
    ADD CONSTRAINT wagtailsearch_query_query_string_key UNIQUE (query_string);


--
-- Name: wagtailsearch_querydailyhits wagtailsearch_querydailyhits_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_querydailyhits
    ADD CONSTRAINT wagtailsearch_querydailyhits_pkey PRIMARY KEY (id);


--
-- Name: wagtailsearch_querydailyhits wagtailsearch_querydailyhits_query_id_date_1dd232e6_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_querydailyhits
    ADD CONSTRAINT wagtailsearch_querydailyhits_query_id_date_1dd232e6_uniq UNIQUE (query_id, date);


--
-- Name: wagtailusers_userprofile wagtailusers_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailusers_userprofile
    ADD CONSTRAINT wagtailusers_userprofile_pkey PRIMARY KEY (id);


--
-- Name: wagtailusers_userprofile wagtailusers_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailusers_userprofile
    ADD CONSTRAINT wagtailusers_userprofile_user_id_key UNIQUE (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: blog_blogdetailpage_blog_detail_image_id_4fc0bf64; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX blog_blogdetailpage_blog_detail_image_id_4fc0bf64 ON public.blog_blogdetailpage USING btree (blog_detail_image_id);


--
-- Name: blog_blogdetailpage_blog_list_image_id_1657541b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX blog_blogdetailpage_blog_list_image_id_1657541b ON public.blog_blogdetailpage USING btree (blog_list_image_id);


--
-- Name: contact_formfield_page_id_3ee48e6d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX contact_formfield_page_id_3ee48e6d ON public.contact_formfield USING btree (page_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: project_projectsdetailpage_bg_color_id_07acf3d0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX project_projectsdetailpage_bg_color_id_07acf3d0 ON public.project_projectsdetailpage USING btree (bg_color_id);


--
-- Name: project_projectsdetailpage_nextpage_bg_image_id_1b69bda7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX project_projectsdetailpage_nextpage_bg_image_id_1b69bda7 ON public.project_projectsdetailpage USING btree (nextpage_bg_image_id);


--
-- Name: project_projectsdetailpage_nextpage_logo_id_f55fee12; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX project_projectsdetailpage_nextpage_logo_id_f55fee12 ON public.project_projectsdetailpage USING btree (nextpage_logo_id);


--
-- Name: project_projectsdetailpage_project_list_bg_image_id_59a1154e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX project_projectsdetailpage_project_list_bg_image_id_59a1154e ON public.project_projectsdetailpage USING btree (project_list_bg_image_id);


--
-- Name: project_projectsdetailpage_project_list_logo_id_011711e1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX project_projectsdetailpage_project_list_logo_id_011711e1 ON public.project_projectsdetailpage USING btree (project_list_logo_id);


--
-- Name: project_projectsdetailpage_section1_image_id_bbdadd0e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX project_projectsdetailpage_section1_image_id_bbdadd0e ON public.project_projectsdetailpage USING btree (section1_image_id);


--
-- Name: project_projectsdetailpage_section2_image_id_e68d6470; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX project_projectsdetailpage_section2_image_id_e68d6470 ON public.project_projectsdetailpage USING btree (section2_image_id);


--
-- Name: taggit_tag_name_58eb2ed9_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_tag_name_58eb2ed9_like ON public.taggit_tag USING btree (name varchar_pattern_ops);


--
-- Name: taggit_tag_slug_6be58b2c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_tag_slug_6be58b2c_like ON public.taggit_tag USING btree (slug varchar_pattern_ops);


--
-- Name: taggit_taggeditem_content_type_id_9957a03c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_taggeditem_content_type_id_9957a03c ON public.taggit_taggeditem USING btree (content_type_id);


--
-- Name: taggit_taggeditem_content_type_id_object_id_196cc965_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_taggeditem_content_type_id_object_id_196cc965_idx ON public.taggit_taggeditem USING btree (content_type_id, object_id);


--
-- Name: taggit_taggeditem_object_id_e2d7d1df; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_taggeditem_object_id_e2d7d1df ON public.taggit_taggeditem USING btree (object_id);


--
-- Name: taggit_taggeditem_tag_id_f4f5b767; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX taggit_taggeditem_tag_id_f4f5b767 ON public.taggit_taggeditem USING btree (tag_id);


--
-- Name: unique_in_progress_workflow; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unique_in_progress_workflow ON public.wagtailcore_workflowstate USING btree (page_id) WHERE ((status)::text = ANY ((ARRAY['in_progress'::character varying, 'needs_changes'::character varying])::text[]));


--
-- Name: wagtailcore_collection_path_d848dc19_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_collection_path_d848dc19_like ON public.wagtailcore_collection USING btree (path varchar_pattern_ops);


--
-- Name: wagtailcore_collectionview_collectionviewrestriction__47320efd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_collectionview_collectionviewrestriction__47320efd ON public.wagtailcore_collectionviewrestriction_groups USING btree (collectionviewrestriction_id);


--
-- Name: wagtailcore_collectionviewrestriction_collection_id_761908ec; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_collectionviewrestriction_collection_id_761908ec ON public.wagtailcore_collectionviewrestriction USING btree (collection_id);


--
-- Name: wagtailcore_collectionviewrestriction_groups_group_id_1823f2a3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_collectionviewrestriction_groups_group_id_1823f2a3 ON public.wagtailcore_collectionviewrestriction_groups USING btree (group_id);


--
-- Name: wagtailcore_comment_page_id_108444b5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_comment_page_id_108444b5 ON public.wagtailcore_comment USING btree (page_id);


--
-- Name: wagtailcore_comment_resolved_by_id_a282aa0e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_comment_resolved_by_id_a282aa0e ON public.wagtailcore_comment USING btree (resolved_by_id);


--
-- Name: wagtailcore_comment_revision_created_id_1d058279; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_comment_revision_created_id_1d058279 ON public.wagtailcore_comment USING btree (revision_created_id);


--
-- Name: wagtailcore_comment_user_id_0c577ca6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_comment_user_id_0c577ca6 ON public.wagtailcore_comment USING btree (user_id);


--
-- Name: wagtailcore_commentreply_comment_id_afc7e027; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_commentreply_comment_id_afc7e027 ON public.wagtailcore_commentreply USING btree (comment_id);


--
-- Name: wagtailcore_commentreply_user_id_d0b3b9c3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_commentreply_user_id_d0b3b9c3 ON public.wagtailcore_commentreply USING btree (user_id);


--
-- Name: wagtailcore_groupapprovalt_groupapprovaltask_id_9a9255ea; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupapprovalt_groupapprovaltask_id_9a9255ea ON public.wagtailcore_groupapprovaltask_groups USING btree (groupapprovaltask_id);


--
-- Name: wagtailcore_groupapprovaltask_groups_group_id_2e64b61f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupapprovaltask_groups_group_id_2e64b61f ON public.wagtailcore_groupapprovaltask_groups USING btree (group_id);


--
-- Name: wagtailcore_groupcollectionpermission_collection_id_5423575a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupcollectionpermission_collection_id_5423575a ON public.wagtailcore_groupcollectionpermission USING btree (collection_id);


--
-- Name: wagtailcore_groupcollectionpermission_group_id_05d61460; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupcollectionpermission_group_id_05d61460 ON public.wagtailcore_groupcollectionpermission USING btree (group_id);


--
-- Name: wagtailcore_groupcollectionpermission_permission_id_1b626275; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_groupcollectionpermission_permission_id_1b626275 ON public.wagtailcore_groupcollectionpermission USING btree (permission_id);


--
-- Name: wagtailcore_grouppagepermission_group_id_fc07e671; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_grouppagepermission_group_id_fc07e671 ON public.wagtailcore_grouppagepermission USING btree (group_id);


--
-- Name: wagtailcore_grouppagepermission_page_id_710b114a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_grouppagepermission_page_id_710b114a ON public.wagtailcore_grouppagepermission USING btree (page_id);


--
-- Name: wagtailcore_locale_language_code_03149338_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_locale_language_code_03149338_like ON public.wagtailcore_locale USING btree (language_code varchar_pattern_ops);


--
-- Name: wagtailcore_page_alias_of_id_12945502; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_alias_of_id_12945502 ON public.wagtailcore_page USING btree (alias_of_id);


--
-- Name: wagtailcore_page_content_type_id_c28424df; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_content_type_id_c28424df ON public.wagtailcore_page USING btree (content_type_id);


--
-- Name: wagtailcore_page_first_published_at_2b5dd637; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_first_published_at_2b5dd637 ON public.wagtailcore_page USING btree (first_published_at);


--
-- Name: wagtailcore_page_live_revision_id_930bd822; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_live_revision_id_930bd822 ON public.wagtailcore_page USING btree (live_revision_id);


--
-- Name: wagtailcore_page_locale_id_3c7e30a6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_locale_id_3c7e30a6 ON public.wagtailcore_page USING btree (locale_id);


--
-- Name: wagtailcore_page_locked_by_id_bcb86245; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_locked_by_id_bcb86245 ON public.wagtailcore_page USING btree (locked_by_id);


--
-- Name: wagtailcore_page_owner_id_fbf7c332; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_owner_id_fbf7c332 ON public.wagtailcore_page USING btree (owner_id);


--
-- Name: wagtailcore_page_path_98eba2c8_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_path_98eba2c8_like ON public.wagtailcore_page USING btree (path varchar_pattern_ops);


--
-- Name: wagtailcore_page_slug_e7c11b8f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_slug_e7c11b8f ON public.wagtailcore_page USING btree (slug);


--
-- Name: wagtailcore_page_slug_e7c11b8f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_page_slug_e7c11b8f_like ON public.wagtailcore_page USING btree (slug varchar_pattern_ops);


--
-- Name: wagtailcore_pagelogentry_action_c2408198; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_action_c2408198 ON public.wagtailcore_pagelogentry USING btree (action);


--
-- Name: wagtailcore_pagelogentry_action_c2408198_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_action_c2408198_like ON public.wagtailcore_pagelogentry USING btree (action varchar_pattern_ops);


--
-- Name: wagtailcore_pagelogentry_content_changed_99f27ade; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_content_changed_99f27ade ON public.wagtailcore_pagelogentry USING btree (content_changed);


--
-- Name: wagtailcore_pagelogentry_content_type_id_74e7708a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_content_type_id_74e7708a ON public.wagtailcore_pagelogentry USING btree (content_type_id);


--
-- Name: wagtailcore_pagelogentry_page_id_8464e327; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_page_id_8464e327 ON public.wagtailcore_pagelogentry USING btree (page_id);


--
-- Name: wagtailcore_pagelogentry_revision_id_8043d103; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_revision_id_8043d103 ON public.wagtailcore_pagelogentry USING btree (revision_id);


--
-- Name: wagtailcore_pagelogentry_user_id_604ccfd8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagelogentry_user_id_604ccfd8 ON public.wagtailcore_pagelogentry USING btree (user_id);


--
-- Name: wagtailcore_pagerevision_approved_go_live_at_e56afc67; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_approved_go_live_at_e56afc67 ON public.wagtailcore_pagerevision USING btree (approved_go_live_at);


--
-- Name: wagtailcore_pagerevision_created_at_66954e3b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_created_at_66954e3b ON public.wagtailcore_pagerevision USING btree (created_at);


--
-- Name: wagtailcore_pagerevision_page_id_d421cc1d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_page_id_d421cc1d ON public.wagtailcore_pagerevision USING btree (page_id);


--
-- Name: wagtailcore_pagerevision_submitted_for_moderation_c682e44c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_submitted_for_moderation_c682e44c ON public.wagtailcore_pagerevision USING btree (submitted_for_moderation);


--
-- Name: wagtailcore_pagerevision_user_id_2409d2f4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagerevision_user_id_2409d2f4 ON public.wagtailcore_pagerevision USING btree (user_id);


--
-- Name: wagtailcore_pagesubscription_page_id_a085e7a6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagesubscription_page_id_a085e7a6 ON public.wagtailcore_pagesubscription USING btree (page_id);


--
-- Name: wagtailcore_pagesubscription_user_id_89d7def9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pagesubscription_user_id_89d7def9 ON public.wagtailcore_pagesubscription USING btree (user_id);


--
-- Name: wagtailcore_pageviewrestri_pageviewrestriction_id_f147a99a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pageviewrestri_pageviewrestriction_id_f147a99a ON public.wagtailcore_pageviewrestriction_groups USING btree (pageviewrestriction_id);


--
-- Name: wagtailcore_pageviewrestriction_groups_group_id_6460f223; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pageviewrestriction_groups_group_id_6460f223 ON public.wagtailcore_pageviewrestriction_groups USING btree (group_id);


--
-- Name: wagtailcore_pageviewrestriction_page_id_15a8bea6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_pageviewrestriction_page_id_15a8bea6 ON public.wagtailcore_pageviewrestriction USING btree (page_id);


--
-- Name: wagtailcore_site_hostname_96b20b46; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_site_hostname_96b20b46 ON public.wagtailcore_site USING btree (hostname);


--
-- Name: wagtailcore_site_hostname_96b20b46_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_site_hostname_96b20b46_like ON public.wagtailcore_site USING btree (hostname varchar_pattern_ops);


--
-- Name: wagtailcore_site_root_page_id_e02fb95c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_site_root_page_id_e02fb95c ON public.wagtailcore_site USING btree (root_page_id);


--
-- Name: wagtailcore_task_content_type_id_249ab8ba; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_task_content_type_id_249ab8ba ON public.wagtailcore_task USING btree (content_type_id);


--
-- Name: wagtailcore_taskstate_content_type_id_0a758fdc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_content_type_id_0a758fdc ON public.wagtailcore_taskstate USING btree (content_type_id);


--
-- Name: wagtailcore_taskstate_finished_by_id_13f98229; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_finished_by_id_13f98229 ON public.wagtailcore_taskstate USING btree (finished_by_id);


--
-- Name: wagtailcore_taskstate_page_revision_id_9f52c88e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_page_revision_id_9f52c88e ON public.wagtailcore_taskstate USING btree (page_revision_id);


--
-- Name: wagtailcore_taskstate_task_id_c3677c34; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_task_id_c3677c34 ON public.wagtailcore_taskstate USING btree (task_id);


--
-- Name: wagtailcore_taskstate_workflow_state_id_9239a775; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_taskstate_workflow_state_id_9239a775 ON public.wagtailcore_taskstate USING btree (workflow_state_id);


--
-- Name: wagtailcore_workflowpage_workflow_id_56f56ff6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowpage_workflow_id_56f56ff6 ON public.wagtailcore_workflowpage USING btree (workflow_id);


--
-- Name: wagtailcore_workflowstate_page_id_6c962862; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowstate_page_id_6c962862 ON public.wagtailcore_workflowstate USING btree (page_id);


--
-- Name: wagtailcore_workflowstate_requested_by_id_4090bca3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowstate_requested_by_id_4090bca3 ON public.wagtailcore_workflowstate USING btree (requested_by_id);


--
-- Name: wagtailcore_workflowstate_workflow_id_1f18378f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowstate_workflow_id_1f18378f ON public.wagtailcore_workflowstate USING btree (workflow_id);


--
-- Name: wagtailcore_workflowtask_task_id_ce7716fe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowtask_task_id_ce7716fe ON public.wagtailcore_workflowtask USING btree (task_id);


--
-- Name: wagtailcore_workflowtask_workflow_id_b9717175; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailcore_workflowtask_workflow_id_b9717175 ON public.wagtailcore_workflowtask USING btree (workflow_id);


--
-- Name: wagtaildocs_document_collection_id_23881625; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtaildocs_document_collection_id_23881625 ON public.wagtaildocs_document USING btree (collection_id);


--
-- Name: wagtaildocs_document_uploaded_by_user_id_17258b41; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtaildocs_document_uploaded_by_user_id_17258b41 ON public.wagtaildocs_document USING btree (uploaded_by_user_id);


--
-- Name: wagtaildocs_uploadeddocument_uploaded_by_user_id_8dd61a73; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtaildocs_uploadeddocument_uploaded_by_user_id_8dd61a73 ON public.wagtaildocs_uploadeddocument USING btree (uploaded_by_user_id);


--
-- Name: wagtailembeds_embed_cache_until_26c94bb0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailembeds_embed_cache_until_26c94bb0 ON public.wagtailembeds_embed USING btree (cache_until);


--
-- Name: wagtailembeds_embed_hash_c9bd8c9a_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailembeds_embed_hash_c9bd8c9a_like ON public.wagtailembeds_embed USING btree (hash varchar_pattern_ops);


--
-- Name: wagtailforms_formsubmission_page_id_e48e93e7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailforms_formsubmission_page_id_e48e93e7 ON public.wagtailforms_formsubmission USING btree (page_id);


--
-- Name: wagtailimages_image_collection_id_c2f8af7e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_image_collection_id_c2f8af7e ON public.wagtailimages_image USING btree (collection_id);


--
-- Name: wagtailimages_image_created_at_86fa6cd4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_image_created_at_86fa6cd4 ON public.wagtailimages_image USING btree (created_at);


--
-- Name: wagtailimages_image_uploaded_by_user_id_5d73dc75; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_image_uploaded_by_user_id_5d73dc75 ON public.wagtailimages_image USING btree (uploaded_by_user_id);


--
-- Name: wagtailimages_rendition_filter_spec_1cba3201; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_rendition_filter_spec_1cba3201 ON public.wagtailimages_rendition USING btree (filter_spec);


--
-- Name: wagtailimages_rendition_filter_spec_1cba3201_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_rendition_filter_spec_1cba3201_like ON public.wagtailimages_rendition USING btree (filter_spec varchar_pattern_ops);


--
-- Name: wagtailimages_rendition_image_id_3e1fd774; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_rendition_image_id_3e1fd774 ON public.wagtailimages_rendition USING btree (image_id);


--
-- Name: wagtailimages_uploadedimage_uploaded_by_user_id_85921689; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailimages_uploadedimage_uploaded_by_user_id_85921689 ON public.wagtailimages_uploadedimage USING btree (uploaded_by_user_id);


--
-- Name: wagtailredirects_redirect_old_path_bb35247b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailredirects_redirect_old_path_bb35247b ON public.wagtailredirects_redirect USING btree (old_path);


--
-- Name: wagtailredirects_redirect_old_path_bb35247b_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailredirects_redirect_old_path_bb35247b_like ON public.wagtailredirects_redirect USING btree (old_path varchar_pattern_ops);


--
-- Name: wagtailredirects_redirect_redirect_page_id_b5728a8f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailredirects_redirect_redirect_page_id_b5728a8f ON public.wagtailredirects_redirect USING btree (redirect_page_id);


--
-- Name: wagtailredirects_redirect_site_id_780a0e1e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailredirects_redirect_site_id_780a0e1e ON public.wagtailredirects_redirect USING btree (site_id);


--
-- Name: wagtailsearch_editorspick_page_id_28cbc274; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailsearch_editorspick_page_id_28cbc274 ON public.wagtailsearch_editorspick USING btree (page_id);


--
-- Name: wagtailsearch_editorspick_query_id_c6eee4a0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailsearch_editorspick_query_id_c6eee4a0 ON public.wagtailsearch_editorspick USING btree (query_id);


--
-- Name: wagtailsearch_query_query_string_e785ea07_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailsearch_query_query_string_e785ea07_like ON public.wagtailsearch_query USING btree (query_string varchar_pattern_ops);


--
-- Name: wagtailsearch_querydailyhits_query_id_2185994b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wagtailsearch_querydailyhits_query_id_2185994b ON public.wagtailsearch_querydailyhits USING btree (query_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blogdetailpage blog_blogdetailpage_blog_detail_image_id_4fc0bf64_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_blogdetailpage
    ADD CONSTRAINT blog_blogdetailpage_blog_detail_image_id_4fc0bf64_fk_wagtailim FOREIGN KEY (blog_detail_image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blogdetailpage blog_blogdetailpage_blog_list_image_id_1657541b_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_blogdetailpage
    ADD CONSTRAINT blog_blogdetailpage_blog_list_image_id_1657541b_fk_wagtailim FOREIGN KEY (blog_list_image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blogdetailpage blog_blogdetailpage_page_ptr_id_fb1104b9_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_blogdetailpage
    ADD CONSTRAINT blog_blogdetailpage_page_ptr_id_fb1104b9_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_bloglistingpage blog_bloglistingpage_page_ptr_id_7666e38e_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_bloglistingpage
    ADD CONSTRAINT blog_bloglistingpage_page_ptr_id_7666e38e_fk_wagtailco FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contact_contactpage contact_contactpage_page_ptr_id_143c93c1_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_contactpage
    ADD CONSTRAINT contact_contactpage_page_ptr_id_143c93c1_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contact_formfield contact_formfield_page_id_3ee48e6d_fk_contact_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_formfield
    ADD CONSTRAINT contact_formfield_page_id_3ee48e6d_fk_contact_c FOREIGN KEY (page_id) REFERENCES public.contact_contactpage(page_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: home_aboutpage home_aboutpage_page_ptr_id_f69f9474_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home_aboutpage
    ADD CONSTRAINT home_aboutpage_page_ptr_id_f69f9474_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: home_homepage home_homepage_page_ptr_id_e5b77cf7_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home_homepage
    ADD CONSTRAINT home_homepage_page_ptr_id_e5b77cf7_fk_wagtailcore_page_id FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: home_technologiespage home_technologiespag_page_ptr_id_ed1f48ee_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.home_technologiespage
    ADD CONSTRAINT home_technologiespag_page_ptr_id_ed1f48ee_fk_wagtailco FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: project_projectsdetailpage project_projectsdeta_bg_color_id_07acf3d0_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectsdetailpage
    ADD CONSTRAINT project_projectsdeta_bg_color_id_07acf3d0_fk_wagtailim FOREIGN KEY (bg_color_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: project_projectsdetailpage project_projectsdeta_nextpage_bg_image_id_1b69bda7_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectsdetailpage
    ADD CONSTRAINT project_projectsdeta_nextpage_bg_image_id_1b69bda7_fk_wagtailim FOREIGN KEY (nextpage_bg_image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: project_projectsdetailpage project_projectsdeta_nextpage_logo_id_f55fee12_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectsdetailpage
    ADD CONSTRAINT project_projectsdeta_nextpage_logo_id_f55fee12_fk_wagtailim FOREIGN KEY (nextpage_logo_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: project_projectsdetailpage project_projectsdeta_page_ptr_id_fbd3e80c_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectsdetailpage
    ADD CONSTRAINT project_projectsdeta_page_ptr_id_fbd3e80c_fk_wagtailco FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: project_projectsdetailpage project_projectsdeta_project_list_bg_imag_59a1154e_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectsdetailpage
    ADD CONSTRAINT project_projectsdeta_project_list_bg_imag_59a1154e_fk_wagtailim FOREIGN KEY (project_list_bg_image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: project_projectsdetailpage project_projectsdeta_project_list_logo_id_011711e1_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectsdetailpage
    ADD CONSTRAINT project_projectsdeta_project_list_logo_id_011711e1_fk_wagtailim FOREIGN KEY (project_list_logo_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: project_projectsdetailpage project_projectsdeta_section1_image_id_bbdadd0e_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectsdetailpage
    ADD CONSTRAINT project_projectsdeta_section1_image_id_bbdadd0e_fk_wagtailim FOREIGN KEY (section1_image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: project_projectsdetailpage project_projectsdeta_section2_image_id_e68d6470_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectsdetailpage
    ADD CONSTRAINT project_projectsdeta_section2_image_id_e68d6470_fk_wagtailim FOREIGN KEY (section2_image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: project_projectslistingpage project_projectslist_page_ptr_id_06742f9d_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_projectslistingpage
    ADD CONSTRAINT project_projectslist_page_ptr_id_06742f9d_fk_wagtailco FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taggit_taggeditem taggit_taggeditem_content_type_id_9957a03c_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_content_type_id_9957a03c_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taggit_taggeditem taggit_taggeditem_tag_id_f4f5b767_fk_taggit_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taggit_taggeditem
    ADD CONSTRAINT taggit_taggeditem_tag_id_f4f5b767_fk_taggit_tag_id FOREIGN KEY (tag_id) REFERENCES public.taggit_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_collectionviewrestriction wagtailcore_collecti_collection_id_761908ec_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction
    ADD CONSTRAINT wagtailcore_collecti_collection_id_761908ec_fk_wagtailco FOREIGN KEY (collection_id) REFERENCES public.wagtailcore_collection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_collectionviewrestriction_groups wagtailcore_collecti_collectionviewrestri_47320efd_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups
    ADD CONSTRAINT wagtailcore_collecti_collectionviewrestri_47320efd_fk_wagtailco FOREIGN KEY (collectionviewrestriction_id) REFERENCES public.wagtailcore_collectionviewrestriction(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_collectionviewrestriction_groups wagtailcore_collecti_group_id_1823f2a3_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_collectionviewrestriction_groups
    ADD CONSTRAINT wagtailcore_collecti_group_id_1823f2a3_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_comment wagtailcore_comment_page_id_108444b5_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_page_id_108444b5_fk_wagtailcore_page_id FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_comment wagtailcore_comment_resolved_by_id_a282aa0e_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_resolved_by_id_a282aa0e_fk_auth_user_id FOREIGN KEY (resolved_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_comment wagtailcore_comment_revision_created_id_1d058279_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_revision_created_id_1d058279_fk_wagtailco FOREIGN KEY (revision_created_id) REFERENCES public.wagtailcore_pagerevision(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_comment wagtailcore_comment_user_id_0c577ca6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_comment
    ADD CONSTRAINT wagtailcore_comment_user_id_0c577ca6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_commentreply wagtailcore_commentr_comment_id_afc7e027_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_commentreply
    ADD CONSTRAINT wagtailcore_commentr_comment_id_afc7e027_fk_wagtailco FOREIGN KEY (comment_id) REFERENCES public.wagtailcore_comment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_commentreply wagtailcore_commentreply_user_id_d0b3b9c3_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_commentreply
    ADD CONSTRAINT wagtailcore_commentreply_user_id_d0b3b9c3_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupapprovaltask_groups wagtailcore_groupapp_group_id_2e64b61f_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups
    ADD CONSTRAINT wagtailcore_groupapp_group_id_2e64b61f_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupapprovaltask_groups wagtailcore_groupapp_groupapprovaltask_id_9a9255ea_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask_groups
    ADD CONSTRAINT wagtailcore_groupapp_groupapprovaltask_id_9a9255ea_fk_wagtailco FOREIGN KEY (groupapprovaltask_id) REFERENCES public.wagtailcore_groupapprovaltask(task_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupapprovaltask wagtailcore_groupapp_task_ptr_id_cfe58781_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupapprovaltask
    ADD CONSTRAINT wagtailcore_groupapp_task_ptr_id_cfe58781_fk_wagtailco FOREIGN KEY (task_ptr_id) REFERENCES public.wagtailcore_task(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcol_collection_id_5423575a_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcol_collection_id_5423575a_fk_wagtailco FOREIGN KEY (collection_id) REFERENCES public.wagtailcore_collection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcol_group_id_05d61460_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcol_group_id_05d61460_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_groupcollectionpermission wagtailcore_groupcol_permission_id_1b626275_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_groupcollectionpermission
    ADD CONSTRAINT wagtailcore_groupcol_permission_id_1b626275_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_grouppagepermission wagtailcore_grouppag_group_id_fc07e671_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission
    ADD CONSTRAINT wagtailcore_grouppag_group_id_fc07e671_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_grouppagepermission wagtailcore_grouppag_page_id_710b114a_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_grouppagepermission
    ADD CONSTRAINT wagtailcore_grouppag_page_id_710b114a_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_alias_of_id_12945502_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_alias_of_id_12945502_fk_wagtailcore_page_id FOREIGN KEY (alias_of_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_content_type_id_c28424df_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_content_type_id_c28424df_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_live_revision_id_930bd822_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_live_revision_id_930bd822_fk_wagtailco FOREIGN KEY (live_revision_id) REFERENCES public.wagtailcore_pagerevision(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_locale_id_3c7e30a6_fk_wagtailcore_locale_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_locale_id_3c7e30a6_fk_wagtailcore_locale_id FOREIGN KEY (locale_id) REFERENCES public.wagtailcore_locale(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_locked_by_id_bcb86245_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_locked_by_id_bcb86245_fk_auth_user_id FOREIGN KEY (locked_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_page wagtailcore_page_owner_id_fbf7c332_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_page
    ADD CONSTRAINT wagtailcore_page_owner_id_fbf7c332_fk_auth_user_id FOREIGN KEY (owner_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagelogentry wagtailcore_pageloge_content_type_id_74e7708a_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagelogentry
    ADD CONSTRAINT wagtailcore_pageloge_content_type_id_74e7708a_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagerevision wagtailcore_pagerevi_page_id_d421cc1d_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagerevision
    ADD CONSTRAINT wagtailcore_pagerevi_page_id_d421cc1d_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagerevision wagtailcore_pagerevision_user_id_2409d2f4_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagerevision
    ADD CONSTRAINT wagtailcore_pagerevision_user_id_2409d2f4_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagesubscription wagtailcore_pagesubs_page_id_a085e7a6_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription
    ADD CONSTRAINT wagtailcore_pagesubs_page_id_a085e7a6_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pagesubscription wagtailcore_pagesubscription_user_id_89d7def9_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pagesubscription
    ADD CONSTRAINT wagtailcore_pagesubscription_user_id_89d7def9_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pageviewrestriction_groups wagtailcore_pageview_group_id_6460f223_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups
    ADD CONSTRAINT wagtailcore_pageview_group_id_6460f223_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pageviewrestriction wagtailcore_pageview_page_id_15a8bea6_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction
    ADD CONSTRAINT wagtailcore_pageview_page_id_15a8bea6_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_pageviewrestriction_groups wagtailcore_pageview_pageviewrestriction__f147a99a_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_pageviewrestriction_groups
    ADD CONSTRAINT wagtailcore_pageview_pageviewrestriction__f147a99a_fk_wagtailco FOREIGN KEY (pageviewrestriction_id) REFERENCES public.wagtailcore_pageviewrestriction(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_site wagtailcore_site_root_page_id_e02fb95c_fk_wagtailcore_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_site
    ADD CONSTRAINT wagtailcore_site_root_page_id_e02fb95c_fk_wagtailcore_page_id FOREIGN KEY (root_page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_task wagtailcore_task_content_type_id_249ab8ba_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_task
    ADD CONSTRAINT wagtailcore_task_content_type_id_249ab8ba_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstat_content_type_id_0a758fdc_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstat_content_type_id_0a758fdc_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstat_page_revision_id_9f52c88e_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstat_page_revision_id_9f52c88e_fk_wagtailco FOREIGN KEY (page_revision_id) REFERENCES public.wagtailcore_pagerevision(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstat_workflow_state_id_9239a775_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstat_workflow_state_id_9239a775_fk_wagtailco FOREIGN KEY (workflow_state_id) REFERENCES public.wagtailcore_workflowstate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstate_finished_by_id_13f98229_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstate_finished_by_id_13f98229_fk_auth_user_id FOREIGN KEY (finished_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_taskstate wagtailcore_taskstate_task_id_c3677c34_fk_wagtailcore_task_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_taskstate
    ADD CONSTRAINT wagtailcore_taskstate_task_id_c3677c34_fk_wagtailcore_task_id FOREIGN KEY (task_id) REFERENCES public.wagtailcore_task(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowstate wagtailcore_workflow_current_task_state_i_3a1a0632_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflow_current_task_state_i_3a1a0632_fk_wagtailco FOREIGN KEY (current_task_state_id) REFERENCES public.wagtailcore_taskstate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowstate wagtailcore_workflow_page_id_6c962862_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflow_page_id_6c962862_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowpage wagtailcore_workflow_page_id_81e7bab6_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowpage
    ADD CONSTRAINT wagtailcore_workflow_page_id_81e7bab6_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowstate wagtailcore_workflow_requested_by_id_4090bca3_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflow_requested_by_id_4090bca3_fk_auth_user FOREIGN KEY (requested_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowtask wagtailcore_workflow_task_id_ce7716fe_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask
    ADD CONSTRAINT wagtailcore_workflow_task_id_ce7716fe_fk_wagtailco FOREIGN KEY (task_id) REFERENCES public.wagtailcore_task(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowstate wagtailcore_workflow_workflow_id_1f18378f_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowstate
    ADD CONSTRAINT wagtailcore_workflow_workflow_id_1f18378f_fk_wagtailco FOREIGN KEY (workflow_id) REFERENCES public.wagtailcore_workflow(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowpage wagtailcore_workflow_workflow_id_56f56ff6_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowpage
    ADD CONSTRAINT wagtailcore_workflow_workflow_id_56f56ff6_fk_wagtailco FOREIGN KEY (workflow_id) REFERENCES public.wagtailcore_workflow(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailcore_workflowtask wagtailcore_workflow_workflow_id_b9717175_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailcore_workflowtask
    ADD CONSTRAINT wagtailcore_workflow_workflow_id_b9717175_fk_wagtailco FOREIGN KEY (workflow_id) REFERENCES public.wagtailcore_workflow(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtaildocs_document wagtaildocs_document_collection_id_23881625_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_document
    ADD CONSTRAINT wagtaildocs_document_collection_id_23881625_fk_wagtailco FOREIGN KEY (collection_id) REFERENCES public.wagtailcore_collection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtaildocs_document wagtaildocs_document_uploaded_by_user_id_17258b41_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_document
    ADD CONSTRAINT wagtaildocs_document_uploaded_by_user_id_17258b41_fk_auth_user FOREIGN KEY (uploaded_by_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtaildocs_uploadeddocument wagtaildocs_uploaded_uploaded_by_user_id_8dd61a73_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtaildocs_uploadeddocument
    ADD CONSTRAINT wagtaildocs_uploaded_uploaded_by_user_id_8dd61a73_fk_auth_user FOREIGN KEY (uploaded_by_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailforms_formsubmission wagtailforms_formsub_page_id_e48e93e7_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailforms_formsubmission
    ADD CONSTRAINT wagtailforms_formsub_page_id_e48e93e7_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailimages_image wagtailimages_image_collection_id_c2f8af7e_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_image
    ADD CONSTRAINT wagtailimages_image_collection_id_c2f8af7e_fk_wagtailco FOREIGN KEY (collection_id) REFERENCES public.wagtailcore_collection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailimages_image wagtailimages_image_uploaded_by_user_id_5d73dc75_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_image
    ADD CONSTRAINT wagtailimages_image_uploaded_by_user_id_5d73dc75_fk_auth_user FOREIGN KEY (uploaded_by_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailimages_rendition wagtailimages_rendit_image_id_3e1fd774_fk_wagtailim; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_rendition
    ADD CONSTRAINT wagtailimages_rendit_image_id_3e1fd774_fk_wagtailim FOREIGN KEY (image_id) REFERENCES public.wagtailimages_image(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailimages_uploadedimage wagtailimages_upload_uploaded_by_user_id_85921689_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailimages_uploadedimage
    ADD CONSTRAINT wagtailimages_upload_uploaded_by_user_id_85921689_fk_auth_user FOREIGN KEY (uploaded_by_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailredirects_redirect wagtailredirects_red_redirect_page_id_b5728a8f_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect
    ADD CONSTRAINT wagtailredirects_red_redirect_page_id_b5728a8f_fk_wagtailco FOREIGN KEY (redirect_page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailredirects_redirect wagtailredirects_red_site_id_780a0e1e_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailredirects_redirect
    ADD CONSTRAINT wagtailredirects_red_site_id_780a0e1e_fk_wagtailco FOREIGN KEY (site_id) REFERENCES public.wagtailcore_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailsearch_editorspick wagtailsearch_editor_page_id_28cbc274_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_editorspick
    ADD CONSTRAINT wagtailsearch_editor_page_id_28cbc274_fk_wagtailco FOREIGN KEY (page_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailsearch_editorspick wagtailsearch_editor_query_id_c6eee4a0_fk_wagtailse; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_editorspick
    ADD CONSTRAINT wagtailsearch_editor_query_id_c6eee4a0_fk_wagtailse FOREIGN KEY (query_id) REFERENCES public.wagtailsearch_query(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailsearch_querydailyhits wagtailsearch_queryd_query_id_2185994b_fk_wagtailse; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailsearch_querydailyhits
    ADD CONSTRAINT wagtailsearch_queryd_query_id_2185994b_fk_wagtailse FOREIGN KEY (query_id) REFERENCES public.wagtailsearch_query(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: wagtailusers_userprofile wagtailusers_userprofile_user_id_59c92331_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wagtailusers_userprofile
    ADD CONSTRAINT wagtailusers_userprofile_user_id_59c92331_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

