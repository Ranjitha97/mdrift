from django.db import models

from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.core.fields import StreamField


class HomePage(Page):

    template = "home/index.html"

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)
    client_logos = StreamField(
        [
            ('image', ImageChooserBlock()),
        ],
        null=False,
        blank=True,
    )  
    content_panels = Page.content_panels + [
        FieldPanel('header_title'),
        FieldPanel('meta_content'),
        StreamFieldPanel('client_logos'),
    ]

class AboutPage(Page):

    template = "about/about_us.html"

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)
    content_panels = Page.content_panels + [
        FieldPanel('header_title'),
        FieldPanel('meta_content'),
    ]

class TechnologiesPage(Page):

    template = "technologies/technical-overview.html"

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)
    content_panels = Page.content_panels + [
        FieldPanel('header_title'),
        FieldPanel('meta_content'),
    ]    