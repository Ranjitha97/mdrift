"""Blog listing and blog detail pages."""
from datetime import date
from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

from streams import blocks


class BlogListingPage(Page):
    """Listing page lists all the Blog Detail Pages."""

    template = "blog/blog_listing_page.html"

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)

    

    content_panels = Page.content_panels + [
        
        FieldPanel("header_title"),
        FieldPanel("meta_content"),
    ]

    def get_context(self, request, *args, **kwargs):
        """Adding custom stuff to our context."""
        context = super().get_context(request, *args, **kwargs)
        # Get all posts
        all_posts = BlogDetailPage.objects.live().public().order_by('-date')
        # Paginate all posts by 2 per page
        paginator = Paginator(all_posts, 2)
        # Try to get the ?page=x value
        page = request.GET.get("page")
        try:
            # If the page exists and the ?page=x is an int
            posts = paginator.page(page)
        except PageNotAnInteger:
            # If the ?page=x is not an int; show the first page
            posts = paginator.page(1)
        except EmptyPage:
            # If the ?page=x is out of range (too high most likely)
            # Then return the last page
            posts = paginator.page(paginator.num_pages)

        # "posts" will have child pages; you'll need to use .specific in the template
        # in order to access child properties, such as youtube_video_id and subtitle
        context["posts"] = posts
        return context


class BlogDetailPage(Page):
    """Blog detail page."""

    def posts(self):
        posts = BlogDetailPage.objects.all()
        posts = posts.order_by('-date')[:3]
        return posts
    

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)

    
    blog_detail_image = models.ForeignKey(
        "wagtailimages.Image",
        blank=False,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    blog_list_image = models.ForeignKey(
        "wagtailimages.Image",
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    date = models.DateField(max_length=50,null=True,blank=True)
    read_time = models.CharField(max_length=100, blank=False, null=True)


    content = StreamField(
        [
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
            ("cards", blocks.CardBlock()),
            ("cta", blocks.CTABlock()),
        ],
        null=True,
        blank=True,
    )
    content_panels = Page.content_panels + [
       
        
        ImageChooserPanel("blog_detail_image"),
        ImageChooserPanel("blog_list_image"),
        FieldPanel("date"),
        StreamFieldPanel("content"),
        FieldPanel("read_time"),
        FieldPanel("header_title"),
        FieldPanel("meta_content"),
        

    ]


