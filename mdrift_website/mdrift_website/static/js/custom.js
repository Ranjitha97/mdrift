$(window).on('load', function(){
  $(".loader").fadeOut("slow", init());
});

function init(){

  $('#mainNav ul li').on('mouseover', function(event) {
    event.preventDefault();
    var _this = $(this);
    
    $('.page-description ul li:not(:nth-of-type('+(_this.index()+1)+')').hide();
    $('.page-description ul li:nth-of-type('+(_this.index()+1)+')').show();
    
  });

  $('#mainNav ul').on('mouseout', function(event) {
    $('.page-description ul li').hide();
  });

  $(document).keyup(function(e) {
    if($('body').hasClass('nav-open')) {
      if (e.keyCode == 27) {
        $('.nav-btn .menu-btn').trigger('click');
      }
    }
  });

  var pageHeight = $(window).innerHeight();
  var pageWidth = $(window).innerWidth();

  var url = window.location.href;
  Cookies.set('url', url);
  var cookie = Cookies.get('intro');
  console.log(cookie, Cookies.get('url'));
  
  if ($(".page").hasClass("home")) {

    if (url == Cookies.get('url')){
      Cookies.remove('intro');
    }

    var introTimeline;
    if (cookie) {
      $(".section-intro").addClass("d-none");
      TweenMax.to($(".social-icons"), 0.5, {y: -(pageHeight/8)});
      $("body").removeClass("intro");
    } else {

      introTimeline = new TimelineMax({onComplete: hideIntro, delay: 1})
      introTimeline.fromTo("#introText1", 0.2, {y:"+=20", opacity: 0}, {y:"0", opacity: 1})
      .fromTo("#introIcon1", 0.2, {opacity: 0}, {opacity: 1})
      .fromTo("#introText2", 0.2, {y:"+=20", opacity: 0}, {y:"0", opacity: 1})
      .fromTo("#introIcon2", 0.2, {opacity: 0}, {opacity: 1})
      .fromTo("#introText3", 0.2, {y:"+=20", opacity: 0}, {y:"0", opacity: 1})

      if (url == Cookies.get('url')){
        Cookies.remove('intro');
      } else {
        Cookies.set('intro', true);
      }

    }

    function hideIntro(){
      setTimeout(function(){
        introTimeline.kill();
        introTimeline = null;
        var hideIntroTimeline = new TimelineMax({repeat:0, onComplete: updateIntroDisplay, onStart: startBannerAnim});
        hideIntroTimeline.to($(".section-intro"), 0.2, {opacity: 0, y: -(pageHeight/8)});
        hideIntroTimeline.to($(".social-icons"), 0.2, {y: -(pageHeight/8)});
      },500);
    }

    function skipIntro(){
      if(introTimeline.isActive()){
        introTimeline.kill();
        introTimeline = null;
        var skipIntroTimeline = new TimelineMax({repeat:0, onComplete: updateIntroDisplay, onStart: startBannerAnim});
        skipIntroTimeline.to($(".section-intro"), 0.2, {opacity: 0, y: -(pageHeight/8)});
        TweenMax.to($(".social-icons"), 0.5, {y: -(pageHeight/8)});
      }
    }

    function updateIntroDisplay(){
      $(".section-intro").addClass("d-none");
    }

    function changeBg1(){
      TweenMax.to($(".section-intro"), 0.2, {backgroundColor:"#5746B9"});
      $(".section-intro .intro-text").addClass("inverted");
    }

    function changeBg2(){
      TweenMax.to($(".section-intro"), 0.2, {backgroundColor:"#FD4B36"});
      $(".section-intro .intro-text").addClass("inverted");
    }

    function startBannerAnim(){
      var t1 = new TimelineMax({repeat:0, delay: 0});
      t1.fromTo($(".banner"), 0.5, {y: -(pageHeight/8)}, {y: 0});
      $("body").removeClass("intro");
    }

    var homeScrollController = new ScrollMagic.Controller();

    var maskTween = TweenMax.to('#textMask', 1, {
      x: "-=50",
      backgroundPosition: "+=50% center"
    });

    var teamDescriptionTween = TweenMax.staggerFromTo([$("#teamDescription h3"), $("#teamDescription p"), $("#teamDescription a")], 0.3, {y:"+=15", opacity:0}, {y:"0", opacity:1}, 0.1);

    var techSlidesTween = TweenMax.staggerFromTo(
      $("#technologySlides .slide-img"), 0.3, {y:"+=20", opacity:0}, {y:"0", opacity:1}, 0.1);

    var aboutTimeline = new TimelineMax()
    aboutTimeline.staggerFromTo([$("#aboutUs h4"), $("#aboutUs a")], 0.3, {y:"+=15", opacity:0}, {y:"0", opacity:1}, 0.1)
    .staggerFromTo($("#aboutUs .core-about"), 0.3, {x:"-=15", opacity:0}, {y:"0", opacity:1}, 0.1);

    var rocketTween = TweenMax.fromTo($("#rocketGraphics"), 1, {y:"73%"}, {y:"12%"});

    var portfolioTimeline = new TimelineMax()
    portfolioTimeline.staggerFromTo([$("#portfolio .section-header h3"), $("#portfolio .section-header p")], 0.3, {y:"+=15", opacity:0}, {y:"0", opacity:1}, 0.1)
    .staggerFromTo($("#portfolio .thumbnail"), 0.3, {y:"-=50", opacity:0}, {y:"0", opacity:1}, 0.1)

    var s1 = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: "#teamDescription",
      duration: 500,
      triggerHook: 0.5
    });

    var s2 = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: "#teamDescription",
      duration: 0,
      offset: 50,
      triggerHook: 1
    });

    var s3 = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: "#technologySlides",
      duration: 0,
      offset: 10,
      triggerHook: 1
    });

    var s4 = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: "#aboutUs",
      duration: 0,
      triggerHook: 0.5
    });

    var s5 = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: "#aboutUs",
      duration: 500,
      triggerHook: 0.5
    });

    var s6 = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: "#portfolio",
      duration: 300,
      offset: 100,
      triggerHook: 1
    });

    var s1v = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: "#teamDescription",
      triggerHook: 1
    });

    var s4v = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: "#aboutUs",
      triggerHook: 1
    });

    var s6v = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: "#portfolio",
      triggerHook: 0.5
    });


    s1.setTween(maskTween);
    s2.setTween(teamDescriptionTween);
    s3.setTween(techSlidesTween);
    s4.setTween(aboutTimeline);
    s5.setTween(rocketTween);
    s6.setTween(portfolioTimeline);

    s6v.setClassToggle(".navbar-light", 'd-none');

    homeScrollController.addScene(s1);
    homeScrollController.addScene(s2);
    homeScrollController.addScene(s3);
    homeScrollController.addScene(s4);
    homeScrollController.addScene(s5);
    homeScrollController.addScene(s6);

    homeScrollController.addScene(s1v);
    homeScrollController.addScene(s4v);
    homeScrollController.addScene(s6v);

    $(".section-intro").one('wheel', function(e) {
      skipIntro();
    });

    var bannerHeight = pageHeight-(pageHeight/8);
    $('.page.home .section-intro').height(pageHeight);
    $('.page.home .banner').height(bannerHeight);
    $('.more-about, .bg-blue').height(pageHeight);

  } else {
    Cookies.set('intro', true);
    $("body").removeClass("intro");
  }

  var closeIconClass;
  if($("body > .page-close-icon").hasClass("text-dark")){
    closeIconClass = "text-dark"
  } else {
    closeIconClass = "text-white"
  }

  var lastScrollTop = 0;
  $(document).scroll(function (event) {
    var scrollTop = $(window).scrollTop();
    if (scrollTop >= pageHeight/6) {
      TweenMax.to(".navbar .navbar-brand", 0.5, {opacity: 0, y:"-100%"});
    } else {
      TweenMax.to(".navbar .navbar-brand", 0.5, {opacity: 1, y:"0%"});
    }

    if ($(".page").hasClass("home") || $(".page").hasClass("about-us") || $(".page").hasClass("technical-overview")){
      if (scrollTop >= pageHeight-(pageHeight/8)-50) {
        $(".nav-btn").removeClass("inverted").addClass("scrolled");
      } else {
        $(".nav-btn").removeClass("scrolled").addClass("inverted");
      }
    }

    if (scrollTop>(pageHeight/8)-30){
      $(".social-icons").addClass("hide");
    } else {
      $(".social-icons").removeClass("hide");
    }

    if ($(".page").hasClass("home")){
      var offsetY = 0;
      if (scrollTop >= 15) {
        if(scrollTop<pageHeight/20){
          offsetY = scrollTop;
        } else {
          offsetY = pageHeight/20;
        }
      } else {
        offsetY = 0;
      }
      TweenMax.to(".page.home .banner img", 0.5, {y:offsetY});
    }

    if ($(".page").hasClass("projects")){
      var opacity = 1;
      if (scrollTop >= pageHeight/6) {
        opacity = 0;
      } else {
        opacity = 1;
      }
      TweenMax.to(".page.projects .banner h1", 0.5, {opacity: opacity, y:scrollTop});
      if (scrollTop >= 50) {
        TweenMax.to(".page.projects .scroll-icon", 0.5, {opacity: 0, y:"-10"});
      } else {
        TweenMax.to(".page.projects .scroll-icon", 0.5, {opacity: 1, y:0});
      }
    }

    if ($(".page").hasClass("project-details")){
      var st = $(this).scrollTop();
      if (st > lastScrollTop){
        TweenMax.to("body > .page-close-icon", 0.5, {opacity: 0, y:"-10"});
        if (scrollTop >= pageHeight-(pageHeight/6)) {
          $("body > .page-close-icon").attr("class", "page-close-icon").addClass("text-dark");
        } else {
          $("body > .page-close-icon").attr("class", "page-close-icon").addClass(closeIconClass);
        }
      } else {
        if (scrollTop >= pageHeight-(pageHeight/6)) {
          $("body > .page-close-icon").attr("class", "page-close-icon").addClass("text-dark");
        } else {
          $("body > .page-close-icon").attr("class", "page-close-icon").addClass(closeIconClass);
        }
        TweenMax.to("body > .page-close-icon", 0.5, {opacity: 1, y:"0"});
      }
      lastScrollTop = st;
    }

  });

  $('.nav-btn').on('click', function(){
    $('.page-description ul li').hide();

    if ($('body').hasClass('nav-open')){
      var navTimeline = new TimelineMax({onComplete: function(){
        $('body').removeClass('nav-open');
      }});
      navTimeline.staggerFromTo($("#mainNav ul.list-unstyled li"), 0.2, {opacity: 1, y: "0"}, {opacity: 0, y: "-=30"}, 0.05);
    } else {
      var navTimeline = new TimelineMax({delay: 0.4});
      navTimeline.staggerFromTo($("#mainNav ul.list-unstyled li"), 0.4, {opacity: 0, y: "-=30"}, {opacity: 1, y: "0"}, 0.05);
      $('body').addClass('nav-open');
    }
  })

  $('.closebtn').on('click', function(){
    $('body').removeClass('nav-open');
    $(".nav-btn").addClass("inverted");
  });

  if($(".page").hasClass("projects")){

    var projectsController = new ScrollMagic.Controller();
    function setProjectCardHeight(){

      pageWidth = $(window).innerWidth();
      pageHeight = $(window).innerHeight();
      if(pageWidth>767){
        $(".page.projects .banner").height(pageHeight-(pageHeight/4));
      } else {
        $(".page.projects .banner").css('height', '');
      }

      $(".projects-listing .card").each(function(){
        var pageWidth = $(window).innerWidth();
        var cardWidth = $(this).width();
        var cardHeight = cardWidth/2.307692308;
        $(this).height(cardHeight);
      });
    }

    $(".projects-listing .card").each(function(){
      var projectsScene = new ScrollMagic.Scene({
        tweenChanges: true,
        triggerElement: this,
        offset: 50,
        triggerHook: 1
      });

      var projectsTween = TweenMax.fromTo($(this), 0.5, {alpha: 0, y:"+=50"}, {alpha: 1, y:"0"});

      projectsScene.setTween(projectsTween);
      projectsController.addScene(projectsScene);
    });
    
    setProjectCardHeight();

    $(window).resize(function(){
      setProjectCardHeight();
    });

  }

  if($(".page").hasClass("project-details")){
    pageWidth = $(window).innerWidth();
    function setBannerHeight(){
      pageWidth = $(window).innerWidth();
      pageHeight = $(window).innerHeight();
      if(pageWidth>767){
        $(".page.project-details .banner").height(pageHeight-(pageHeight/8));
      } else {
        $(".page.project-details .banner").css('height', '');
      }
      var cardWidth = $(".next-project .card").width();
      var cardHeight = cardWidth/2.307692308;
      $(".next-project .card").height(cardHeight);

      var footerHeight = $("footer").innerHeight();
      if(pageWidth>767){
        $(".wipe-trigger").height(pageHeight-footerHeight);
        $(".next-project").css('padding-bottom', footerHeight+20);
      } else {
        $(".wipe-trigger").height(cardHeight + footerHeight);
        $(".next-project").css({top: 'auto', 'padding-bottom': footerHeight+20});
      }

    }

    setBannerHeight();

    $(window).resize(function(){
      setBannerHeight();
    })

    var projectDetailsController = new ScrollMagic.Controller();

    var parallaxScene = new ScrollMagic.Scene({
      tweenChanges: true,
      triggerElement: ".parallax-wrapper",
      duration: "200%",
      triggerHook: "onEnter"
    });

    var autoScrollSceneV = new ScrollMagic.Scene({
      duration : "100%",
      tweenChanges: true,
      triggerElement: ".project-thumb",
      triggerHook: 'onCenter'
    });

    var autoScrollSceneH = new ScrollMagic.Scene({
      duration : "100%",
      tweenChanges: true,
      triggerElement: ".project-thumb",
      triggerHook: 'onCenter'
    });

    var parallax = TweenMax.to($(".parallax-bg"), 1, {y: '-50%', ease:Power0.easeNone});
    parallaxScene.setTween(parallax);



    var autoScrollTimeline = new TimelineMax({repeat:-1, repeatDelay:1, delay: 1});
    var imgHeight = $(".project-thumb .autoscroll.vertical img").height();
    var imgPartialHeight = imgHeight/10;
    var duration = (imgHeight/2000)
    autoScrollTimeline.to($(".project-thumb .autoscroll.vertical img"), duration, {y: -(imgPartialHeight), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.vertical img"), duration, {y: -(imgPartialHeight*2), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.vertical img"), duration, {y: -(imgPartialHeight*3), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.vertical img"), duration, {y: -(imgPartialHeight*4), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.vertical img"), duration, {y: -(imgPartialHeight*5), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.vertical img"), duration, {y: -(imgPartialHeight*6), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.vertical img"), duration, {y: -(imgPartialHeight*7), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.vertical img"), duration, {y: -(imgHeight-$(".project-thumb .autoscroll.vertical").height()), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.vertical img"), 0.5, {y: 0});

    autoScrollSceneV.setTween(autoScrollTimeline);
    projectDetailsController.addScene(autoScrollSceneV);



    var autoScrollTimelineW = new TimelineMax({repeat:-1, repeatDelay:1, delay: 1});
    var imgWidth = $(".project-thumb .autoscroll.horizontal img").width();
    var imgPartialWidth = imgWidth/10;
    var duration = (imgWidth/2000)
    autoScrollTimelineW.to($(".project-thumb .autoscroll.horizontal img"), duration, {x: -(imgPartialWidth), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.horizontal img"), duration, {x: -(imgPartialWidth*2), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.horizontal img"), duration, {x: -(imgPartialWidth*3), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.horizontal img"), duration, {x: -(imgPartialWidth*4), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.horizontal img"), duration, {x: -(imgPartialWidth*5), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.horizontal img"), duration, {x: -(imgPartialWidth*6), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.horizontal img"), duration, {x: -(imgPartialWidth*7), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.horizontal img"), duration, {x: -(imgWidth-$(".project-thumb .autoscroll.horizontal").width()), ease: Power2.easeInOut})
    .to($(".project-thumb .autoscroll.horizontal img"), 0.5, {x: 0});

    autoScrollSceneH.setTween(autoScrollTimelineW);
    projectDetailsController.addScene(autoScrollSceneH);



    projectDetailsController.addScene(parallaxScene);
    
    
  }

  if($(".page").hasClass("about-us")){
    function setBannerHeight(){
      pageWidth = $(window).innerWidth();
      pageHeight = $(window).innerHeight();
      if(pageWidth>767){
        $(".page.about-us .banner").height(pageHeight-(pageHeight/8));
      } else {
        $(".page.about-us .banner").css('height', '');
      }

      $(".social-icons").css({'bottom': (pageHeight/8)+16})



      var letterAnim = 0;
      var proTrigger = function () {
        if(letterAnim == 0) {
          setInterval(function(){ 
            $("#ourProcess .swiper-pagination-bullet:nth-of-type("+letterAnim+")").css({
              'transform' : 'scale(1)'
            });
            if(letterAnim==$('#ourProcess .swiper-pagination-bullet').length) {
              letterAnim = 0
            }
            letterAnim++;
            $("#ourProcess .swiper-pagination-bullet:nth-of-type("+letterAnim+")").css({
              'transform' : 'scale(2.5)'
            });
          }, 400);
        }
      }

      var aboutScrollController = new ScrollMagic.Controller();
      var ourProcess1 = new ScrollMagic.Scene({
        tweenChanges: true,
        triggerElement: "#ourProcess",
        triggerHook: 0.5
      });

      ourProcess1.setTween(proTrigger)
      aboutScrollController.addScene(ourProcess1);


      particlesJS("particles-js",
        { "particles": {
            "number": {
              "value": 139,
              "density": {
                "enable": true,
                "value_area": 441.3648243462092
              }
            },
            "color": {
              "value": "#fff"
            },
            "shape": {
              "type": "circle",
              "stroke": {
                "width": 1,
                "color": "rgba(238,238,238,0.8)"
              },
              "polygon": {
                "nb_sides": 3
              },
              "image": {
                "src": "",
                "width": 100,
                "height": 100
              }
            },
            "opacity": {
              "value": 0.38481889460772545,
              "random": true,
              "anim": {
                "enable": false,
                "speed": 1,
                "opacity_min": 0.7821448583331299,
                "sync": false
              }
            },
            "size": {
              "value": 3,
              "random": true,
              "anim": {
                "enable": false,
                "speed": 40,
                "size_min": 0.2,
                "sync": false
              }
            },
            "line_linked": {
              "enable": false,
              "distance": 500,
              "color": "#ffffff",
              "opacity": 0.20844356791251797,
              "width": 2
            },
            "move": {
              "enable": true,
              "speed": 1,
              "direction": "none",
              "random": false,
              "straight": false,
              "out_mode": "out",
              "bounce": false,
              "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 1200
              }
            }
          },
          "interactivity": {
            "detect_on": "canvas",
            "events": {
              "onhover": {
                "enable": true,
                "mode": "repulse"
              },
              "onclick": {
                "enable": true,
                "mode": "push"
              },
              "resize": true
            },
            "modes": {
              "grab": {
                "distance": 150,
                "line_linked": {
                  "opacity": 0.4
                }
              },
              "bubble": {
                "distance": 400,
                "size": 4,
                "duration": 0.3,
                "opacity": 1,
                "speed": 3
              },
              "repulse": {
                "distance": 100,
                "duration": .3
              },
              "push": {
                "particles_nb": 30
              },
              "remove": {
                "particles_nb": 2
              }
            }
          },
          "retina_detect": false
        }
      );

    }

    setBannerHeight();

    $(window).resize(function(){
      setBannerHeight();
    })
  }

  if ($(".page").hasClass("technical-overview")){
    function setBannerHeight(){
      pageWidth = $(window).innerWidth();
      pageHeight = $(window).innerHeight();
      if(pageWidth>767){
        $(".page.technical-overview .banner").height(pageHeight-(pageHeight/8));
      } else {
        $(".page.technical-overview .banner").css('height', '');
      }

      $(".social-icons").css({'bottom': (pageHeight/8)+16})

      particlesJS("particles-js",
        {
          "particles": {
            "number": {
              "value": 100,
              "density": {
                "enable": false,
                "value_area": 2000
              }
            },
            "color": {
              "value": "#f5f2f2"
            },
            "shape": {
              "type": "edge",
              "stroke": {
                "width": 1,
                "color": "rgba(255, 255, 255, 0.2)"
              },
              "polygon": {
                "nb_sides": 4
              },
              "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
              }
            },
            "opacity": {
              "value": 0.01603412060865523,
              "random": false,
              "anim": {
                "enable": false,
                "speed": 0.3248308849205381,
                "opacity_min": 0.016241544246026904,
                "sync": false
              }
            },
            "size": {
              "value": 4,
              "random": true,
              "anim": {
                "enable": false,
                "speed": 40,
                "size_min": 0.1,
                "sync": false
              }
            },
            "line_linked": {
              "enable": true,
              "distance": 150,
              "color": "#ffffff",
              "opacity": 0.4,
              "width": 1
            },
            "move": {
              "enable": true,
              "speed": .7,
              "direction": "bottom-left",
              "random": false,
              "straight": false,
              "out_mode": "out",
              "bounce": false,
              "attract": {
                "enable": false,
                "rotateX": 962.0472365193136,
                "rotateY": 1200
              }
            }
          },
          "interactivity": {
            "detect_on": "canvas",
            "events": {
              "onhover": {
                "enable": true,
                "mode": "bubble"
              },
              "onclick": {
                "enable": true,
                "mode": "push"
              },
              "resize": true
            },
            "modes": {
              "grab": {
                "distance": 300,
                "line_linked": {
                  "opacity": 1
                }
              },
              "bubble": {
                "distance": 300,
                "size": 8.181158184520177,
                "duration": 6.009371371029953,
                "opacity": 0.251743935813417,
                "speed": 3
              },
              "repulse": {
                "distance": 200,
                "duration": 0.4
              },
              "push": {
                "particles_nb": 4
              },
              "remove": {
                "particles_nb": 2
              }
            }
          },
          "retina_detect": true
        } 
      );

    }

    setBannerHeight();

    $(window).resize(function(){
      setBannerHeight();
    })
  }

}