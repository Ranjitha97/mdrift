(function (window) {

    // $(function () {
          getCookie = function(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
              var cookies = document.cookie.split(';');
              for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
                }
              }
            }
            return cookieValue;
          };
    
          $('#datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
          });
    
          $.validator.addMethod("regex", function(value, element, regexpr) {
            return regexpr.test(value);
          });
    
          var contactFormValidator = $("#contactForm").validate({
            errorElement: 'small',
            errorClass: 'error text-danger',
            errorPlacement: function(error, element) {
              if (element.parent().hasClass("input-group")) {
                error.appendTo( element.parent().parent());
              } else {
                error.appendTo( element.parent());
              }
            },
            rules: {
              "email": {
                required: true,
                regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
              },
              "description": {
                required: true
              }
            },
            messages: {
              "email": {
                required: "Please enter your email",
                regex: "Please enter a valid email"
              },
              "description": {
                required: "Please describe your needs"
              }
            },
            submitHandler: function() {
              var csrftoken = getCookie('csrftoken');
              var formdata = new FormData();
              formdata.append('name',$('#name').val());
              formdata.append('companyName',$('#companyName').val());
              formdata.append('email',$('#email').val());
              formdata.append('datepicker',$('#datepicker').val());
              formdata.append('budget',$('#budget').val());
              formdata.append('description',$('#description').val());
              $.ajax({
                  method: 'POST',
                  url: $(form).attr('action'),
                  data: formdata,
                  contentType: false,
                  processData: false,
                  beforeSend: function(xhr, settings) {
                      xhr.setRequestHeader("X-CSRFToken", csrftoken);
                  }
              })
              .done( function (d, textStatus, jqXHR) {
                // alert(d);
                $("#msgBox").html('Enquiry sent successfully').removeClass("d-none");
                setTimeout(function(){
                   $("#msgBox").addClass("d-none"); 
                }, 3000)
                $("#contactForm").trigger("reset");
              })
              .fail( function (jqXHR, textStatus, errorThrown) {
                $("#msgBox").html('Something went wrong!Please try after some time').removeClass("d-none");
                setTimeout(function(){
                   $("#msgBox").addClass("d-none"); 
                }, 3000)
              })
              .always(function(){
              })
            }
          });
    
          // });
    // $("#contact_submit").click(function() {
    //   alert(this.id);
    //     var csrftoken = getCookie('csrftoken');
    //     var formdata = new FormData();
    //     $.ajax({
    //         method: 'POST',
    //         url: '/' + 'api/contact/email/send',
    //         data: formdata,
    //         contentType: false,
    //         processData: false,
    //         beforeSend: function(xhr, settings) {
    //             xhr.setRequestHeader("X-CSRFToken", csrftoken);
    //         }
    //     })
    //     .done( function (d, textStatus, jqXHR) {
    //       alert(d);
    //     })
    //     .fail( function (jqXHR, textStatus, errorThrown) {
    //       alert(jQuery.parseJSON(jqXHR.responseText))
    //     })
    //     .always(function(){
    //     })
    // });
    
    })(this);
    
    $.extend(true, $.fn.datetimepicker.defaults, {
              icons: {
                time: 'ion-ios-clock-outline',
                date: 'ion-ios-calendar-outline',
                up: 'ion-ios-arrow-up',
                down: 'ion-ios-arrow-down',
                previous: 'ion-ios-arrow-left',
                next: 'ion-ios-arrow-right',
                today: 'ion-ios-checkmark-outline',
                clear: 'ion-ios-trash',
                close: 'ion-ios-close-outline'
              }
            });
    
    