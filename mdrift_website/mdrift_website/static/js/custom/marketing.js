(function (window) {

  // $(function () {
        getCookie = function(name) {
          var cookieValue = null;
          if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
              }
            }
          }
          return cookieValue;
        };
  
        toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-bottom-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
  
  
        $.validator.addMethod("regex", function(value, element, regexpr) {
          return regexpr.test(value);
        });
        var contactFormValidator = $("#quickQuote").validate({
          errorElement: 'small',
          errorClass: 'error text-danger',
          errorPlacement: function(error, element) {
            if (element.parent().hasClass("form-check")) {
              error.appendTo( element.parent().parent());
            } else {
              error.appendTo( element.parent());
            }
          },
          rules: {
            "firstName": {
              required: true
            },
            "lastName": {
              required: true
            },
            "email": {
              required: true,
              regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            },
            "modelsRequired": {
              required: true,
              // minlength: 2
            },
            "description": {
              required: true
            }
          },
          messages: {
            "firstName": {
              required: "Please enter your first name",
            },
            "lastName": {
              required: "Please enter your last name",
            },
            "email": {
              required: "Please enter your email",
              regex: "Please enter a valid email"
            },
            "modelsRequired": {
              required: "Please select a  models"
            },
            "description": {
              required: "Please describe your needs"
            }
          },
          submitHandler: function() {
            var csrftoken = getCookie('csrftoken');
            var formdata = new FormData();
            var modelsRequired = '';
            var count = 0 
            $('.form-check-input:checked').each(function(){        
                var values = $(this).val();
                if(count > 0){
                  modelsRequired += ',';
                }
                modelsRequired += values;              
                count +=1;
            });
            formdata.append('firstName',$('#firstName').val());
            formdata.append('lastName',$('#lastName').val());
            formdata.append('email',$('#email').val());
            formdata.append('modelsRequired',modelsRequired);
            formdata.append('description',$('#description').val());
            $.ajax({
                method: 'POST',
                url: '/' + 'api/quick/quote/send',
                data: formdata,
                contentType: false,
                processData: false,
                beforeSend: function(xhr, settings) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            })
            .done( function (d, textStatus, jqXHR) {
              // $("#msgBox").html('Enquiry sent successfully').removeClass("d-none");
              toastr.success("Message sent successfully");
              $("#quickQuote").trigger("reset");
            })
            .fail( function (jqXHR, textStatus, errorThrown) {
              toastr.error("Message sent failed");
            })
            .always(function(){
            })
          }
        });
  
        // });
  // $("#contact_submit").click(function() {
  //   alert(this.id);
  //     var csrftoken = getCookie('csrftoken');
  //     var formdata = new FormData();
  //     $.ajax({
  //         method: 'POST',
  //         url: '/' + 'api/contact/email/send',
  //         data: formdata,
  //         contentType: false,
  //         processData: false,
  //         beforeSend: function(xhr, settings) {
  //             xhr.setRequestHeader("X-CSRFToken", csrftoken);
  //         }
  //     })
  //     .done( function (d, textStatus, jqXHR) {
  //       alert(d);
  //     })
  //     .fail( function (jqXHR, textStatus, errorThrown) {
  //       alert(jQuery.parseJSON(jqXHR.responseText))
  //     })
  //     .always(function(){
  //     })
  // });
  
  })(this);